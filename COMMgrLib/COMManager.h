#pragma once
#include "afx.h"
#include "Diagnostic.h"

#define COMMGR_POLL_TIME        1000
#define COMMGR_POLL_SHORT_TIME  100
#define COM_ALL                 (-1)
#define COMMGR_FILMAPPING_NAME  _T("WW12_FM_COMMANAGER")

typedef  void (*PFNSTATECHANGECB)(LPVOID pParam, int nPortID, int nNewState);

typedef struct _CBInfo CBInfo;

struct _CBInfo{
    CBInfo             *m_pNext;
    PFNSTATECHANGECB    m_pfnStateChangeCB;
    LPVOID              m_pUserParam;
};

typedef struct _COMPortInfo COMPortInfo;
struct _COMPortInfo{
    DWORD               m_dwHubIndex;
    DWORD               m_dwPortIndex;
};

typedef struct{
    BOOL                m_bFreePollCOM;     // 控制方释放了COM状态扫描控制权
    BOOL                m_bCOMUsed[COM_MAX];
    LPVOID              m_pCOMUser[COM_MAX];
    int                 m_nPhoneMode[COM_MAX];
    COMPortInfo         m_COMPortInfo[COM_MAX];
}SharedCOMState;

class COMManager
{
public:
    // 只允许存在一个COMManager实例
    static int          Create(COMManager **ppCOMMgr);
    static int          Release(void);
    static void         UpdatePhoneMode(int nPortID, int nNewMode, COMPortInfo myInfo);
    static int          GetCurrentPhoneMode(int nPortID);
    static BOOL         GetCurrentPortUsed(int nPortID);
    
    void                RegStateChangeCB(int nPortID, PFNSTATECHANGECB pfnStateChangeCB, LPVOID pUserParam);
    void                UnregStateChangeCB(int nPortID, PFNSTATECHANGECB pfnStateChangeCB, LPVOID pUserParam);
    BOOL                GetUsed(int nPortID)                                    {return m_pSharedCOMState?m_pSharedCOMState->m_bCOMUsed[nPortID]:FALSE;};
    BOOL                SetUsed(int nPortID, BOOL bUsed, LPVOID pUser);
    int                 GetPhoneMode(int nPortID)                               {return m_pSharedCOMState?m_pSharedCOMState->m_nPhoneMode[nPortID]:DIAG_PHONEMODE_NOPORT;};
    void                SetPhoneMode(int nPortID, int nNewMode, COMPortInfo myInfo);
    BOOL                IsPortExist(int nPortID)                                {return m_pSharedCOMState?(m_pSharedCOMState->m_nPhoneMode[nPortID] != DIAG_PHONEMODE_NOPORT):FALSE;};
    BOOL                IsPortSame(int nPortID1, int nPortID2);
    DWORD               GetHubIndex(int nPortID)                                {return m_pSharedCOMState?m_pSharedCOMState->m_COMPortInfo[nPortID].m_dwHubIndex:0;};
    DWORD               GetPortIndex(int nPortID)                               {return m_pSharedCOMState?m_pSharedCOMState->m_COMPortInfo[nPortID].m_dwPortIndex:0;};

    void                RefreshCOMList(CComboBox &cCOMCombo, CStringArray &arModeString, int nDefCOMID=-1);
    void                RefreshCOMSel(CComboBox &cCOMCombo, int nSel, CStringArray &arModeString);
    int                 GetCOMPortID(CComboBox &cCOMCombo, int nCOMIdx);

private:
    COMManager(void);
    ~COMManager(void);
    void                UpdateCOMInfo(BOOL bOnlyUsedInfo);
    void                GetCOMString(int nPort, CString &szString, CString &szMode);
    static UINT         COMManagerThread(LPVOID pParam);
    void                ResetCOMManagerThread(void)    { m_pThread = NULL;}
    void                CBInfoAdd(int nPortID, PFNSTATECHANGECB pfnCB, LPVOID pUser);
    void                CBInfoRemove(int nPortID, PFNSTATECHANGECB pfnCB, LPVOID pUser);
    void                CallCBInfo(int nPortID, int nNewMode);
    
private:
    CDiagnostic         m_PhoneDiag[COM_MAX];
    CBInfo             *m_pCBInfo[COM_MAX];
    HANDLE              m_hWaitEvent;              //A event handle we need for internal synchronisation
    CWinThread         *m_pThread; 
    BOOL                m_bHandleCOMPoll;           // 是否获得了COM口扫描权
    SharedCOMState     *m_pSharedCOMState;
    HANDLE              m_hMapFile;
    BOOL                m_bMonitor;

    // 只允许存在一个COMManager实例
    static COMManager  *m_pCOMMgr;
    static int          m_nRefCnt;
};