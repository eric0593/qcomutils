/*=================================================================================================

FILE : SERIALPORT.C

SERVICES: Implementation of an MFC wrapper class for serial ports communicate

GENERAL DESCRIPTION:

PUBLIC CLASSES AND STATIC FUNCTIONS:
CSerialComm;

INITIALIZATION AND SEQUENCING REQUIREMENTS:

====================================================================================================*/


/*====================================================================================================

==MODIFY HISTOY FOR FILE==

when         who     what, where, why
----------   ---     ----------------------------------------------------------
12/12/2003   tyz      create

=====================================================================================================*/
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "stdafx.h"
#include "SerialComm.h"
#include "Log.h"
#include "USBPorts.h"

/*===========================================================================

DEFINITIONS AND CONSTANTS

===========================================================================*/
static const TCHAR *pstrCommPortName[] = {     
     _T("\\\\.\\COM1"),   _T("\\\\.\\COM2"),   _T("\\\\.\\COM3"),   _T("\\\\.\\COM4"),   _T("\\\\.\\COM5")
    ,_T("\\\\.\\COM6"),   _T("\\\\.\\COM7"),   _T("\\\\.\\COM8"),   _T("\\\\.\\COM9"),   _T("\\\\.\\COM10")
    ,_T("\\\\.\\COM11"),  _T("\\\\.\\COM12"),  _T("\\\\.\\COM13"),  _T("\\\\.\\COM14"),  _T("\\\\.\\COM15")
    ,_T("\\\\.\\COM16"),  _T("\\\\.\\COM17"),  _T("\\\\.\\COM18"),  _T("\\\\.\\COM19"),  _T("\\\\.\\COM20")
    ,_T("\\\\.\\COM21"),  _T("\\\\.\\COM22"),  _T("\\\\.\\COM23"),  _T("\\\\.\\COM24"),  _T("\\\\.\\COM25")
    ,_T("\\\\.\\COM26"),  _T("\\\\.\\COM27"),  _T("\\\\.\\COM28"),  _T("\\\\.\\COM29"),  _T("\\\\.\\COM30")
    ,_T("\\\\.\\COM31"),  _T("\\\\.\\COM32"),  _T("\\\\.\\COM33"),  _T("\\\\.\\COM34"),  _T("\\\\.\\COM35")
    ,_T("\\\\.\\COM36"),  _T("\\\\.\\COM37"),  _T("\\\\.\\COM38"),  _T("\\\\.\\COM39"),  _T("\\\\.\\COM40")
    ,_T("\\\\.\\COM41"),  _T("\\\\.\\COM42"),  _T("\\\\.\\COM43"),  _T("\\\\.\\COM44"),  _T("\\\\.\\COM45")
    ,_T("\\\\.\\COM46"),  _T("\\\\.\\COM47"),  _T("\\\\.\\COM48"),  _T("\\\\.\\COM49"),  _T("\\\\.\\COM50")
    ,_T("\\\\.\\COM51"),  _T("\\\\.\\COM52"),  _T("\\\\.\\COM53"),  _T("\\\\.\\COM54"),  _T("\\\\.\\COM55")
    ,_T("\\\\.\\COM56"),  _T("\\\\.\\COM57"),  _T("\\\\.\\COM58"),  _T("\\\\.\\COM59"),  _T("\\\\.\\COM60")
    ,_T("\\\\.\\COM61"),  _T("\\\\.\\COM62"),  _T("\\\\.\\COM63"),  _T("\\\\.\\COM64"),  _T("\\\\.\\COM65")
    ,_T("\\\\.\\COM66"),  _T("\\\\.\\COM67"),  _T("\\\\.\\COM68"),  _T("\\\\.\\COM69"),  _T("\\\\.\\COM70")
    ,_T("\\\\.\\COM71"),  _T("\\\\.\\COM72"),  _T("\\\\.\\COM73"),  _T("\\\\.\\COM74"),  _T("\\\\.\\COM75")
    ,_T("\\\\.\\COM76"),  _T("\\\\.\\COM77"),  _T("\\\\.\\COM78"),  _T("\\\\.\\COM79"),  _T("\\\\.\\COM80")
    ,_T("\\\\.\\COM81"),  _T("\\\\.\\COM82"),  _T("\\\\.\\COM83"),  _T("\\\\.\\COM84"),  _T("\\\\.\\COM85")
    ,_T("\\\\.\\COM86"),  _T("\\\\.\\COM87"),  _T("\\\\.\\COM88"),  _T("\\\\.\\COM89"),  _T("\\\\.\\COM90")
    ,_T("\\\\.\\COM91"),  _T("\\\\.\\COM92"),  _T("\\\\.\\COM93"),  _T("\\\\.\\COM94"),  _T("\\\\.\\COM95")
    ,_T("\\\\.\\COM96"),  _T("\\\\.\\COM97"),  _T("\\\\.\\COM98"),  _T("\\\\.\\COM99"),  _T("\\\\.\\COM100")
    ,_T("\\\\.\\COM101"), _T("\\\\.\\COM102"), _T("\\\\.\\COM103"), _T("\\\\.\\COM104"), _T("\\\\.\\COM105")
    ,_T("\\\\.\\COM106"), _T("\\\\.\\COM107"), _T("\\\\.\\COM108"), _T("\\\\.\\COM109"), _T("\\\\.\\COM110")   
    ,_T("\\\\.\\COM111"), _T("\\\\.\\COM112"), _T("\\\\.\\COM113"), _T("\\\\.\\COM114"), _T("\\\\.\\COM115")
    ,_T("\\\\.\\COM116"), _T("\\\\.\\COM117"), _T("\\\\.\\COM118"), _T("\\\\.\\COM119"), _T("\\\\.\\COM120")
    ,_T("\\\\.\\COM121"), _T("\\\\.\\COM122"), _T("\\\\.\\COM123"), _T("\\\\.\\COM124"), _T("\\\\.\\COM125")
    ,_T("\\\\.\\COM126"), _T("\\\\.\\COM127"), _T("\\\\.\\COM128"), _T("\\\\.\\COM129"), _T("\\\\.\\COM130")
    ,_T("\\\\.\\COM131"), _T("\\\\.\\COM132"), _T("\\\\.\\COM133"), _T("\\\\.\\COM134"), _T("\\\\.\\COM135")
    ,_T("\\\\.\\COM136"), _T("\\\\.\\COM137"), _T("\\\\.\\COM138"), _T("\\\\.\\COM139"), _T("\\\\.\\COM140")
    ,_T("\\\\.\\COM141"), _T("\\\\.\\COM142"), _T("\\\\.\\COM143"), _T("\\\\.\\COM144"), _T("\\\\.\\COM145")
    ,_T("\\\\.\\COM146"), _T("\\\\.\\COM147"), _T("\\\\.\\COM148"), _T("\\\\.\\COM149"), _T("\\\\.\\COM150")
    ,_T("\\\\.\\COM151"), _T("\\\\.\\COM152"), _T("\\\\.\\COM153"), _T("\\\\.\\COM154"), _T("\\\\.\\COM155")
    ,_T("\\\\.\\COM156"), _T("\\\\.\\COM157"), _T("\\\\.\\COM158"), _T("\\\\.\\COM159"), _T("\\\\.\\COM160")
    ,_T("\\\\.\\COM161"), _T("\\\\.\\COM162"), _T("\\\\.\\COM163"), _T("\\\\.\\COM164"), _T("\\\\.\\COM165")
    ,_T("\\\\.\\COM166"), _T("\\\\.\\COM167"), _T("\\\\.\\COM168"), _T("\\\\.\\COM169"), _T("\\\\.\\COM170")
    ,_T("\\\\.\\COM171"), _T("\\\\.\\COM172"), _T("\\\\.\\COM173"), _T("\\\\.\\COM174"), _T("\\\\.\\COM175")
    ,_T("\\\\.\\COM176"), _T("\\\\.\\COM177"), _T("\\\\.\\COM178"), _T("\\\\.\\COM179"), _T("\\\\.\\COM180")
    ,_T("\\\\.\\COM181"), _T("\\\\.\\COM182"), _T("\\\\.\\COM183"), _T("\\\\.\\COM184"), _T("\\\\.\\COM185")
    ,_T("\\\\.\\COM186"), _T("\\\\.\\COM187"), _T("\\\\.\\COM188"), _T("\\\\.\\COM189"), _T("\\\\.\\COM190")
    ,_T("\\\\.\\COM191"), _T("\\\\.\\COM192"), _T("\\\\.\\COM193"), _T("\\\\.\\COM194"), _T("\\\\.\\COM195")
    ,_T("\\\\.\\COM196"), _T("\\\\.\\COM197"), _T("\\\\.\\COM198"), _T("\\\\.\\COM199"), _T("\\\\.\\COM200")     
    ,_T("\\\\.\\COM201"), _T("\\\\.\\COM202"), _T("\\\\.\\COM203"), _T("\\\\.\\COM204"), _T("\\\\.\\COM205")
    ,_T("\\\\.\\COM206"), _T("\\\\.\\COM207"), _T("\\\\.\\COM208"), _T("\\\\.\\COM209"), _T("\\\\.\\COM210")   
    ,_T("\\\\.\\COM211"), _T("\\\\.\\COM212"), _T("\\\\.\\COM213"), _T("\\\\.\\COM214"), _T("\\\\.\\COM215")
    ,_T("\\\\.\\COM216"), _T("\\\\.\\COM217"), _T("\\\\.\\COM218"), _T("\\\\.\\COM219"), _T("\\\\.\\COM220")
    ,_T("\\\\.\\COM221"), _T("\\\\.\\COM222"), _T("\\\\.\\COM223"), _T("\\\\.\\COM224"), _T("\\\\.\\COM225")
    ,_T("\\\\.\\COM226"), _T("\\\\.\\COM227"), _T("\\\\.\\COM228"), _T("\\\\.\\COM229"), _T("\\\\.\\COM230")
    ,_T("\\\\.\\COM231"), _T("\\\\.\\COM232"), _T("\\\\.\\COM233"), _T("\\\\.\\COM234"), _T("\\\\.\\COM235")
    ,_T("\\\\.\\COM236"), _T("\\\\.\\COM237"), _T("\\\\.\\COM238"), _T("\\\\.\\COM239"), _T("\\\\.\\COM240")
    ,_T("\\\\.\\COM241"), _T("\\\\.\\COM242"), _T("\\\\.\\COM243"), _T("\\\\.\\COM244"), _T("\\\\.\\COM245")
    ,_T("\\\\.\\COM246"), _T("\\\\.\\COM247"), _T("\\\\.\\COM248"), _T("\\\\.\\COM249"), _T("\\\\.\\COM250")
    ,_T("\\\\.\\COM251"), _T("\\\\.\\COM252"), _T("\\\\.\\COM253"), _T("\\\\.\\COM254"), _T("\\\\.\\COM255")    
    ,_T("\\\\.\\COM256") 
};

/*===========================================================================

LOCAL/STATIC DATA

===========================================================================*/
#define COM_POLL_WAIT       1500
/*==========================================================================

STATIC FUNCTION DEFINATION

==========================================================================*/

UINT CSerialComm::PortMonitorThread(LPVOID pParam)
{
    CSerialComm* pThis = (CSerialComm*)pParam;
    pThis->PortMonitor();
    return 0;
}

void CSerialComm::PortMonitor(void)
{
    DWORD dwTransfer, dwEvtMask;//, dwErrorFlags;
    OVERLAPPED os = {0};
    LOG_OUTPUT(_T("CSerialComm::PortMonitor Start COM%d\n"),GetPortID()+1);
    os.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    ASSERT(os.hEvent);

    // if port is close, this thread is terminated by exiting the proc
    while(GetOpenFlag()) 
    {
        if(m_bInterOpen)
        {
            dwEvtMask = 0;

            if(FALSE == WaitCommEvent(GetPortHandle(), &dwEvtMask, &os)) 
            {         
                if(GetLastError() != ERROR_IO_PENDING) 
                {
                    InterClose();
                    WaitMS(COM_POLL_WAIT);
                    continue;
                }
                else
                {
                    GetOverlappedResult(GetPortHandle(), &os, &dwTransfer, TRUE);
                    os.Offset += dwTransfer;
                }
            }

            if(dwEvtMask & EV_RXFLAG)
            {
                RXFrame();
            }
            else if(dwEvtMask & EV_DSR)
            {
                InterClose();
            }
            else
            {
                TRACE("Event flag 0x%x received COM%d!\n", dwEvtMask, GetPortID()+1);
            }
        }
        else
        {
            if(FALSE == InterOpen())
            {
                WaitMS(COM_POLL_WAIT);
            }
        }
    }
    
    CloseHandle(os.hEvent);
    ResetMonitorThread();      //监视线程已经退出,将其句柄置为NULL;
    LOG_OUTPUT(_T("CSerialComm::PortMonitor END COM%d\n"),GetPortID()+1);
}
/*==========================================================================

MEMBER FUNCTION DEFINATION

==========================================================================*/

CSerialComm::CSerialComm()
{
    m_Parameter.port = DEFAULT_PORT;
    m_Parameter.dwBaudRate = DEFAULT_BAUDRATE;
    m_Parameter.bDataBits = DEFAULT_DATABITS;
    m_Parameter.bStopBits = DEFAULT_STOPBITS;
    m_Parameter.bParity = DEFAULT_PARITY;
    m_Parameter.fDTRDSR = DEFAULT_DTRDSR;
    m_Parameter.fDtrControl = DEFAULT_DTRCTRL;
    m_Parameter.fRTSCTS = DEFAULT_RTSCTS;
    m_Parameter.fRtsControl = DEFAULT_RTSCTRL;
    m_Parameter.fXONXOFF = DEFAULT_XONXOFF;
    m_Parameter.XonChar = DEFAULT_XONCHAR;
    m_Parameter.XoffChar = DEFAULT_XOFFCHAR;
    m_Parameter.evtChar = DEFAULT_EVTCHAR;
    m_Parameter.dwCommMask = DEFAULT_COMMASK;

    m_Parameter.CommTimeOuts.ReadIntervalTimeout = DEFAULT_RDINTERVAL_TOUT;
    m_Parameter.CommTimeOuts.ReadTotalTimeoutConstant = 0;//DEFAULT_RDTOTAL_TOUT;
    m_Parameter.CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
    m_Parameter.CommTimeOuts.WriteTotalTimeoutConstant = DEFAULT_WTTOTAL_TOUT;
    m_Parameter.CommTimeOuts.WriteTotalTimeoutMultiplier = 0;

    CommonInit();
}

CSerialComm::CSerialComm(const char* fileName)
{
    //add code to get info from file.

    CommonInit();
}

CSerialComm::CSerialComm(PortPara portPara)
{
    m_Parameter = portPara;

    CommonInit();
}

//三种构造方式公用的部分
void CSerialComm::CommonInit()
{
    m_nPortID           = m_Parameter.port;   
    m_bPortOpen         = FALSE;
    m_bInterOpen        = FALSE;
    m_hSerialPort       = INVALID_HANDLE_VALUE;
    m_pUser             = NULL;
    m_pThread           = NULL;
    m_dwHubIndex        = 0;
    m_dwPortIndex       = 0;
    
    memset(&m_osRead, 0, sizeof(m_osRead));
    memset(&m_osWrite, 0, sizeof(m_osWrite));
    m_osRead.hEvent     = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_osWrite.hEvent    = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_hWaitEvent        = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_hTXEvent          = CreateEvent(NULL, TRUE, FALSE, NULL);
}

CSerialComm::~CSerialComm()
{
    if(m_bPortOpen)
    {
        Close();
    }

    CloseHandle(m_osRead.hEvent);
    CloseHandle(m_osWrite.hEvent);
    CloseHandle(m_hWaitEvent);
    CloseHandle(m_hTXEvent);
}

const TCHAR* CSerialComm::GetPortName(int portName) 
{ 
    return pstrCommPortName[portName]; 
}

//初始化设定的参数打开串口
BOOL  CSerialComm::Open(PFNFRAMECB pfnFrameCB, LPVOID pUser)
{
    if(TRUE == m_bPortOpen)
    {//该对象已经开启该串口
        TRACE("COM%d have been opened!\n", GetPortID()+1);
        return FALSE;
    }
    
    CUSBPorts myUSBPort;
    if(myUSBPort.FindUsbDeviceByPort(m_nPortID+1))
    {
        m_dwHubIndex = myUSBPort.GetHubIndex();
        m_dwPortIndex = myUSBPort.GetPortIndex();
    }
    else
    {
        m_dwHubIndex = 0;
        m_dwPortIndex = 0;
    }

    m_bPortOpen = InterOpen();
    if(m_bPortOpen)
    {
        m_pfnFrameCB    = pfnFrameCB;
        m_pUser         = pUser;
        m_pThread       = AfxBeginThread( PortMonitorThread, this, THREAD_PRIORITY_NORMAL );
        m_pThread->m_bAutoDelete = TRUE;
    }
    return m_bPortOpen;
}

BOOL  CSerialComm::Close(void)
{
    if(FALSE == m_bPortOpen)
    {
        return FALSE;
    }
    LOG_OUTPUT(_T("CSerialComm::Close Start COM%d\n"),GetPortID()+1);
    m_bPortOpen = FALSE;      //可能会有一个线程监视串口事件，CloseHandle会导致得到一个0x0的事件（不管mask是什么）。
    SetEvent(m_hWaitEvent);

    if(m_bInterOpen)
    {
        SetCommMask(m_hSerialPort,0); 
        while(m_pThread) { Sleep(10); }
        
        PurgeComm(m_hSerialPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);//清除buffer
        CloseHandle(m_hSerialPort);
        m_bInterOpen = FALSE;
    }
    else
    {
        while(m_pThread) { Sleep(10); }
    }
    LOG_OUTPUT(_T("CSerialComm::Close END COM%d\n"),GetPortID()+1);
    return TRUE;
}

BOOL CSerialComm::InterOpen(void)
{
    if(TRUE == m_bInterOpen)
    {
        LOG_OUTPUT(_T("COM%d have been opened!\n"), GetPortID()+1);
        return FALSE;
    }
    
    m_hSerialPort = CreateFile(GetPortName(m_nPortID),
                                     GENERIC_READ | GENERIC_WRITE,
                                     0,	   // Exclusive access
                                     NULL, // no security attrs
                                     OPEN_EXISTING,
                                     FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                                     NULL);
    if(m_hSerialPort == INVALID_HANDLE_VALUE)
    {//该串口已经被其它进程占用或设备不可用
        return FALSE;
    }
    
    //设定串口参数
    m_dcb.DCBlength = sizeof( DCB );
    if(!GetCommState(m_hSerialPort, &m_dcb))  //  获得端口默认设置
    {
        LOG_OUTPUT(_T("Failed to get COM%d DCB!\n"), GetPortID()+1);
        CloseHandle(m_hSerialPort);
        return FALSE;
    }

    //对串口的参数按照m_Parameter的值进行调整
    m_dcb.BaudRate      = m_Parameter.dwBaudRate;
    m_dcb.ByteSize      = m_Parameter.bDataBits;
    m_dcb.StopBits      = m_Parameter.bStopBits;
    m_dcb.Parity        = m_Parameter.bParity;
    m_dcb.fOutxCtsFlow  = m_Parameter.fRTSCTS;
    m_dcb.fRtsControl   = m_Parameter.fRtsControl;
    m_dcb.fOutxDsrFlow  = m_Parameter.fDTRDSR;
    m_dcb.fDtrControl   = m_Parameter.fDtrControl;
    m_dcb.fOutX         = m_Parameter.fXONXOFF;
    m_dcb.fInX          = m_Parameter.fXONXOFF;
    m_dcb.XoffChar      = m_Parameter.XoffChar;
    m_dcb.XonChar       = m_Parameter.XonChar;
    m_dcb.EvtChar       = m_Parameter.evtChar;

    if( !SetCommMask(m_hSerialPort, m_Parameter.dwCommMask)||
        !SetupComm(m_hSerialPort, RXQUEUESIZE, TXQUEUESIZE)||
        !SetCommTimeouts(m_hSerialPort,&m_Parameter.CommTimeOuts)||
        !SetCommState(m_hSerialPort, &m_dcb))
    {
        LOG_OUTPUT(_T("Failed to set COM%d param!\n"),GetPortID()+1);
        CloseHandle(m_hSerialPort);
        return FALSE;
    }
    
    PurgeComm(m_hSerialPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);  //清空buffer
    m_dwBufReadIdx  = 0;
    m_dwBufWriteIdx = 0;
    m_dwBufScanIdx  = 0;
    m_bInterOpen    = TRUE;
    LOG_OUTPUT(_T("CSerialComm::InterOpen Success COM%d\n"),GetPortID()+1);
    return TRUE;
}

BOOL CSerialComm::InterClose(void)
{
    if(FALSE == m_bPortOpen)
    {
        LOG_OUTPUT(_T("COM%d have not been opened by user!\n"), GetPortID()+1);
        return FALSE;
    }
    
    LOG_OUTPUT(_T("CSerialComm::InterClose Start COM%d\n"), GetPortID()+1);
    m_bInterOpen = FALSE;
    SetCommMask(m_hSerialPort,0); 
    PurgeComm(m_hSerialPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);//清除buffer
    CloseHandle(m_hSerialPort);
    LOG_OUTPUT(_T("CSerialComm::InterClose End COM%d\n"), GetPortID()+1);
    return TRUE;
}

BOOL CSerialComm::Read(char* pData, DWORD dwlength, DWORD *pdwReaded)
{
    DWORD dwReaded = 0, dwErrorFlags = 0;//, dwError = 0;
    COMSTAT ComStat;

    if(!GetOpenFlag() || !m_bInterOpen)
    {
        return FALSE;
    }
    
    ClearCommError(GetPortHandle(), &dwErrorFlags, &ComStat);
    dwlength = min((DWORD)dwlength, ComStat.cbInQue);           //选较小的值读取

    if(dwErrorFlags > 0)
    {
        return FALSE;
    }

    if (!ReadFile(m_hSerialPort, pData, dwlength, &dwReaded, &m_osRead)) 
    {
        if (GetLastError() != ERROR_IO_PENDING) 
        { // ReadFile failed, but it isn't delayed. Report error and abort.
            return FALSE;
        }
        else 
        {// Read is pending.
            if(!GetOverlappedResult(m_hSerialPort, &m_osRead, &dwReaded, TRUE))
            {
                return FALSE;
            }
        }
    }

    if(pdwReaded)
    {
        *pdwReaded = dwReaded;
    }
    return TRUE; 
}

BOOL CSerialComm::Write(char* pData, DWORD dwlength, DWORD *pdwWritten)
{
    DWORD dwWritten =0, dwErrorFlags = 0;  
    COMSTAT ComStat;

    if(!GetOpenFlag() || !m_bInterOpen)
    {
        return FALSE;
    }

    ///////////////////////////////////////////////////////////////////////////
    //ClearCommError只能Clear与串口通信相关的错误，如果在之前有其他非串口错误
    //(如ResetEvent产生的ERROR_INVALID_HANDLE错误)发生可能会导致误判串口状态。
    //可以考虑清除所有错误信息。
    ///////////////////////////////////////////////////////////////////////////
    ClearCommError(m_hSerialPort, &dwErrorFlags, &ComStat);   //clear all previous error

    if (FALSE == WriteFile(m_hSerialPort, pData, dwlength, &dwWritten, &m_osWrite)) 
    {
        if (GetLastError() != ERROR_IO_PENDING) 
        { // WriteFile failed, but it isn't delayed. Report error and abort.
            return FALSE;
        }
        else 
        {// Write is pending.         
            if (!GetOverlappedResult(m_hSerialPort, &m_osWrite, &dwWritten, TRUE))
            {
                return FALSE;
            }
        }
    }
    
    if(pdwWritten)
    {
        *pdwWritten = dwWritten;
    }
    return TRUE;
}

void CSerialComm::RXFrame(void)
{
    DWORD dwLen;
    ReadToBuf();
    
    while(m_dwBufScanIdx<m_dwBufWriteIdx)
    {
        if(m_bRXBuf[m_dwBufScanIdx++] == DEFAULT_EVTCHAR)
        {
            dwLen = m_dwBufScanIdx-m_dwBufReadIdx;
            if(dwLen >= 2)
            {
                m_pfnFrameCB(m_pUser,dwLen);
            }
        }
    }
}

void CSerialComm::ReadToBuf(void)
{
    DWORD dwLen, dwReaded = 0,dwErrorFlags = 0;//, dwError = 0;
    COMSTAT ComStat;

    if(!GetOpenFlag() || !m_bInterOpen)
    {
        return;
    }
    
    ClearCommError(GetPortHandle(), &dwErrorFlags, &ComStat);

    if(dwErrorFlags > 0)
    {
        return;
    }
    
    dwLen = (DWORD)(RXBUFSIZE-m_dwBufWriteIdx);
    if(dwLen < ComStat.cbInQue)
    {
        m_dwBufWriteIdx = 0;
        m_dwBufReadIdx  = 0;
        m_dwBufScanIdx  = 0;
    }
    dwLen = min(dwLen, ComStat.cbInQue);           //选较小的值读取
    
    if (!ReadFile(m_hSerialPort, &m_bRXBuf[m_dwBufWriteIdx], dwLen, &dwReaded, &m_osRead)) 
    {
        if (GetLastError() != ERROR_IO_PENDING) 
        { // ReadFile failed, but it isn't delayed. Report error and abort.
            return;
        }
        else 
        {// Read is pending.
            if(!GetOverlappedResult(m_hSerialPort, &m_osRead, &dwReaded, TRUE))
            {
                return;
            }
        }
    }

    m_dwBufWriteIdx += dwReaded;
}

void CSerialComm::ReadFrame(BYTE* pData, int nSize)
{
    if(nSize<0)
    {
        nSize = m_dwBufScanIdx-m_dwBufReadIdx;
    }
    else
    {
        nSize = min(nSize,(int)(m_dwBufScanIdx-m_dwBufReadIdx));
    }

    if(pData)
    {
        memcpy(pData, &m_bRXBuf[m_dwBufReadIdx], nSize);
    }
    m_dwBufReadIdx = m_dwBufScanIdx;
    if(m_dwBufWriteIdx == m_dwBufReadIdx)
    {
        m_dwBufWriteIdx = 0;
        m_dwBufReadIdx  = 0;
        m_dwBufScanIdx  = 0;
    }
}

BOOL CSerialComm::WaitMS(int nMS)
{
    ResetEvent(m_hWaitEvent);
    if(WAIT_OBJECT_0 == WaitForSingleObject(m_hWaitEvent, nMS))
    {
        return FALSE;
    }
    return TRUE;
}
