/*=================================================================================================

FILE : USBPorts.h

SERVICES: Declaration of an MFC wrapper class for USBPorts

GENERAL DESCRIPTION:

PUBLIC CLASSES AND STATIC FUNCTIONS:
CUSBPorts;

INITIALIZATION AND SEQUENCING REQUIREMENTS:

====================================================================================================*/


/*====================================================================================================

==MODIFY HISTOY FOR FILE==

when         who     what, where, why
----------   ---     ----------------------------------------------------------
22/08/2013   JYH      create

=====================================================================================================*/
#if !defined(_USBPORTS_H__)
#define _USBPORTS_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include <windows.h>
#include <basetyps.h>

#include <setupapi.h>
#include <commctrl.h>      
#include <usbioctl.h>
#include <usbiodef.h>
#include "usb100.h"
#include "usb200.h"
#include <winioctl.h>

/*===========================================================================

DEFINITIONS AND CONSTANTS

===========================================================================*/
#define MAX_PORTNAME_LEN            64
#define MAX_USBNAME_LEN             2048
#define NUM_HCS_TO_CHECK            10
/*===========================================================================

TYPE DECLARATIONS

===========================================================================*/

typedef struct _STRING_DESCRIPTOR_NODE
{
    struct _STRING_DESCRIPTOR_NODE *Next;
    UCHAR                           DescriptorIndex;
    USHORT                          LanguageID;
    USB_STRING_DESCRIPTOR          *StringDescriptor;
} STRING_DESCRIPTOR_NODE, *PSTRING_DESCRIPTOR_NODE;

// Common Class Interface Descriptor
//
typedef struct _USB_INTERFACE_DESCRIPTOR2 {
    UCHAR  bLength;             // offset 0, size 1
    UCHAR  bDescriptorType;     // offset 1, size 1
    UCHAR  bInterfaceNumber;    // offset 2, size 1
    UCHAR  bAlternateSetting;   // offset 3, size 1
    UCHAR  bNumEndpoints;       // offset 4, size 1
    UCHAR  bInterfaceClass;     // offset 5, size 1
    UCHAR  bInterfaceSubClass;  // offset 6, size 1
    UCHAR  bInterfaceProtocol;  // offset 7, size 1
    UCHAR  iInterface;          // offset 8, size 1
    USHORT wNumClasses;         // offset 9, size 2
} USB_INTERFACE_DESCRIPTOR2, *PUSB_INTERFACE_DESCRIPTOR2;

/*===========================================================================

CLASS DEFINITIONS

===========================================================================*/
class CUSBPorts
{
public:
    // Construction and Destruction
    CUSBPorts(void);
    virtual ~CUSBPorts(void);
    
    bool                FindUsbDeviceByPort(const int nPortID, ULONG nCmpLevel = 2);
    bool                FindUsbDeviceByDevPath(TCHAR *pDevPath, const GUID *pUSBClass, ULONG nCmpLevel = 2);
    bool                GetUsbDeviceDetails(HDEVINFO hardware_dev_info, PSP_DEVICE_INTERFACE_DATA dev_info_data,PSP_DEVICE_INTERFACE_DETAIL_DATA* dev_info_detail_data);
    bool                GetUsbDeviceName(HDEVINFO hardware_dev_info, PSP_DEVICE_INTERFACE_DATA dev_info_data,CString* name);
    bool                GetUsbDriverName(HDEVINFO hardware_dev_info, PSP_DEVINFO_DATA dev_info_data,CString* name);
    bool                GetUsbDevicePortName(HDEVINFO hardware_dev_info, PSP_DEVINFO_DATA dev_info_data, CString* pszPortName);
    CString&            GetUsbPath(void)    {return m_UsbName;};
    ULONG               GetHubIndex(void)   {return m_HubIndex;};
    ULONG               GetPortIndex(void)  {return m_PortIndex;};
    bool                IsSamePort(ULONG nHubIndex, ULONG nPortIndex)   {return (m_bInited&&(nHubIndex == m_HubIndex)&&(nPortIndex == m_PortIndex));};
    
    bool                EnumUsbDeviceByIndexInit(const GUID *pUSBClass, ULONG nHubIndex, ULONG nPortIndex, ULONG nCmpLevel = 2);
    bool                EnumUsbDeviceByIndexNext(const GUID *pUSBClass);
    void                EnumUsbDeviceByIndexEnd(void);
private:
    PTSTR               WideStrToMultiStr(LPCWSTR WideStr);
    bool                SyncUsbDevicePositon(void);
    bool                EnumerateHostController(HANDLE hHCDev);
    BOOL                EnumerateHub (PTSTR HubName);
    BOOL                EnumerateHubPorts (HANDLE hHubDevice, ULONG NumPorts);
    PTSTR               GetHCDDriverKeyName(HANDLE HCD);
    PTSTR               GetRootHubName(HANDLE HostController);
    BOOL                AreThereStringDescriptors (PUSB_DEVICE_DESCRIPTOR  DeviceDesc, PUSB_CONFIGURATION_DESCRIPTOR ConfigDesc);
    PTSTR               GetExternalHubName(HANDLE Hub, ULONG ConnectionIndex);
    PTSTR               GetDriverKeyName(HANDLE Hub, ULONG ConnectionIndex);
    bool                GetParentDriverName(CString *pszDriverName, CString *pszParentDriverName);

    PUSB_DESCRIPTOR_REQUEST GetConfigDescriptor(HANDLE hHubDevice, ULONG ConnectionIndex, UCHAR DescriptorIndex);
    PSTRING_DESCRIPTOR_NODE GetAllStringDescriptors (HANDLE                          hHubDevice,
                                                     ULONG                           ConnectionIndex,
                                                     PUSB_DEVICE_DESCRIPTOR          DeviceDesc,
                                                     PUSB_CONFIGURATION_DESCRIPTOR   ConfigDesc);
    PSTRING_DESCRIPTOR_NODE GetStringDescriptors (HANDLE  hHubDevice,
                                                  ULONG   ConnectionIndex,
                                                  UCHAR   DescriptorIndex,
                                                  ULONG   NumLanguageIDs,
                                                  USHORT  *LanguageIDs,
                                                  PSTRING_DESCRIPTOR_NODE StringDescNodeTail);
    PSTRING_DESCRIPTOR_NODE GetStringDescriptor (HANDLE  hHubDevice,
                                                 ULONG   ConnectionIndex,
                                                 UCHAR   DescriptorIndex,
                                                 USHORT  LanguageID);
private:
    bool                m_bInited;
    CString             m_UsbName;
    CString             m_UsbDriverName;
    CString             m_UsbParentDriverName;
    CString             m_UsbRootDriverName;
    ULONG               m_HubIndex;
    ULONG               m_PortIndex;
    ULONG               m_CompareLevel;

    //Enum Used
    HDEVINFO            m_pEnumDevInfo;
    ULONG               m_EnumHubIndex;
    ULONG               m_EnumPortIndex;
    ULONG               m_EnumIndex;
};
#endif // !defined(_USBPORTS_H__)
