#include "StdAfx.h"
#include "DloadConfig.h"
#include "Shlwapi.h"

CDloadConfig *CDloadConfig::m_pDloadConfig  = NULL;
int           CDloadConfig::m_nRefCnt       = 0;

int CDloadConfig::Create(CDloadConfig **ppDloadConfig)
{
    if(m_nRefCnt == 0)
    {
        m_pDloadConfig = new CDloadConfig();
        if(!ppDloadConfig) m_nRefCnt++;
    }
    
    if(ppDloadConfig)
    {
        *ppDloadConfig = m_pDloadConfig;
        m_nRefCnt++;
    }
    return m_nRefCnt;
}

int CDloadConfig::Release(void)
{
    if(m_nRefCnt == 0)
    {
        return 0;
    }
    else if(m_nRefCnt == 1 && m_pDloadConfig)
    {
        delete m_pDloadConfig;
        m_pDloadConfig = NULL;
    }
    m_nRefCnt--;
    return m_nRefCnt;
}

CDloadConfig::CDloadConfig(void)
{
    CString szTemp;

    ResetConfig();
    m_bLocked = FALSE;
    m_pIniFile = new CIniFile(DLOAD_CFG_FILE);
    CChipset::Create(&m_pChipset);
    
    for(int i=0;i<COM_MAX;i++)
    {
        m_pCOMList[i] = COM_MAX;
    }
    
    for(int i=0;i<DLOAD_HISTORY_NUM;i++)
    {
        szTemp.Format(DLOAD_CFG_MBNPATH_KEY, i);
        SetMBNPath(m_pIniFile->GetValue(DLOAD_CFG_DLOAD_SECTION, szTemp), i, FALSE);
    }
    
    if(m_szMBNPath.GetCount()>0 && PathFileExists(GetMBNPath()+DLOAD_CFG_FILE))
    {
        m_pCustIniFile = new CIniFile(GetMBNPath()+DLOAD_CFG_FILE);
    }
    else
    {
        m_pCustIniFile = NULL;
    }

    m_nCOMNum = m_pIniFile->GetIntValue(DLOAD_CFG_COM_SECTION, DLOAD_CFG_COMNUM_KEY,10);
    for(int i=0;i<m_nCOMNum;i++)
    {
        szTemp.Format(DLOAD_CFG_COMNO_KEY, i);
        m_pCOMList[i] = m_pIniFile->GetIntValue(DLOAD_CFG_COM_SECTION, szTemp, COM_MAX);
    }
    
    m_bAutoDLoad    = (BOOL)m_pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_AUTODLOAD_KEY,   m_bAutoDLoad);
    m_bLogEnable    = (BOOL)m_pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_LOGENABLE_KEY,   m_bLogEnable);
    m_bLocked       = (BOOL)m_pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_LOCKED_KEY,      m_bLocked);
    if(m_pCustIniFile)
    {
        LoadIniConfig(m_pCustIniFile);
    }
    else
    {
        LoadIniConfig(m_pIniFile);
    }
}

CDloadConfig::~CDloadConfig(void)
{
    m_pIniFile->SetIntValue(DLOAD_CFG_COM_SECTION, DLOAD_CFG_COMNUM_KEY, m_nCOMNum);

    CString szTemp;
    for(int i=0;i<m_nCOMNum;i++)
    {
        szTemp.Format(DLOAD_CFG_COMNO_KEY, i);
        m_pIniFile->SetIntValue(DLOAD_CFG_COM_SECTION, szTemp, m_pCOMList[i]);
    }
    
    for(int i=0;i<m_szMBNPath.GetCount();i++)
    {
        szTemp.Format(DLOAD_CFG_MBNPATH_KEY, i);
        m_pIniFile->SetValue(DLOAD_CFG_DLOAD_SECTION, szTemp, m_szMBNPath[i]);
    }

    m_pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_AUTODLOAD_KEY,   m_bAutoDLoad);
    m_pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_LOGENABLE_KEY,   m_bLogEnable);
    m_pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_LOCKED_KEY,      m_bLocked);
    
    SaveIniConfig(m_pIniFile);
    
    if(m_pCustIniFile)
    {
        SaveIniConfig(m_pCustIniFile);
        delete m_pCustIniFile;
    }
    if(m_pIniFile)
    {
        delete m_pIniFile;
    }
    CChipset::Release();
}

void CDloadConfig::LoadIniConfig(CIniFile *pIniFile)
{
    if(!pIniFile)
    {
        return;
    }
    m_nChipsetIdx   = pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_CHIPSET_KEY);
    SetNVPath(pIniFile->GetValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVPATH_KEY, DLOAD_DEFAULT_NVPATH));
    SetNVFile(pIniFile->GetValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVFILE_KEY));
    SetEFSPath(pIniFile->GetValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_EFSPATH_KEY, DLOAD_DEFAULT_EFSPATH));
    
    m_bDloadMBN         = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_MBN_KEY,         m_bDloadMBN);
    m_bDloadFACT        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FACT_KEY,        m_bDloadFACT);
    m_bEraseFlash       = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ERASE_KEY,       m_bEraseFlash);
    m_bDloadPBL         = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_PBL_KEY,         m_bDloadPBL);
    m_bDloadQCSBL       = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_QCSBL_KEY,       m_bDloadQCSBL);
    m_bDloadOEMSBL      = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_OEMSBL_KEY,      m_bDloadOEMSBL);
    m_bDloadAMSS        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_AMSS_KEY,        m_bDloadAMSS);
    m_bDloadAPPS        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_APPS_KEY,        m_bDloadAPPS);
    m_bDloadOBL         = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_OBL_KEY,         m_bDloadOBL);
    m_bDloadFOTAUI      = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FOTAUI_KEY,      m_bDloadFOTAUI);
    m_bDloadCEFS        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_CEFS_KEY,        m_bDloadCEFS);
    m_bDloadAPPSBL      = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_APPSBL_KEY,      m_bDloadAPPSBL);
    m_bDloadAPPSCEFS    = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_APPS_CEFS_KEY,   m_bDloadAPPSCEFS);
    m_bDloadFLASHBIN    = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FLASH_BIN_KEY,   m_bDloadFLASHBIN);
    m_bDloadDSP1        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_DSP1_KEY,        m_bDloadDSP1);
    m_bDloadCUSTOM      = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_CUSTOM_KEY,      m_bDloadCUSTOM);
    m_bDloadDBL         = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_DBL_KEY,         m_bDloadDBL);
    m_bDloadFSBL        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FSBL_KEY,        m_bDloadFSBL);
    m_bDloadOSBL        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_OSBL_KEY,        m_bDloadOSBL);
    m_bDloadDSP2        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_DSP2_KEY,        m_bDloadDSP2);
    m_bDloadRAW         = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_RAW_KEY,         m_bDloadRAW);
    m_bDloadMsimage     = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_MSIMAGE_KEY,     m_bDloadMsimage);
    m_bDloadARD         = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARD_KEY,         m_bDloadARD);
    m_bDloadARDBOOT     = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDBOOT_KEY,     m_bDloadARDBOOT);
    m_bDloadARDSYS      = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDSYS_KEY,      m_bDloadARDSYS);
    m_bDloadARDUSER     = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDUSER_KEY,     m_bDloadARDUSER);
    m_bDloadARDRCY      = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDRCY_KEY,      m_bDloadARDRCY);
    m_bDloadARDCLRCACHE = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDCLRCACHE_KEY, m_bDloadARDCLRCACHE);
    m_bNVBackup         = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVBACKUP_KEY,    m_bNVBackup);
    m_bNVRestore        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVRESTORE_KEY,   m_bNVRestore);
    m_bEFSBackup        = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_EFSBACKUP_KEY,   m_bEFSBackup);
    m_bEFSRestore       = (BOOL)pIniFile->GetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_EFSRESTORE_KEY,  m_bEFSRestore);
}

void CDloadConfig::SaveIniConfig(CIniFile *pIniFile)
{
    if(!pIniFile)
    {
        return;
    }
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_CHIPSET_KEY,   m_nChipsetIdx);
    pIniFile->SetValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVPATH_KEY,       m_szNVPath);
    pIniFile->SetValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVFILE_KEY,       m_szNVFile);
    pIniFile->SetValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_EFSPATH_KEY,      m_szEFSPath);
    
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_MBN_KEY,         m_bDloadMBN);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FACT_KEY,        m_bDloadFACT);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ERASE_KEY,       m_bEraseFlash);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_PBL_KEY,         m_bDloadPBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_QCSBL_KEY,       m_bDloadQCSBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_OEMSBL_KEY,      m_bDloadOEMSBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_AMSS_KEY,        m_bDloadAMSS);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_APPS_KEY,        m_bDloadAPPS);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_OBL_KEY,         m_bDloadOBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FOTAUI_KEY,      m_bDloadFOTAUI);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_CEFS_KEY,        m_bDloadCEFS);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_APPSBL_KEY,      m_bDloadAPPSBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_APPS_CEFS_KEY,   m_bDloadAPPSCEFS);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FLASH_BIN_KEY,   m_bDloadFLASHBIN);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_DSP1_KEY,        m_bDloadDSP1);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_CUSTOM_KEY,      m_bDloadCUSTOM);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_DBL_KEY,         m_bDloadDBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_FSBL_KEY,        m_bDloadFSBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_OSBL_KEY,        m_bDloadOSBL);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_DSP2_KEY,        m_bDloadDSP2);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_RAW_KEY,         m_bDloadRAW);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_MSIMAGE_KEY,     m_bDloadMsimage);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARD_KEY,         m_bDloadARD);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDBOOT_KEY,     m_bDloadARDBOOT);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDSYS_KEY,      m_bDloadARDSYS);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDUSER_KEY,     m_bDloadARDUSER);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDRCY_KEY,      m_bDloadARDRCY);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_ARDCLRCACHE_KEY, m_bDloadARDCLRCACHE);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVBACKUP_KEY,    m_bNVBackup);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_NVRESTORE_KEY,   m_bNVRestore);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_EFSBACKUP_KEY,   m_bEFSBackup);
    pIniFile->SetIntValue(DLOAD_CFG_DLOAD_SECTION, DLOAD_CFG_EFSRESTORE_KEY,  m_bEFSRestore);
}

void CDloadConfig::ResetConfig(void)
{
    m_szNVFile.Empty();
    m_szNVPath          = DLOAD_DEFAULT_NVPATH;
    m_szEFSPath         = DLOAD_DEFAULT_EFSPATH;
    m_bDloadMBN         = TRUE;
    m_bDloadFACT        = FALSE;
    m_bEraseFlash       = FALSE;
    m_bDloadPBL         = TRUE;
    m_bDloadQCSBL       = TRUE;
    m_bDloadOEMSBL      = TRUE;
    m_bDloadAMSS        = TRUE;
    m_bDloadAPPS        = FALSE;
    m_bDloadOBL         = FALSE;
    m_bDloadFOTAUI      = FALSE;
    m_bDloadCEFS        = FALSE;
    m_bDloadAPPSBL      = TRUE;
    m_bDloadAPPSCEFS    = FALSE;
    m_bDloadFLASHBIN    = FALSE;
    m_bDloadDSP1        = FALSE;
    m_bDloadCUSTOM      = FALSE;
    m_bDloadDBL         = TRUE;
    m_bDloadFSBL        = TRUE;
    m_bDloadOSBL        = TRUE;
    m_bDloadDSP2        = FALSE;
    m_bDloadRAW         = FALSE;
    m_bDloadMsimage     = TRUE;
    m_bDloadARD         = TRUE;
    m_bDloadARDBOOT     = TRUE;
    m_bDloadARDSYS      = TRUE;
    m_bDloadARDUSER     = TRUE;
    m_bDloadARDRCY      = TRUE;
    m_bDloadARDCLRCACHE = TRUE;
    m_bNVBackup         = FALSE;
    m_bNVRestore        = FALSE;
    m_bEFSBackup        = FALSE;
    m_bEFSRestore       = FALSE;
    m_bAutoDLoad        = FALSE;
    m_bLogEnable        = FALSE;
    m_bMemDump          = FALSE;
    m_dwStartAddr       = 0;
    m_dwReadSize        = 0;
}

void CDloadConfig::SetMBNPath(const CString &szPath, int nIdx, BOOL bUpdateIni)
{
    if(szPath.IsEmpty())
    {
        return;
    }

    for(int i=0; i<m_szMBNPath.GetCount(); i++)
    {
        if(m_szMBNPath[i] == szPath)
        {
            m_szMBNPath.RemoveAt(i);
        }
    }

    if(m_szMBNPath.GetCount()>= DLOAD_HISTORY_NUM)
    {
        m_szMBNPath.RemoveAt(DLOAD_HISTORY_NUM-1);
    }

    m_szMBNPath.InsertAt(nIdx, szPath);

    if(m_szMBNPath[nIdx].GetAt(m_szMBNPath[nIdx].GetLength()-1) != '\\')
    {
        m_szMBNPath[nIdx].AppendChar('\\');
    }
    
    if(bUpdateIni)
    {
        if(m_pCustIniFile)
        {
            delete m_pCustIniFile;
            m_pCustIniFile = NULL;
        }

        if(m_szMBNPath.GetCount()>0 && PathFileExists(GetMBNPath()+DLOAD_CFG_FILE))
        {
            m_pCustIniFile = new CIniFile(GetMBNPath()+DLOAD_CFG_FILE);
            LoadIniConfig(m_pCustIniFile);
        }
    }
}

void CDloadConfig::SetNVPath(const CString &szPath)
{
    m_szNVPath = szPath;
    if(m_szNVPath.GetAt(m_szNVPath.GetLength()-1) != '\\')
    {
        m_szNVPath.AppendChar('\\');
    }
    CreateDirectory(m_szNVPath, NULL);
}

void CDloadConfig::SetEFSPath(const CString &szPath)   
{
    m_szEFSPath = szPath;
    if(m_szEFSPath.GetAt(m_szEFSPath.GetLength()-1) != '\\')
    {
        m_szEFSPath.AppendChar('\\');
    }
    CreateDirectory(m_szEFSPath, NULL);
}

int CDloadConfig::GetCOMIdxByID(int nPort)
{
    for(int i=0;i<m_nCOMNum;i++)
    {
        if(m_pCOMList[i] == nPort)
        {
            return i;
        }
    }
    return COM_MAX;
}

int CDloadConfig::GetCOMAvailNum(void)
{
    int nCount = 0;
    for(int i=0;i<COM_MAX;i++)
    {
        if(m_pCOMList[i] < COM_MAX)
        {
            nCount++;
        }
    }
    return GetCOMNum()<nCount?GetCOMNum():nCount;
}

CQcnFile& CDloadConfig::GetSampleQcn(void)
{
    CString szSampleFile = GetNVSample();
    m_SampleQcn.Close();
    m_SampleQcn.Open(szSampleFile);
    return m_SampleQcn;
}

CQcnFile& CDloadConfig::GetNVQcn(void)
{
    CString szNVFile = GetNVFile();
    m_NVQcn.Close();
    m_NVQcn.Open(szNVFile);
    return m_NVQcn;
}