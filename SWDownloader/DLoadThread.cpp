#include "StdAfx.h"
#include "DLoadThread.h"
#include "QcnFile.h"
#include "Log.h"
#include <shlwapi.h>
#include <winioctl.h>

static const GUID udisk_usb_class_id = GUID_DEVINTERFACE_DISK;

DLoadThread::DLoadThread(void)
{
    LOG_OUTPUT(_T("DLoadThread Start\n"));
    m_WaitEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_bRunning  = FALSE;
    m_pThread   = NULL;
    m_bResetting= FALSE;
    m_dwStartTimeMS = 0;
    m_dwWorkTimeMS  = 0;
    CDloadConfig::Create(&m_pDloadConfig);
    COMManager::Create(&m_pCOMMgr);
    LOG_OUTPUT(_T("DLoadThread END\n"));
}

DLoadThread::~DLoadThread(void)
{
    LOG_OUTPUT(_T("~DLoadThread Start\n"));
    Stop();
    CloseHandle(m_WaitEvent);
    CDloadConfig::Release();
    COMManager::Release();
    LOG_OUTPUT(_T("~DLoadThread End\n"));
}

BOOL DLoadThread::Start(int nPort)
{
    if(m_bRunning)
    {
        return TRUE;
    }
    
    m_bRunning      = TRUE;
    m_bUserCancel   = FALSE;
    m_nWaitTime     = 0;
    m_bTimeOut      = FALSE;
    m_nPort         = nPort;
    m_nSwitchPort   = COM_MAX;
    MOVE_TO_STEP(DLOAD_STEP_INIT);

    LOG_OUTPUT(_T("DLoadThread::Start Start\n"));
	m_pThread       = AfxBeginThread(DLoadThreadProc, this);
	m_pThread->m_bAutoDelete = TRUE;
    LOG_OUTPUT(_T("DLoadThread::Start END\n"));
    return TRUE;
}
void DLoadThread::SwitchPort(int nPort)
{
    if(m_nPort != nPort)
    {
        m_nSwitchPort = nPort;
    }
}

BOOL DLoadThread::Stop(void)
{
    if(!m_bRunning)
    {
        return TRUE;
    }
    LOG_OUTPUT(_T("DLoadThread::Stop Start\n"));
    m_bRunning = FALSE;
    m_PhoneDiag.SetFastBootMode(FALSE);
    m_PhoneDiag.StopDiag();
    m_pCOMMgr->SetUsed(m_nPort, FALSE, this);

    // 退出可能的等待函数
    SetEvent(m_WaitEvent);
    // 等待线程完全退出
    while(m_pThread) { Sleep(10);}
    LOG_OUTPUT(_T("DLoadThread::Stop End\n"));
    return TRUE;
}

void DLoadThread::RunFSM(void)
{
    LOG_OUTPUT(_T("DLoadThread::RunFSM Start\n"));
    ResetEvent(m_WaitEvent);
    for(;;)
    {
        if(!m_bRunning)
        {
            m_bUserCancel = TRUE;
            break;
        }
        
        if(FALSE == ProcessStep(m_nCurrStep))
        {
            if(m_bRunning)
            {
                // Wait Message to go on
                m_bTimeOut = WaitMsg();
                if(m_bTimeOut)
                {
                    m_nWaitTime = 0; 
                    break;
                }
            }
            else
            {
                m_nWaitTime = 0; 
            }
        }
        else
        {
            SendProgressMsg();
            m_bTimeOut  = FALSE;
            m_nWaitTime = 0;
        }

        if(m_nCurrStep == DLOAD_STEP_EXIT)
        {
            break;
        }
    }
    
    m_bRunning = FALSE;
    ProcessStep(DLOAD_STEP_EXIT);
    LOG_OUTPUT(_T("DLoadThread::RunFSM End\n"));
}


// Step Functions
BOOL DLoadThread::ProcessStep(int nStep)
{
    LOG_OUTPUT(_T("DLoadThread::ProcessStep COM%d-COM%d %d %d %d\n"),m_nPort+1,m_nSwitchPort+1,nStep,m_nWaitTime,m_nStepRet);
    if(m_nSwitchPort != COM_MAX && m_nSwitchPort != m_nPort)
    {
        m_pCOMMgr->SetUsed(m_nPort, FALSE, this);
        m_PhoneDiag.StopDiag();

        if(TRUE == m_pCOMMgr->SetUsed(m_nSwitchPort, TRUE, this))
        {
            if(TRUE == m_PhoneDiag.StartDiag(m_nSwitchPort))
            {
                LOG_OUTPUT(_T("Success Switch Port to COM%d from COM%d!\n"),m_nSwitchPort+1,m_nPort+1);
                m_nPort = m_nSwitchPort;
                m_nSwitchPort = COM_MAX;
                m_UsbPorts.FindUsbDeviceByPort(m_nPort+1);
            }
            else
            {
                m_pCOMMgr->SetUsed(m_nSwitchPort, FALSE, this);
                LOG_OUTPUT(_T("Failed Switch Port to COM%d from COM%d!\n"),m_nSwitchPort+1,m_nPort+1);
            }
        }
    }

    switch(nStep){
    case DLOAD_STEP_INIT:
        return StepInit();

    case DLOAD_STEP_COMCONNECT:
        return StepCOMConnect();

    case DLOAD_STEP_NVBACKUP:
        return StepNVBackup();

    case DLOAD_STEP_EFSBACKUP:
        return StepEFSBackup();

    case DLOAD_STEP_DLOADMODE:
        return StepDLoadMode();

    case DLOAD_STEP_DLOADEMMC:
        return StepDLoadEMMC();

    case DLOAD_STEP_RUNARMPRG:
        return StepRunARMPRG();

    case DLOAD_STEP_DLOADINGMBN:
        return StepDloadMBN();

    case DLOAD_STEP_DLOADINGARD:
        return StepDloadARD();

    case DLOAD_STEP_EFSRESTORE:
        return StepEFSRestore();

    case DLOAD_STEP_NVRESTORE:
        return StepNVRestore();

    case DLOAD_STEP_RESET:
        return StepReset();

    case DLOAD_STEP_EXIT:
        return StepExit();

    default:
        break;
    }
    return TRUE;
}

BOOL DLoadThread::StepInit(void)
{
    switch(m_nStepRet){
    case DLOAD_STEPRET_INIT:
        m_nStatus   = DLOAD_STATUS_SUCCESS;
        m_bNeedReset= FALSE;
        m_bWaitReset= FALSE;
        m_Curr  = 0;
        m_Total = 0;
        m_szProgName.Empty();
        m_szNVAuto.Empty();
        SendProgressMsg();
        m_bNVRestore  = m_pDloadConfig->IsNVRestore();
        m_bEFSRestore = m_pDloadConfig->IsEFSRestore();
        
        if(TRUE == m_pCOMMgr->SetUsed(m_nPort, TRUE, this))
        {
            if(TRUE == m_PhoneDiag.StartDiag(m_nPort))
            {
                break;
            }
            else
            {
                m_pCOMMgr->SetUsed(m_nPort, FALSE, this);
            }
        }
        
        m_UsbPorts.FindUsbDeviceByPort(m_nPort+1);
        if(m_pDloadConfig->IsDloadARD() 
        && !m_pDloadConfig->IsDloadMBN()
        && !(m_pDloadConfig->IsMemDump()&& m_pDloadConfig->GetReadSize())
        && !m_pDloadConfig->IsNVBackup()
        && !m_pDloadConfig->IsEFSBackup())
        {
            m_dwStartTimeMS = GetTickCount();
            m_dwWorkTimeMS  = 0;
            MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
            LOG_OUTPUT(_T("DLoadThread::StepInit DLOAD_STEP_DLOADINGARD\n"));
            return TRUE;
        }

        STEP_RET(DLOAD_STEPRET_RETRY);
        return FALSE;
        
    default:
        if(TRUE == m_pCOMMgr->SetUsed(m_nPort, TRUE, this))
        {
            if(TRUE == m_PhoneDiag.StartDiag(m_nPort))
            {
                break;
            }
            else
            {
                m_UsbPorts.FindUsbDeviceByPort(m_nPort+1);
                m_pCOMMgr->SetUsed(m_nPort, FALSE, this);
                if(m_pDloadConfig->IsDloadARD())
                {
                    m_Fastboot.SetHubPortIndex(m_UsbPorts.GetHubIndex(),m_UsbPorts.GetPortIndex());
                    if(m_Fastboot.Exec(NULL,this) == 0)
                    {
                        MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
                        LOG_OUTPUT(_T("DLoadThread::StepInit DLOAD_STEP_DLOADINGARD\n"));
                        return TRUE;
                    }
                }
            }
        }

        m_nWaitTime = 0;
        return FALSE;
    }
    
    m_UsbPorts.FindUsbDeviceByPort(m_nPort+1);
    if(m_pDloadConfig->IsDloadARD() 
    && !m_pDloadConfig->IsDloadMBN()
    && !(m_pDloadConfig->IsMemDump()&& m_pDloadConfig->GetReadSize())
    && !m_pDloadConfig->IsNVBackup()
    && !m_pDloadConfig->IsEFSBackup())
    {
        m_dwStartTimeMS = GetTickCount();
        m_dwWorkTimeMS  = 0;
        MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
        LOG_OUTPUT(_T("DLoadThread::StepInit DLOAD_STEP_DLOADINGARD\n"));
        return TRUE;
    }

    LOG_OUTPUT(_T("DLoadThread::StepInit DLOAD_STEP_COMCONNECT\n"));
    MOVE_TO_STEP(DLOAD_STEP_COMCONNECT);
    return TRUE;
}

BOOL DLoadThread::StepCOMConnect(void)
{
    int status = m_PhoneDiag.ConnectPhone();
    
    m_dwStartTimeMS = GetTickCount();
    m_dwWorkTimeMS  = 0;
    switch(status){
    case DIAG_PHONEMODE_CDMA:
    case DIAG_PHONEMODE_WCDMA:
        break;
    
    case DIAG_PHONEMODE_DLOAD:
    case DIAG_PHONEMODE_EDLOAD:
        MOVE_TO_STEP(DLOAD_STEP_RUNARMPRG);
        LOG_OUTPUT(_T("DLoadThread::StepCOMConnect DLOAD_STEP_RUNARMPRG\n"));
        return TRUE;

    case DIAG_PHONEMODE_ARMPRG:
        MOVE_TO_STEP(DLOAD_STEP_DLOADINGMBN);
        LOG_OUTPUT(_T("DLoadThread::StepCOMConnect DLOAD_STEP_DLOADINGMBN\n"));
        return TRUE;
        
    case DIAG_PHONEMODE_FASTBOOT:
        MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
        LOG_OUTPUT(_T("DLoadThread::StepCOMConnect DLOAD_STEP_DLOADINGARD\n"));
        return TRUE;

    default:
        if(m_pDloadConfig->IsDloadARD())
        {
            m_Fastboot.SetHubPortIndex(m_UsbPorts.GetHubIndex(),m_UsbPorts.GetPortIndex());
            if(m_Fastboot.Exec(NULL,this) == 0)
            {
                MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
                LOG_OUTPUT(_T("DLoadThread::StepCOMConnect DLOAD_STEP_DLOADINGARD\n"));
                return TRUE;
            }
        }
        m_nWaitTime = 0;
        return FALSE;
    }

    MOVE_TO_STEP(DLOAD_STEP_NVBACKUP);
    LOG_OUTPUT(_T("DLoadThread::StepCOMConnect DLOAD_STEP_NVBACKUP\n"));
    return TRUE;
}

BOOL DLoadThread::StepNVBackup(void)
{
    if(m_pDloadConfig->IsNVBackup())
    {
        if(FALSE == NVBackup(m_pDloadConfig->GetNVPath()))
        {
            m_nStatus = DLOAD_STATUS_NVBACKUP;
            MOVE_TO_STEP(DLOAD_STEP_EXIT);
            return TRUE;
        }
    }
    MOVE_TO_STEP(DLOAD_STEP_EFSBACKUP);
    return TRUE;
}

BOOL DLoadThread::StepEFSBackup(void)
{
    if(m_pDloadConfig->IsEFSBackup())
    {
        if(FALSE == EFSBackup(m_pDloadConfig->GetEFSPath()))
        {
            m_nStatus = DLOAD_STATUS_EFSBACKUP;
            MOVE_TO_STEP(DLOAD_STEP_EXIT);
            return TRUE;
        }
    }
    
    MOVE_TO_STEP(DLOAD_STEP_DLOADMODE);
    return TRUE;
}

BOOL DLoadThread::StepDLoadMode(void)
{
    if(m_pDloadConfig->IsDloadMBN()||(m_pDloadConfig->IsMemDump()&& m_pDloadConfig->GetReadSize()))
    {
        int status;
        
        switch(m_nStepRet){
        case DLOAD_STEPRET_INIT:
            m_nWaitTime = 0;
            m_nStatus = DLOAD_STATUS_FAILEDTODLOAD;
            status = COMManager::GetCurrentPhoneMode(m_PhoneDiag.GetPortID());
            switch(status){
            case DIAG_PHONEMODE_CDMA:
            case DIAG_PHONEMODE_WCDMA:
                m_PhoneDiag.SendSwitchToDLoadReq();
                STEP_RET(DLOAD_STEPRET_RETRY);
                return FALSE;

            case DIAG_PHONEMODE_DLOAD:
            case DIAG_PHONEMODE_EDLOAD:
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                break;
                
            case DIAG_PHONEMODE_ARMPRG:
                MOVE_TO_STEP(DLOAD_STEP_DLOADINGMBN);
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                return TRUE;

            case DIAG_PHONEMODE_FASTBOOT:
                MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                return TRUE;

            default:
                m_nStatus = DLOAD_STATUS_FAILEDTODLOAD;
                STEP_RET(DLOAD_STEPRET_RETRY);
                LOG_OUTPUT(_T("DLoadThread::StepDLoadMode FAILEDTODLOAD\n"));
                return FALSE;
            }
            break;
            
        default:
            status = m_PhoneDiag.ConnectPhone();
            switch(status){
            case DIAG_PHONEMODE_CDMA:
            case DIAG_PHONEMODE_WCDMA:
                m_nStatus = DLOAD_STATUS_FAILEDTODLOAD;
                m_PhoneDiag.SendSwitchToDLoadReq();
                return FALSE;

            case DIAG_PHONEMODE_DLOAD:
            case DIAG_PHONEMODE_EDLOAD:
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                break;
                
            case DIAG_PHONEMODE_ARMPRG:
                MOVE_TO_STEP(DLOAD_STEP_DLOADINGMBN);
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                return TRUE;

            case DIAG_PHONEMODE_FASTBOOT:
                MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                return TRUE;

            default:
                m_nStatus = DLOAD_STATUS_FAILEDTODLOAD;
                LOG_OUTPUT(_T("DLoadThread::StepDLoadMode FAILEDTODLOAD\n"));
                return FALSE;
            }
            break;
        }
    }
    MOVE_TO_STEP(DLOAD_STEP_RUNARMPRG);
    return TRUE;
}

BOOL DLoadThread::StepRunARMPRG(void)
{
    if(m_pDloadConfig->IsDloadMBN()||(m_pDloadConfig->IsMemDump()&& m_pDloadConfig->GetReadSize()))
    {
        int status = 0;
        
        switch(m_nStepRet){
        case DLOAD_STEPRET_INIT:
            m_nWaitTime = 0;
            m_bDLoadedARMPrg = FALSE;
            m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
            status = COMManager::GetCurrentPhoneMode(m_PhoneDiag.GetPortID());
            STEP_RET(DLOAD_STEPRET_RETRY);
            if(status == DIAG_PHONEMODE_ARMPRG)
            {
                return FALSE;
            }
            break;
            
        default:
            status = m_PhoneDiag.ConnectPhone();
            break;
        }
        
        switch(status){
        case DIAG_PHONEMODE_CDMA:
        case DIAG_PHONEMODE_WCDMA:
            if(!m_bDLoadedARMPrg)
            {
                m_PhoneDiag.SendSwitchToDLoadReq();
            }
            m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
            LOG_OUTPUT(_T("DLoadThread::StepRunARMPRG INLINE %d****\n"),m_bDLoadedARMPrg);
            return FALSE;

        case DIAG_PHONEMODE_DLOAD:
            if(!m_bDLoadedARMPrg)
            {
                if(FALSE == DLoadARMPRG())
                {
                    if(m_pDloadConfig->IsEMMC()&&m_pDloadConfig->IsDloadMBN())
                    {
                        m_nStatus   = DLOAD_STATUS_SUCCESS;
                        MOVE_TO_STEP(DLOAD_STEP_DLOADEMMC);
                        return TRUE;
                    }
                    else
                    {
                        MOVE_TO_STEP(DLOAD_STEP_EXIT);
                        return TRUE;
                    }
                }
                m_bDLoadedARMPrg = TRUE;
            }
            return FALSE;

        case DIAG_PHONEMODE_EDLOAD:
            if(!m_bDLoadedARMPrg)
            {
                if(FALSE == DLoadEARMPRG())
                {
                    MOVE_TO_STEP(DLOAD_STEP_EXIT);
                    return TRUE;
                }
                m_bDLoadedARMPrg = TRUE;
            }
            return FALSE;
            
        case DIAG_PHONEMODE_ARMPRG:
            m_nStatus   = DLOAD_STATUS_SUCCESS;
            break;
        
        case DIAG_PHONEMODE_FASTBOOT:
            MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
            m_nStatus   = DLOAD_STATUS_SUCCESS;
            return TRUE;

        default:
            m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
            LOG_OUTPUT(_T("DLoadThread::StepRunARMPRG Status %d %d****\n"),m_bDLoadedARMPrg,status);
            return FALSE;
        }
    }
    MOVE_TO_STEP(DLOAD_STEP_DLOADINGMBN);
    return TRUE;
}

BOOL DLoadThread::StepDloadMBN(void)
{
    if(m_pDloadConfig->IsDloadMBN()||(m_pDloadConfig->IsMemDump()&& m_pDloadConfig->GetReadSize()))
    {
        int status = COMManager::GetCurrentPhoneMode(m_PhoneDiag.GetPortID());

        switch(status){
        case DIAG_PHONEMODE_CDMA:
        case DIAG_PHONEMODE_WCDMA:
        case DIAG_PHONEMODE_DLOAD:
        case DIAG_PHONEMODE_EDLOAD:
            m_nStatus = DLOAD_STATUS_FAILED;
            //MOVE_TO_STEP(DLOAD_STEP_EXIT);
            status = m_PhoneDiag.ConnectPhone();
            LOG_OUTPUT(_T("DLoadThread::StepDloadMBN INLINE****\n"));
            return FALSE;
            
        case DIAG_PHONEMODE_ARMPRG:
            if(m_pDloadConfig->IsMemDump()&& m_pDloadConfig->GetReadSize())
            {
                if(FALSE == MemDump())
                {
                    MOVE_TO_STEP(DLOAD_STEP_EXIT);
                    return TRUE;
                }
            }
            
            if(m_pDloadConfig->IsDloadMBN())
            {
                if(FALSE == DloadMBN())
                {
                    MOVE_TO_STEP(DLOAD_STEP_EXIT);
                    return TRUE;
                }
            }
            
            //if(m_pDloadConfig->IsAutoDLoad() || m_bEFSRestore || m_bNVRestore)
            {
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                m_nNextStep = DLOAD_STEP_DLOADEMMC;
                MOVE_TO_STEP(DLOAD_STEP_RESET);
                return TRUE;
            }
            break;
            
        case DIAG_PHONEMODE_FASTBOOT:
            MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
            m_nStatus   = DLOAD_STATUS_SUCCESS;
            return TRUE;
            
        default:
            m_nStatus = DLOAD_STATUS_FAILED;
            return FALSE;
        }

        m_nStatus   = DLOAD_STATUS_SUCCESS;
    }
    MOVE_TO_STEP(DLOAD_STEP_DLOADEMMC);
    return TRUE;
}

BOOL DLoadThread::StepDLoadEMMC(void)
{
    if(m_pDloadConfig->IsEMMC() && m_pDloadConfig->IsDloadMBN())
    {
        int status = m_PhoneDiag.ConnectPhone();
        LOG_OUTPUT(_T("DLoadThread::StepDLoadEMMC %d m_bWaitReset=%d\n"),status,m_bWaitReset);
        switch(status){
        case DIAG_PHONEMODE_CDMA:
        case DIAG_PHONEMODE_WCDMA:
            m_nStatus = DLOAD_STATUS_FAILEDTODLOAD;
            m_PhoneDiag.SendSwitchToDLoadReq();
            m_bWaitReset = TRUE;
            return FALSE;
            
        case DIAG_PHONEMODE_DLOAD:
        case DIAG_PHONEMODE_EDLOAD:
        case DIAG_PHONEMODE_ARMPRG:
        default:
            if(m_bWaitReset)
            {
                return FALSE;
            }
            
            if(!DLoadFindEMMC(m_UsbPorts.GetHubIndex(),m_UsbPorts.GetPortIndex()))
            {
                m_nWaitTime = 0;
                m_nStatus = DLOAD_STATUS_FAILEDTORESET;
                return FALSE;
            }
            
            m_PhoneDiag.SetFastBootMode(TRUE);
            m_pCOMMgr->SetUsed(m_nPort, TRUE, this);
            m_PhoneDiag.ConnectPhone();
            
            if(FALSE == DLoadEMMCImages())
            {
                m_nStatus = DLOAD_STATUS_FAILEDTODLOAD;
                LOG_OUTPUT(_T("DLoadThread::StepDLoadEMMC FAILEDTODLOAD\n"));
                MOVE_TO_STEP(DLOAD_STEP_EXIT);
                return TRUE;
            }

            m_PhoneDiag.SetFastBootMode(FALSE);
            m_PhoneDiag.ConnectPhone();
            
            if(m_bEFSRestore || m_bNVRestore)
            {
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                m_nNextStep = DLOAD_STEP_EFSRESTORE;
                MOVE_TO_STEP(DLOAD_STEP_RESET);
                return TRUE;
            }
            break;
        }
        
        m_nStatus   = DLOAD_STATUS_SUCCESS;
        m_nNextStep = DLOAD_STEP_EXIT;
        MOVE_TO_STEP(DLOAD_STEP_RESET);
        return TRUE;
    }
    MOVE_TO_STEP(DLOAD_STEP_DLOADINGARD);
    return TRUE;
}

BOOL DLoadThread::StepDloadARD(void)
{
    if(m_pDloadConfig->IsDloadARD())
    {
        int status = m_PhoneDiag.ConnectPhone();
        LOG_OUTPUT(_T("DLoadThread::StepDloadARD %d m_bWaitReset=%d\n"),status,m_bWaitReset);
        switch(status){
        case DIAG_PHONEMODE_CDMA:
        case DIAG_PHONEMODE_WCDMA:
            m_nStatus = DLOAD_STATUS_FAILEDTODLOAD;
            m_PhoneDiag.SendSwitchToDLoadReq();
            m_bWaitReset = TRUE;
            return FALSE;
            
        case DIAG_PHONEMODE_DLOAD:
        {
            BYTE sec_code[DIAG_PASSWORD_SIZE] ;
            memset(sec_code,0xFF,DIAG_PASSWORD_SIZE);
            m_PhoneDiag.SendDLoadUnlockReq(sec_code, DIAG_PASSWORD_SIZE);
            if(m_PhoneDiag.SendDLoadResetModeReq(RESET_FASTBOOT_MODE))
            {
                if(m_PhoneDiag.SendDLoadResetReq())
                {
                    m_bWaitReset = FALSE;
                }
            }
            m_nStatus = DLOAD_STATUS_FAILED;
            return FALSE;
        }

        case DIAG_PHONEMODE_EDLOAD:
            m_nStatus = DLOAD_STATUS_FAILED;
            m_PhoneDiag.SendDLoadResetReq();
            return FALSE;

        case DIAG_PHONEMODE_ARMPRG:
            m_nStatus = DLOAD_STATUS_FAILED;
            m_PhoneDiag.SendAPResetReq();
            return FALSE;
            
        default:
            if(m_bWaitReset)
            {
                return FALSE;
            }

            m_Fastboot.SetHubPortIndex(m_UsbPorts.GetHubIndex(),m_UsbPorts.GetPortIndex());
            if(m_Fastboot.Exec(NULL,this) != 0)
            {
                return FALSE;
            }
            
            m_PhoneDiag.SetFastBootMode(TRUE);
            m_pCOMMgr->SetUsed(m_nPort, TRUE, this);
            m_PhoneDiag.ConnectPhone();
            if(FALSE == DloadARD())
            {
                LOG_OUTPUT(_T("DLoadThread::StepDloadARD DloadARD Failed\n"));
                m_nStatus = DLOAD_STATUS_FAILED;
                MOVE_TO_STEP(DLOAD_STEP_EXIT);
                return TRUE;
            }

            m_PhoneDiag.SetFastBootMode(FALSE);
            m_PhoneDiag.ConnectPhone();

            if(m_bEFSRestore || m_bNVRestore)
            {
                m_nStatus   = DLOAD_STATUS_SUCCESS;
                m_nNextStep = DLOAD_STEP_EFSRESTORE;
                MOVE_TO_STEP(DLOAD_STEP_RESET);
                return TRUE;
            }
            break;
        }

        m_nStatus   = DLOAD_STATUS_SUCCESS;
    }
    MOVE_TO_STEP(DLOAD_STEP_EFSRESTORE);
    return TRUE;
}

BOOL DLoadThread::StepEFSRestore(void)
{
    if(m_bEFSRestore)
    {
        if(FALSE == EFSRestore(m_pDloadConfig->GetEFSPath()))
        {
            m_nStatus = DLOAD_STATUS_EFSRESTORE;
            MOVE_TO_STEP(DLOAD_STEP_EXIT);
            return TRUE;
        }
    }
    MOVE_TO_STEP(DLOAD_STEP_NVRESTORE);
    return TRUE;
}

BOOL DLoadThread::StepNVRestore(void)
{
    if(m_bNVRestore)
    {
        if(FALSE == NVRestore())
        {
            m_nStatus = DLOAD_STATUS_NVRESTORE;
            MOVE_TO_STEP(DLOAD_STEP_EXIT);
            return TRUE;
        }
    }

    if(m_bNeedReset)
    {
        m_nNextStep = DLOAD_STEP_EXIT;
        MOVE_TO_STEP(DLOAD_STEP_RESET);
        m_bNeedReset = FALSE;
        return TRUE;
    }
    MOVE_TO_STEP(DLOAD_STEP_EXIT);
    return TRUE;
}

BOOL DLoadThread::StepReset(void)
{
    int status;

    switch(m_nStepRet){
    case DLOAD_STEPRET_INIT:
        status = COMManager::GetCurrentPhoneMode(m_PhoneDiag.GetPortID());
        LOG_OUTPUT(_T("DLoadThread::StepReset %d\n"),status);
        switch(status){
        case DIAG_PHONEMODE_CDMA:
        case DIAG_PHONEMODE_WCDMA:
            if(FALSE == m_PhoneDiag.SendModeChangeCmd(CMDIAG_MODE_OFFLINE_D_F))
            {
                if(m_nStatus == DLOAD_STATUS_SUCCESS)
                {
                    m_nStatus = DLOAD_STATUS_FAILEDTORESET;
                }
                MOVE_TO_STEP(m_nNextStep);
                return TRUE;
            }
            if(FALSE == m_PhoneDiag.SendModeChangeCmd(CMDIAG_MODE_RESET_F))
            {
                if(m_nStatus == DLOAD_STATUS_SUCCESS)
                {
                    m_nStatus = DLOAD_STATUS_FAILEDTORESET;
                }
                MOVE_TO_STEP(m_nNextStep);
                return TRUE;
            }
            break;
        case DIAG_PHONEMODE_DLOAD:
        case DIAG_PHONEMODE_EDLOAD:
            if(FALSE == m_PhoneDiag.SendDLoadResetReq())
            {
                if(m_nStatus == DLOAD_STATUS_SUCCESS)
                {
                    m_nStatus = DLOAD_STATUS_FAILEDTORESET;
                }
                MOVE_TO_STEP(m_nNextStep);
                return TRUE;
            }
            break;
        case DIAG_PHONEMODE_ARMPRG:
            if(FALSE == m_PhoneDiag.SendAPResetReq())
            {
                if(m_nStatus == DLOAD_STATUS_SUCCESS)
                {
                    m_nStatus = DLOAD_STATUS_FAILEDTORESET;
                }
                MOVE_TO_STEP(m_nNextStep);
                return TRUE;
            }
            break;
        case DIAG_PHONEMODE_FASTBOOT:
            break;
        default:
            m_nStatus = DLOAD_STATUS_FAILEDTORESET;
            MOVE_TO_STEP(m_nNextStep);
            return TRUE;
        }
        m_bResetting = FALSE;
        STEP_RET(DLOAD_STEPRET_RETRY);
        return FALSE;
        
    default:
        status = m_PhoneDiag.ConnectPhone();
        switch(status){
        case DIAG_PHONEMODE_CDMA:
        case DIAG_PHONEMODE_WCDMA:
        case DIAG_PHONEMODE_DLOAD:
        case DIAG_PHONEMODE_EDLOAD:
        case DIAG_PHONEMODE_ARMPRG:
            if(FALSE == m_bResetting)
            {
                return FALSE;
            }
            break;

        case DIAG_PHONEMODE_FASTBOOT:
        default:
            if(m_nNextStep == DLOAD_STEP_DLOADINGARD && m_pDloadConfig->IsDloadARD())
            {
                break;
            }
            m_bResetting = TRUE;
            return FALSE;
        }
        break;
    }
    
    LOG_OUTPUT(_T("DLoadThread::StepReset Moveto %d\n"),m_nNextStep);
    MOVE_TO_STEP(m_nNextStep);
    return TRUE;
}

BOOL DLoadThread::StepExit(void)
{
    LOG_OUTPUT(_T("DLoadThread::StepExit %d %d\n"),m_bTimeOut,m_bUserCancel);
    if(m_bTimeOut)
    {
        if(m_nStatus == DLOAD_STATUS_SUCCESS)
        {
            m_nStatus = DLOAD_STATUS_COMTIMEOUT;
        }
    }
    else if(m_bUserCancel)
    {
        m_nStatus = DLOAD_STATUS_USERCANCEL;
    }
    
    m_PhoneDiag.SetFastBootMode(FALSE);
    m_PhoneDiag.StopDiag();
    m_pCOMMgr->SetUsed(m_nPort, FALSE, this);
    m_nCurrStep = DLOAD_STEP_EXIT;
    if(m_dwStartTimeMS == 0)
    {
        m_dwWorkTimeMS = 0;
    }
    else
    {
        m_dwWorkTimeMS = GetTickCount()-m_dwStartTimeMS;
    }
    SendCompleteMsg();
    return TRUE;
}

// Message Functions
void DLoadThread::SendProgressMsg(void)
{
    if(m_pfnProgress)
    {
        m_pfnProgress(m_pUserParam, m_nIndex);
    }
}

void DLoadThread::SendCompleteMsg(void)
{
    if(m_pfnProgress)
    {
        m_pfnProgress(m_pUserParam, m_nIndex);
    }
}

BOOL DLoadThread::WaitMsg(UINT nWait, UINT nTimeOut)
{
    if(WaitForSingleObject(m_WaitEvent, nWait) == WAIT_TIMEOUT)
    {
        m_nWaitTime += nWait;
    }

    ResetEvent(m_WaitEvent);
    if(m_nWaitTime >= nTimeOut)
    {
        LOG_OUTPUT(_T("DLoadThread::WaitMsg %d %d %d\n"),m_nWaitTime,nTimeOut,nWait);
        return TRUE;
    }
    return FALSE;
}

void DLoadThread::WaitMS(UINT nWait)
{
    WaitForSingleObject(m_WaitEvent, nWait);
    ResetEvent(m_WaitEvent);
}

void DLoadThread::SetCurr(DWORD dwCurr)
{
    m_Curr = dwCurr; 
    if(m_Curr%0x10000 == 0 || m_Curr == m_Total)
    {
        SendProgressMsg();
    }
};

UINT DLoadThread::DLoadThreadProc(LPVOID lpParam)
{
    DLoadThread *pDLoadThread = (DLoadThread *)lpParam;
    pDLoadThread->RunFSM();
    pDLoadThread->ResetDLoadThread();
    return 0;
}

BOOL DLoadThread::DLoadARMPRG(void)
{
    BYTE *pBuf;
    int   nLen;
    DWORD dwAddr;
    WORD wDataSize = PKT_BUF_SIZE;
    diag_dload_param_rsp_type RspParam;
    BYTE sec_code[DIAG_PASSWORD_SIZE] ;
    CString szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetARMPRGName();

    m_szProgName = m_pDloadConfig->GetARMPRGName();
    CHexFile myAPFile(szName);
    if(!myAPFile.IsValid())
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::DLoadARMPRG Failed Open %s\n"),szName);
        return FALSE;
    }

    if(FALSE == m_PhoneDiag.SendDLoadParamReq(&RspParam))
    {
        m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
        LOG_OUTPUT(_T("DLoadThread::DLoadARMPRG Failed SendDLoadParamReq\n"));
        return FALSE;
    }
    wDataSize = min(wDataSize, RspParam.max_write_size);

    // Send SEC Code
    memset(sec_code,0xFF,DIAG_PASSWORD_SIZE);
    if(FALSE == m_PhoneDiag.SendDLoadUnlockReq(sec_code, DIAG_PASSWORD_SIZE))
    {
        m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
        LOG_OUTPUT(_T("DLoadThread::DLoadARMPRG Failed SendDLoadUnlockReq\n"));
        return FALSE;
    }
    
    diag_dload_ver_rsp_type VerReq;
    if(FALSE == m_PhoneDiag.SendDLoadVerReq(&VerReq))
    {
        m_szVersion.Empty();
    }
    else
    {
        m_szVersion.Empty();
        for(int i=0;i<VerReq.len;i++)
        {
            m_szVersion.Insert(m_szVersion.GetLength(),VerReq.ver[i]);
        }
    }

    LOG_OUTPUT(_T("DLoadThread::DLoadARMPRG VER:%s\n"),m_szVersion);
    if(m_pDloadConfig->IsDloadARD()&&!(m_pDloadConfig->IsEMMC()&&m_pDloadConfig->IsDloadMBN()))
    {
        int nRetry = 5;
        do
        {
            if(m_PhoneDiag.SendDLoadResetModeReq(RESET_FASTBOOT_MODE))
            {
                break;
            }
        }
        while(m_bRunning && nRetry--);
    }
    
    if(FALSE == myAPFile.EnumSegmentInit())
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        return FALSE;
    }
    
    m_Curr  = 0;
    m_Total = (DWORD)myAPFile.GetLength();
    SendProgressMsg();

    while(myAPFile.EnumNextSegment() && m_bRunning)
    {
        myAPFile.GetSegmentAddr(&dwAddr, NULL);
        pBuf  = myAPFile.GetSegmentBuf();
        nLen  = myAPFile.GetSegmentSize();
        pBuf += myAPFile.GetSegmentStartAddr();
        
        dwAddr = dwAddr<<16;
        dwAddr+= myAPFile.GetSegmentStartAddr();
        while(nLen>0 && m_bRunning)
        {
            if(FALSE == m_PhoneDiag.SendDLoadWrite32Req(dwAddr,pBuf,min(nLen,wDataSize)))
            {
                m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
                LOG_OUTPUT(_T("DLoadThread::DLoadARMPRG Failed SendDLoadWrite32Req\n"));
                return FALSE;
            }
            dwAddr += wDataSize;
            pBuf   += wDataSize;
            nLen   -= wDataSize;
        }
        m_Curr = (DWORD)myAPFile.GetPosition();
        SendProgressMsg();
    }
    
    if(FALSE == m_PhoneDiag.SendDLoadGoReq(myAPFile.GetStartAddr()))
    {
        m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
        LOG_OUTPUT(_T("DLoadThread::DLoadARMPRG Failed SendDLoadGoReq\n"));
        return FALSE;
    }
    m_nStatus = DLOAD_STATUS_SUCCESS;
    m_szProgName.Empty();
    return TRUE;
}


BOOL DLoadThread::DLoadEARMPRG(void)
{
    BYTE *pBuf;
    int   nLen;
    DWORD dwAddr;
    WORD wDataSize = PKT_BUF_SIZE;
    diag_dload_param_rsp_type RspParam;
    CString szName = m_pDloadConfig->GetMBNPath()+ _T("E") + m_pDloadConfig->GetARMPRGName();

    m_szProgName = _T("E") + m_pDloadConfig->GetARMPRGName();
    CHexFile myAPFile(szName);
    if(!myAPFile.IsValid())
    {
        // Try normal armprg
        szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetARMPRGName();
        m_szProgName = m_pDloadConfig->GetARMPRGName();
        myAPFile.Open(szName);
        if(!myAPFile.IsValid())
        {
            m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
            LOG_OUTPUT(_T("DLoadThread::DLoadEARMPRG Failed Open %s\n"),szName);
            return FALSE;
        }
    }

    if(FALSE == m_PhoneDiag.SendDLoadParamReq(&RspParam))
    {
        m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
        LOG_OUTPUT(_T("DLoadThread::DLoadEARMPRG Failed SendDLoadParamReq\n"));
        return FALSE;
    }
    wDataSize = min(wDataSize, RspParam.max_write_size);

    if(FALSE == myAPFile.EnumSegmentInit())
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        return FALSE;
    }
    
    m_Curr  = 0;
    m_Total = (DWORD)myAPFile.GetLength();
    SendProgressMsg();

    while(myAPFile.EnumNextSegment() && m_bRunning)
    {
        myAPFile.GetSegmentAddr(&dwAddr, NULL);
        pBuf  = myAPFile.GetSegmentBuf();
        nLen  = myAPFile.GetSegmentSize();
        pBuf += myAPFile.GetSegmentStartAddr();
        
        dwAddr = dwAddr<<16;
        dwAddr+= myAPFile.GetSegmentStartAddr();
        while(nLen>0 && m_bRunning)
        {
            if(FALSE == m_PhoneDiag.SendDLoadWrite32Req(dwAddr,pBuf,min(nLen,wDataSize)))
            {
                m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
                LOG_OUTPUT(_T("DLoadThread::DLoadEARMPRG Failed SendDLoadWrite32Req\n"));
                return FALSE;
            }
            dwAddr += wDataSize;
            pBuf   += wDataSize;
            nLen   -= wDataSize;
        }
        m_Curr = (DWORD)myAPFile.GetPosition();
        SendProgressMsg();
    }
    
    if(FALSE == m_PhoneDiag.SendDLoadGoReq(myAPFile.GetStartAddr()))
    {
        m_nStatus = DLOAD_STATUS_FAILEDTORUNARMPRG;
        LOG_OUTPUT(_T("DLoadThread::DLoadEARMPRG Failed SendDLoadGoReq\n"));
        return FALSE;
    }
    m_nStatus = DLOAD_STATUS_SUCCESS;
    m_szProgName.Empty();
    return TRUE;
}

BOOL DLoadThread::DloadMBN(void)
{
    BOOL        bRet = TRUE;
    CFile       myFile;
    CFileStatus myFileStatus;
    CString     szName;

    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SendAPHelloReq(&aphello))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        LOG_OUTPUT(_T("DLoadThread::DloadMBN SendAPHelloReq Failed\n"));
        return FALSE;
    }
    
    m_bUnframedSupport = m_pDloadConfig->IsUnframe();
    
    m_szFlashHD.Empty();
    for(int i=0; i<aphello.flashnamelen; i++)
    {
        m_szFlashHD.AppendChar(aphello.flashname[i]);
    }

    LOG_OUTPUT(_T("DLoadThread::DloadMBN FLASH %s\n"),m_szFlashHD);
    SyncMBNPath();

    BOOL    bMultiMBN = m_pDloadConfig->IsMultiMBN();
    if(bMultiMBN)
    {
        if(m_pDloadConfig->IsDloadFACT())
        {
            if(myFile.GetStatus(GetMBNPath()+m_pDloadConfig->GetFactName(),myFileStatus))
            {
                bMultiMBN = FALSE;
                szName = GetMBNPath()+m_pDloadConfig->GetFactName();
                m_szProgName = m_pDloadConfig->GetFactName();
            }
            else if(myFile.GetStatus(m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetFactName(),myFileStatus))
            {
                bMultiMBN = FALSE;
                szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetFactName();
                m_szProgName = m_pDloadConfig->GetFactName();
            }
        }
    }
    else
    {
        if(myFile.GetStatus(GetMBNPath()+m_pDloadConfig->GetAMSSName(),myFileStatus))
        {
            szName = GetMBNPath()+m_pDloadConfig->GetAMSSName();
        }
        else
        {
            szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetAMSSName();
        }
        m_szProgName = m_pDloadConfig->GetAMSSName();
    }
    
    if(bMultiMBN)
    {
        bRet = DLoadMultiMBN();
    }
    else
    {
        DWORD dwAddr;
        bRet = DLoadOneMBN(szName, m_pDloadConfig->IsJumbBoot(&dwAddr)?dwAddr:0);
    }
    if(bRet)
    {
        m_szProgName.Empty();
    }
    return bRet;
}

BOOL DLoadThread::DLoadOneMBN(CString &szName, DWORD dwOffset)
{
    CFile       myFile;
    if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed Open %s\n"),szName);
        return FALSE;
    }
    
    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SetStreamWindowSize((aphello.windowsize+1)/2))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed SetStreamWindowSize\n"));
        return FALSE;
    }
    
    DWORD dwEraseStart = 0, dwEraseEnd = 0;
    DWORD dwAddress = aphello.base_addr;
    if(m_pDloadConfig->IsMultiMBN()) // 需要进入Factory Mode
    {
        // 关闭已经打开的下载请求
        if(m_bRunning)  m_PhoneDiag.SendAPCloseReq();
        if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SendAPOpenReq(OPEN_MODE_FACTORY))
        {
            m_nStatus = DLOAD_STATUS_FAILED;
            LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed SendAPOpenReq\n"));
            return FALSE;
        }
        dwAddress = 0;
    }
    else if(m_pDloadConfig->IsEraseFlash())
    {
        BOOL bFind = FALSE;
        dwEraseStart = (DWORD)myFile.GetLength();
        for(int i=0; i<aphello.num_blocks; i++)
        {
            if(FALSE == bFind && dwEraseEnd>dwEraseStart)
            {
                dwEraseStart = dwEraseEnd;
                bFind = TRUE;
            }
            dwEraseEnd += aphello.blocks[i];
        }
    }

    dwAddress += dwOffset;
    myFile.Seek(dwAddress,CFile::begin);
    
    m_szProgName += _T(" - ");
    for(int i=0; i<aphello.flashnamelen; i++)
    {
        m_szProgName.AppendChar(aphello.flashname[i]);
    }

    m_Curr  = 0;
    m_Total = (DWORD)myFile.GetLength()-dwOffset+(dwEraseEnd-dwEraseStart);
    SendProgressMsg();
    
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    UINT wMaxLen = MIN(DEFAULT_MAX_FRAME_SIZE, aphello.preferred_block_size);
    UINT wLen;
    BOOL bRet = TRUE;
    while(m_bRunning)
    {
        wLen = myFile.Read(bData, wMaxLen);
        if(wLen == 0)
        {
            SendProgressMsg();
            break;
        }
        
        if(m_bUnframedSupport)
        {
            if(FALSE == m_PhoneDiag.SendAPUnframedStreamWriteReq(dwAddress,bData,(WORD)wLen))
            {
                m_nStatus = DLOAD_STATUS_FAILED;
                bRet = FALSE;
                LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed SendAPUnframedStreamWriteReq\n"));
                break;
            }
        }
        else
        {
            if(FALSE == m_PhoneDiag.SendAPStreamWriteReq(dwAddress,bData,(WORD)wLen))
            {
                m_nStatus = DLOAD_STATUS_FAILED;
                bRet = FALSE;
                LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed SendAPStreamWriteReq\n"));
                break;
            }
        }
        
        dwAddress += wLen;
        m_Curr += wLen;
        if(dwAddress%0x10000 == 0)
        {
            SendProgressMsg();
        }
    }
    
    memset(bData, 0xFF, wMaxLen);
    while(m_bRunning)
    {
        if(dwEraseStart >= dwEraseEnd)
        {
            SendProgressMsg();
            break;
        }
        
        wLen = MIN(wMaxLen, dwEraseEnd-dwEraseStart);
        if(m_bUnframedSupport)
        {
            if(FALSE == m_PhoneDiag.SendAPUnframedStreamWriteReq(dwAddress,bData,(WORD)wLen))
            {
                m_nStatus = DLOAD_STATUS_FAILED;
                bRet = FALSE;
                LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed SendAPUnframedStreamWriteReq\n"));
                break;
            }
        }
        else
        {
            if(FALSE == m_PhoneDiag.SendAPStreamWriteReq(dwEraseStart,bData,(WORD)wLen))
            {
                m_nStatus = DLOAD_STATUS_FAILED;
                bRet = FALSE;
                LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed SendAPStreamWriteReq\n"));
                break;
            }
        }
        
        dwEraseStart += wLen;
        m_Curr += wLen;
        if(dwEraseStart%0x10000 == 0)
        {
            SendProgressMsg();
        }
    }
    
    // 检查Stream包的回复
    if(bRet == TRUE)
    {
        m_nWaitTime = 0;
        while(m_bRunning && FALSE == m_PhoneDiag.IsStreamComplete())
        {
            m_PhoneDiag.SendTimeoutStreamFrames();
            if(TRUE == WaitMsg(STREAMFRAME_TIMEOUT_MS/100, STREAMFRAME_TIMEOUT_MS))
            {
                m_nStatus = DLOAD_STATUS_FAILED;
                bRet = FALSE;
                LOG_OUTPUT(_T("DLoadThread::DLoadOneMBN Failed Wait\n"));
                break;
            }
        }
        m_nWaitTime = 0;
    }
    
    if(m_pDloadConfig->IsMultiMBN()) // 需要进入Factory Mode
    {
        // 关闭已经打开的下载请求
        if(m_bRunning)  m_PhoneDiag.SendAPCloseReq();
    }
    return bRet;
}

BOOL DLoadThread::DLoadMultiMBN(void)
{
    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SetStreamWindowSize((aphello.windowsize+1)/2))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        LOG_OUTPUT(_T("DLoadThread::DLoadMultiMBN SetStreamWindowSize Failed\n"));
        return FALSE;
    }
    
    CString szFlashName = _T(" - ")+m_szFlashHD;
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    UINT wMaxLen = MIN(DEFAULT_MAX_FRAME_SIZE, aphello.preferred_block_size);
    UINT wLen;
    BOOL bRet = TRUE;
    CString szName;

    // 关闭已经打开的下载请求
    if(m_bRunning)  m_PhoneDiag.SendAPCloseReq();

    // 设置Security mode
    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SendAPSecModeReq(m_pDloadConfig->GetSecMode()))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        LOG_OUTPUT(_T("DLoadThread::DLoadMultiMBN SendAPSecModeReq Failed\n"));
        return FALSE;
    }

    // 发送Partition File
    if(m_bRunning && !m_pDloadConfig->IsEMMC()) 
    {
        CFile       myFile;
        CString     szName = GetMBNPath()+m_pDloadConfig->GetPartitionName();
        m_szProgName = m_pDloadConfig->GetPartitionName()+szFlashName;
        if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
        {
            szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetPartitionName();
            if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
            {
                m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
                LOG_OUTPUT(_T("DLoadThread::DLoadMultiMBN Failed Open %s \n"),szName);
                return FALSE;
            }
        }

        m_Curr  = 0;
        m_Total = (DWORD)myFile.GetLength();
        SendProgressMsg();
        while(m_bRunning)
        {
            wLen = myFile.Read(bData, DEFAULT_MAX_FRAME_SIZE);
            if(wLen == 0)
            {
                SendProgressMsg();
                break;
            }
            if(FALSE == m_PhoneDiag.SendAPPartiTblReq(FALSE, bData, wLen))
            {
                if(FALSE == m_PhoneDiag.SendAPPartiTblReq(TRUE, bData, wLen))
                {
                    m_nStatus = DLOAD_STATUS_FAILED;
                    bRet = FALSE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadMultiMBN SendAPPartiTblReq Failed\n"));
                    break;
                }
            }
            m_Curr += wLen;
            if(m_Curr%0x10000 == 0)
            {
                SendProgressMsg();
            }
        }

        if(FALSE == bRet)
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadPBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetPBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_PBL, wMaxLen))
        {
            return FALSE;
        }
    }
    
    if(m_pDloadConfig->IsDloadQCSBL() && m_pDloadConfig->IsUSBBoot() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetQCSBLHDCFGName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadEraseOneMultiMBN(szName, OPEN_MULTI_MODE_QCSBLHDCFG, wMaxLen))
        {
            return FALSE;
        }
    }
    
    if(m_pDloadConfig->IsDloadOEMSBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetOEMSBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_OEMSBL, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadDBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetDBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_DBL, wMaxLen))
        {
            return FALSE;
        }
    }
    
    if(m_pDloadConfig->IsDloadFSBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetFSBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_FSBL, wMaxLen))
        {
            return FALSE;
        }
    }
    
    if(m_pDloadConfig->IsDloadOSBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetOSBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_OSBL, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadAMSS() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetAMSSName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_AMSS, wMaxLen))
        {
            return FALSE;
        }
    }
    
    if(m_pDloadConfig->IsDloadCEFS() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetCEFSName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_CEFS, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadAPPSBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetAPPSBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_APPSBL, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadAPPS() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetAPPSName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_APPS, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadAPPSCEFS() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetAPPSCEFSName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_APPS_CEFS, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadFLASHBIN() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetFLASHBINName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_APPS_WM60, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadOBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetOBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_OBL, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadFOTAUI() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetFOTAUIName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_FOTAUI, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadDSP1() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetDSP1Name();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_DSP1, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadDSP2() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetDSP2Name();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_DSP2, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadCUSTOM() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetCUSTOMName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_CUSTOM, wMaxLen))
        {
            return FALSE;
        }
    }

    if(m_pDloadConfig->IsDloadRAW() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetRAWName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_RAW, wMaxLen))
        {
            return FALSE;
        }
    }
    
    if(m_pDloadConfig->IsDloadQCSBL() && !m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetQCSBLName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_QCSBL, wMaxLen))
        {
            return FALSE;
        }

        szName = m_pDloadConfig->GetQCSBLHDCFGName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_QCSBLHDCFG, wMaxLen))
        {
            return FALSE;
        }
    }
    
    if(m_pDloadConfig->IsDloadMsimage() && m_pDloadConfig->IsEMMC())
    {
        szName = m_pDloadConfig->GetMsimageName();
        m_szProgName = szName+szFlashName;
        if(FALSE == m_bRunning || FALSE == DLoadOneMultiMBN(szName, OPEN_MULTI_MODE_MSIMAGE, wMaxLen))
        {
            return FALSE;
        }
    }
    
    m_szVersion.Empty();
    return TRUE;
}

BOOL DLoadThread::DLoadOneMultiMBN(CString &szName, open_multi_mode_type mMode, UINT wMaxLen)
{
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    UINT wLen = 0;
    BOOL bRet = TRUE;
    DWORD dwAddress;
    BYTE *pHDData = NULL;
    
    if(m_pDloadConfig->IsMultiMBN() == CHIPSET_MULTIBOOT_1)
    {
        CString szFileName;
        szFileName.Empty();
        switch(mMode){
        case OPEN_MULTI_MODE_OEMSBL:
            szFileName = m_pDloadConfig->GetOEMSBLHDName();
            break;
        case OPEN_MULTI_MODE_AMSS:
            szFileName = m_pDloadConfig->GetAMSSHDName();
            break;
        case OPEN_MULTI_MODE_APPS:
            szFileName = m_pDloadConfig->GetAPPSHDName();
            break;
        case OPEN_MULTI_MODE_APPSBL:
            szFileName = m_pDloadConfig->GetAPPSBLHDName();
            break;
#if 0
        case OPEN_MULTI_MODE_QCSBLHDCFG:
            szName = GetQCSBLHDCFGName(szName, GetMBNPath());
            break;
#endif
        default:
            break;
        }
        
        if(m_bRunning && !szFileName.IsEmpty()) 
        {
            CFile       myFile;
            CString     szName = GetMBNPath()+szFileName;
            if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
            {
                szName = m_pDloadConfig->GetMBNPath()+szFileName;
                if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
                {
                    m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadOneMultiMBN Failed Open %s\n"),szName);
                    return FALSE;
                }
            }
            pHDData = bData;
            wLen = myFile.Read(pHDData, DEFAULT_MAX_FRAME_SIZE);
        }
    }

    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SendAPOpenMultiReq(mMode,pHDData,wLen))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        LOG_OUTPUT(_T("DLoadThread::DLoadOneMultiMBN Failed SendAPOpenMultiReq\n"));
        return FALSE;
    }
    
    // 发送数据
    if(m_bRunning) 
    {
        CFile       myFile;
        CString     myName;

        myName = GetMBNPath()+szName;
        if(FALSE == myFile.Open(myName, CFile::modeRead|CFile::shareDenyWrite))
        {
            myName = m_pDloadConfig->GetMBNPath()+szName;
            if(FALSE == myFile.Open(myName, CFile::modeRead|CFile::shareDenyWrite))
            {
                m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
                LOG_OUTPUT(_T("DLoadThread::DLoadOneMultiMBN Failed Open %s\n"),myName);
                return FALSE;
            }
        }

        m_Curr  = 0;
        m_Total = (DWORD)myFile.GetLength();
        dwAddress = 0;
        SendProgressMsg();
        while(m_bRunning)
        {
            wLen = myFile.Read(bData, wMaxLen);
            if(wLen == 0)
            {
                SendProgressMsg();
                break;
            }
            
            if(m_bUnframedSupport)
            {
                if(FALSE == m_PhoneDiag.SendAPUnframedStreamWriteReq(dwAddress,bData,(WORD)wLen))
                {
                    m_nStatus = DLOAD_STATUS_FAILED;
                    bRet = FALSE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadOneMultiMBN Failed SendAPUnframedStreamWriteReq\n"));
                    break;
                }
            }
            else
            {
                if(FALSE == m_PhoneDiag.SendAPStreamWriteReq(dwAddress,bData,(WORD)wLen))
                {
                    m_nStatus = DLOAD_STATUS_FAILED;
                    bRet = FALSE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadOneMultiMBN Failed SendAPStreamWriteReq\n"));
                    break;
                }
            }
            dwAddress += wLen;
            m_Curr += wLen;
            if(m_Curr%0x10000 == 0)
            {
                SendProgressMsg();
            }
        }

        if(TRUE == bRet)
        {
            // 检查Stream包的回复
            m_nWaitTime = 0;
            while(m_bRunning && FALSE == m_PhoneDiag.IsStreamComplete())
            {
                m_PhoneDiag.SendTimeoutStreamFrames();
                if(TRUE == WaitMsg(STREAMFRAME_TIMEOUT_MS/100, STREAMFRAME_TIMEOUT_MS))
                {
                    m_nStatus = DLOAD_STATUS_FAILED;
                    bRet = FALSE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadOneMultiMBN Failed Wait\n"));
                    break;
                }
            }
            m_nWaitTime = 0;
        }

        if(FALSE == bRet)
        {
            return FALSE;
        }
    }

    // 关闭已经打开的下载请求
    if(m_bRunning)
    {
        if(FALSE == m_PhoneDiag.SendAPCloseReq())
        {
            m_nStatus = DLOAD_STATUS_FAILED;
            LOG_OUTPUT(_T("DLoadThread::DLoadOneMultiMBN Failed SendAPCloseReq\n"));
            return FALSE;
        }
    }
    return TRUE;
}


BOOL DLoadThread::DLoadEraseOneMultiMBN(CString &szName, open_multi_mode_type mMode, UINT wMaxLen)
{
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    UINT wLen = 0;
    BOOL bRet = TRUE;
    DWORD dwAddress;
    BYTE *pHDData = NULL;
    
    if(m_pDloadConfig->IsMultiMBN() == CHIPSET_MULTIBOOT_1)
    {
        CString szFileName;
        szFileName.Empty();
        switch(mMode){
        case OPEN_MULTI_MODE_OEMSBL:
            szFileName = m_pDloadConfig->GetOEMSBLHDName();
            break;
        case OPEN_MULTI_MODE_AMSS:
            szFileName = m_pDloadConfig->GetAMSSHDName();
            break;
        case OPEN_MULTI_MODE_APPS:
            szFileName = m_pDloadConfig->GetAPPSHDName();
            break;
        case OPEN_MULTI_MODE_APPSBL:
            szFileName = m_pDloadConfig->GetAPPSBLHDName();
            break;
#if 0
        case OPEN_MULTI_MODE_QCSBLHDCFG:
            szName = GetQCSBLHDCFGName(szName, GetMBNPath());
            break;
#endif
        default:
            break;
        }
        
        if(m_bRunning && !szFileName.IsEmpty()) 
        {
            CFile       myFile;
            CString     szName = GetMBNPath()+szFileName;
            if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
            {
                szName = m_pDloadConfig->GetMBNPath()+szFileName;
                if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
                {
                    m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadEraseOneMultiMBN Failed Open %s\n"),szName);
                    return FALSE;
                }
            }
            pHDData = bData;
            wLen = myFile.Read(pHDData, DEFAULT_MAX_FRAME_SIZE);
            memset(bData, 0xFF, wLen);
        }
    }

    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SendAPOpenMultiReq(mMode,pHDData,wLen))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        LOG_OUTPUT(_T("DLoadThread::DLoadEraseOneMultiMBN Failed SendAPOpenMultiReq\n"));
        return FALSE;
    }
    
    // 发送数据
    if(m_bRunning) 
    {
        CFile       myFile;
        CString     myName;
        myName = GetMBNPath()+szName;
        if(FALSE == myFile.Open(myName, CFile::modeRead|CFile::shareDenyWrite))
        {
            myName = m_pDloadConfig->GetMBNPath()+szName;
            if(FALSE == myFile.Open(myName, CFile::modeRead|CFile::shareDenyWrite))
            {
                m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
                LOG_OUTPUT(_T("DLoadThread::DLoadEraseOneMultiMBN Failed Open %s\n"),myName);
                return FALSE;
            }
        }

        m_Curr  = 0;
        m_Total = (DWORD)myFile.GetLength();
        dwAddress = 0;
        SendProgressMsg();
        while(m_bRunning)
        {
            wLen = myFile.Read(bData, wMaxLen);
            if(wLen == 0)
            {
                SendProgressMsg();
                break;
            }
            memset(bData, 0xFF, wLen);
            if(m_bUnframedSupport)
            {
                if(FALSE == m_PhoneDiag.SendAPUnframedStreamWriteReq(dwAddress,bData,(WORD)wLen))
                {
                    m_nStatus = DLOAD_STATUS_FAILED;
                    bRet = FALSE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadEraseOneMultiMBN Failed SendAPUnframedStreamWriteReq\n"));
                    break;
                }
            }
            else
            {
                if(FALSE == m_PhoneDiag.SendAPStreamWriteReq(dwAddress,bData,(WORD)wLen))
                {
                    m_nStatus = DLOAD_STATUS_FAILED;
                    bRet = FALSE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadEraseOneMultiMBN Failed SendAPStreamWriteReq\n"));
                    break;
                }
            }
            dwAddress += wLen;
            m_Curr += wLen;
            if(m_Curr%0x10000 == 0)
            {
                SendProgressMsg();
            }
        }

        if(TRUE == bRet)
        {
            // 检查Stream包的回复
            m_nWaitTime = 0;
            while(m_bRunning && FALSE == m_PhoneDiag.IsStreamComplete())
            {
                m_PhoneDiag.SendTimeoutStreamFrames();
                if(TRUE == WaitMsg(STREAMFRAME_TIMEOUT_MS/100, STREAMFRAME_TIMEOUT_MS))
                {
                    m_nStatus = DLOAD_STATUS_FAILED;
                    bRet = FALSE;
                    LOG_OUTPUT(_T("DLoadThread::DLoadEraseOneMultiMBN Failed Wait\n"));
                    break;
                }
            }
            m_nWaitTime = 0;
        }

        if(FALSE == bRet)
        {
            return FALSE;
        }
    }

    // 关闭已经打开的下载请求
    if(m_bRunning)
    {
        if(FALSE == m_PhoneDiag.SendAPCloseReq())
        {
            m_nStatus = DLOAD_STATUS_FAILED;
            LOG_OUTPUT(_T("DLoadThread::DLoadEraseOneMultiMBN Failed SendAPCloseReq\n"));
            return FALSE;
        }
    }
    return TRUE;
}

BOOL DLoadThread::DloadARD(void)
{
    BOOL        bRet = TRUE;
    BOOL        bReset = FALSE;
    CString     szName;
    CString     szFastCMD;
    
    if(m_bRunning && m_pDloadConfig->IsDloadARDCLRCACHE())
    {
        bReset = TRUE;
        m_szProgName = _T("Clear cache...");
        szFastCMD.Format(_T("erase cache"));
        if(FALSE == m_bRunning || FALSE == DloadFastBoot(szFastCMD))
        {
            return FALSE;
        }
    }
    
    if(m_bRunning && m_pDloadConfig->IsDloadARDBOOT())
    {
        bReset = TRUE;
        szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetARDBOOTName();
        m_szProgName = m_pDloadConfig->GetARDBOOTName();
        szFastCMD.Format(_T("flash boot \"%s\""),szName);
        if(FALSE == m_bRunning || FALSE == DloadFastBoot(szFastCMD))
        {
            return FALSE;
        }
    }
    
    if(m_bRunning && m_pDloadConfig->IsDloadARDSYS())
    {
        bReset = TRUE;
        szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetARDSYSName();
        m_szProgName = m_pDloadConfig->GetARDSYSName();
        szFastCMD.Format(_T("flash system \"%s\""),szName);
        if(FALSE == m_bRunning || FALSE == DloadFastBoot(szFastCMD))
        {
            return FALSE;
        }
    }

    if(m_bRunning && m_pDloadConfig->IsDloadARDUSER())
    {
        bReset = TRUE;
        szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetARDUSERName();
        m_szProgName = m_pDloadConfig->GetARDUSERName();
        szFastCMD.Format(_T("flash userdata \"%s\""),szName);
        if(FALSE == m_bRunning || FALSE == DloadFastBoot(szFastCMD))
        {
            return FALSE;
        }
    }

    if(m_bRunning && m_pDloadConfig->IsDloadARDRCY())
    {
        bReset = TRUE;
        szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetARDRCYName();
        m_szProgName = m_pDloadConfig->GetARDRCYName();
        szFastCMD.Format(_T("flash recovery \"%s\""),szName);
        if(FALSE == m_bRunning || FALSE == DloadFastBoot(szFastCMD))
        {
            return FALSE;
        }
    }
    
    if(m_bRunning && bReset == TRUE)
    {
        szFastCMD.Format(_T("reboot"));
        m_szProgName = szFastCMD;
        SendProgressMsg();
        bRet = DloadFastBoot(szFastCMD);
    }
    
    if(bRet)
    {
        m_szProgName.Empty();
    }
    return bRet;
}

BOOL DLoadThread::DloadFastBoot(CString &szCmd)
{
    BOOL bRet = TRUE;
#if 1
    char cmd[CMD_MAX_LEN];

#ifdef _UNICODE
    WideCharToMultiByte(CP_ACP,WC_COMPOSITECHECK, szCmd.GetBuffer(), -1, cmd, sizeof(cmd), NULL, NULL);
#else
    strcpy(cmd,szCmd.GetBuffer());
#endif
    m_Fastboot.SetHubPortIndex(m_UsbPorts.GetHubIndex(),m_UsbPorts.GetPortIndex());
    m_Fastboot.EnableImageLimit(!m_pDloadConfig->IsEMMC());
    bRet = m_Fastboot.Exec(cmd,this) == 0?TRUE:FALSE;
#else
    SECURITY_ATTRIBUTES sa;
    HANDLE              hRead,hWrite;   
                
    sa.nLength   =   sizeof(SECURITY_ATTRIBUTES);   
    sa.lpSecurityDescriptor   =   NULL;   
    sa.bInheritHandle   =   TRUE;   
    if(!CreatePipe(&hRead,&hWrite,&sa,0))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        return FALSE;   
    }     
    STARTUPINFO             si;   
    PROCESS_INFORMATION     pi;

    si.cb           =   sizeof(STARTUPINFO);   
    GetStartupInfo(&si);     
    si.hStdError    =   hWrite;   
    si.hStdOutput   =   hWrite;   
    si.wShowWindow  =   SW_HIDE;   
    si.dwFlags      =   STARTF_USESHOWWINDOW   |   STARTF_USESTDHANDLES;   

    szCmd = _T("fastboot") + szCmd;
    if(!CreateProcess(NULL,szCmd.GetBuffer(),NULL,NULL,TRUE,NULL,NULL,NULL,&si,&pi))   
    {   
        m_nStatus = DLOAD_STATUS_FAILED;
        return FALSE;  
    }
    CloseHandle(hWrite);   
                
    char   buffer[4096]   =   {0};   
    DWORD   bytesRead;
    while(m_bRunning)   
    {
        if(ReadFile(hRead,buffer,4095,&bytesRead,NULL)==NULL)  
        {
            // TODO: 分析结果
            break;   
        }
        
        TRACE("%d\n",buffer);
        // 更新状态
        WaitMS(777);
    }
#endif
    return bRet;
}

BOOL DLoadThread::EFSBackup(CString &szPath)
{
    return TRUE;
}

BOOL DLoadThread::EFSRestore(CString &szPath)
{
    if(!m_bNeedReset)
    {
        WaitMS(10000);
    }

    if(FALSE == m_bRunning || szPath.IsEmpty() || !m_PhoneDiag.EnterSPCMode())
    {
        m_nStatus = DLOAD_STATUS_EFSRESTORE;
        LOG_OUTPUT(_T("DLoadThread::EFSRestore Failed EnterSPCMode\n"));
        return FALSE;
    }
    
    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SendModeChangeCmd(CMDIAG_MODE_OFFLINE_D_F))
    {
        m_nStatus = DLOAD_STATUS_EFSRESTORE;
        LOG_OUTPUT(_T("DLoadThread::EFSRestore Failed SendModeChangeCmd\n"));
        return FALSE;
    }

    m_bNeedReset = TRUE;

    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.EFS2Connect())
    {
        m_nStatus = DLOAD_STATUS_EFSRESTORE;
        LOG_OUTPUT(_T("DLoadThread::EFSRestore Failed EFS2Connect\n"));
        return FALSE;
    }

    m_szEFSRoot = m_pDloadConfig->GetEFSPath();
    if(m_szEFSRoot.GetAt(0) == '.')
    {
        TCHAR szCurrPath[MAX_PATH];   
        GetCurrentDirectory(MAX_PATH,szCurrPath);
        m_szEFSRoot.Delete(0);
        m_szEFSRoot = szCurrPath+m_szEFSRoot;
    }
    return SyncDirectoryToPhone(szPath);
}

BOOL DLoadThread::SyncFileToPhone(CString &szName)
{
    CString myEFSName = szName;
    CFile   myFile;
    DWORD   dwOffset = 0;
    BYTE    bData[EFS2_DATAPACKET_LEN];
    DWORD   wLen;
    int32   fd;
    BOOL    bRet = TRUE;

    myEFSName.Replace(m_szEFSRoot, _T(""));
    m_szProgName = myEFSName;
    
    //判断是否需要删除一些文件
    if(myEFSName.Right(4) == _T(".del"))
    {
        m_Curr  = 0;
        m_Total = 100;
        SendProgressMsg();
        myEFSName.Replace(_T(".del"), _T(""));
        if(m_PhoneDiag.EFS2IsFileExist(myEFSName))
        {
            m_Curr  = 50;
            m_Total = 100;
            SendProgressMsg();
            if(!m_PhoneDiag.EFS2RemoveFile(myEFSName))
            {
                m_nStatus = DLOAD_STEP_EFSRESTORE;
                LOG_OUTPUT(_T("DLoadThread::SyncFileToPhone Failed EFS2RemoveFile %s\n"),myEFSName);
                return FALSE;
            }
        }
        else
        {
            LOG_OUTPUT(_T("DLoadThread::SyncFileToPhone EFS2IsFileExist FALSE %s\n"),myEFSName);
        }
        m_Curr  = 100;
        m_Total = 100;
        SendProgressMsg();
        return TRUE;
    }

    if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::SyncFileToPhone Failed Open %s\n"),szName);
        return FALSE;
    }
    
    fd = m_PhoneDiag.EFS2OpenFile(myEFSName);
    if(fd < 0)
    {
        fd = m_PhoneDiag.EFS2CreateFile(myEFSName);
    }
    else
    {
        m_PhoneDiag.EFS2TruncateFile(myEFSName, 0);
    }
    if(fd < 0)
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::SyncFileToPhone Failed EFSOpen %s\n"),myEFSName);
        return FALSE;
    }
    m_Curr  = 0;
    m_Total = (DWORD)myFile.GetLength();
    SendProgressMsg();
    while(m_bRunning)
    {
        wLen = myFile.Read(bData, EFS2_DATAPACKET_LEN);
        if(wLen == 0)
        {
            SendProgressMsg();
            break;
        }

        if(FALSE == m_PhoneDiag.EFS2WriteFile(fd,bData,wLen,dwOffset))
        {
            m_nStatus = DLOAD_STEP_EFSRESTORE;
            bRet = FALSE;
            LOG_OUTPUT(_T("DLoadThread::SyncFileToPhone Failed EFS2WriteFile\n"));
            break;
        }
        dwOffset += wLen;
        m_Curr += wLen;
        if(m_Curr%0x1000 == 0)
        {
            SendProgressMsg();
        }
    }
    m_PhoneDiag.EFS2CloseFile(fd);
    return bRet;
}

BOOL DLoadThread::SyncDirectoryToPhone(CString &szPath)
{
    CFileFind   myFind;
    CString     myPath = szPath + _T("*");
    BOOL bFind = myFind.FindFile(myPath);
    while(bFind && m_bRunning)
    {
        bFind = myFind.FindNextFile();
        if(myFind.GetFileName() == _T(".") || myFind.GetFileName() == _T("..") || myFind.IsHidden())
        {
            continue;
        }
        
        if(myFind.IsDirectory())
        {
            CString myEFSPath = myFind.GetFilePath();
            myEFSPath.Replace(m_szEFSRoot, _T(""));
            m_PhoneDiag.EFS2MkDir(myEFSPath);
            if(FALSE == SyncDirectoryToPhone(myFind.GetFilePath()+_T("\\")))
            {
                LOG_OUTPUT(_T("DLoadThread::SyncDirectoryToPhone Failed SyncDirectoryToPhone %s\n"),myEFSPath);
                return FALSE;
            }
        }
        else
        {
            if(FALSE == SyncFileToPhone(myFind.GetFilePath()))
            {
                return TRUE;//FALSE;
            }
        }
    }
    myFind.Close();
    return TRUE;
}

BOOL DLoadThread::NVBackup(CString &szPath)
{
    CQcnFile  myQCNFile;
    CQcnFile &myQCNSample = m_pDloadConfig->GetSampleQcn();
    
    if(!m_PhoneDiag.EnterSPCMode())
    {
        m_nStatus = DLOAD_STATUS_NVBACKUP;
        return FALSE;
    }

    m_szNVAuto.Format(_T("NVBACKUPFILECOM%d.qcn"),m_nPort+1);
    m_szNVAuto = szPath+m_szNVAuto;
    m_szProgName = m_szNVAuto;
    
    if(FALSE == myQCNFile.Open(m_szNVAuto, TRUE))
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        return FALSE;
    }
    
    if(myQCNSample.IsValid())
    {
        m_Curr  = 0;
        m_Total = myQCNSample.GetCount();
        SendProgressMsg();

        BYTE bBuf[DIAG_NV_ITEM_SIZE];
        nv_items_enum_type  nItem;
        UINT                nContext;

        for(int i=0;i<(int)myQCNSample.GetCount() && m_bRunning;i++)
        {
            if(myQCNSample.GetNVItem(i, &nItem, NULL, 0, &nContext))
            {
                memset(bBuf,0,DIAG_NV_ITEM_SIZE);
                if(nContext)
                {
                    if(m_PhoneDiag.SendReadNVItemExt(nItem,bBuf,DIAG_NV_ITEM_SIZE,nContext))
                    {
                        myQCNFile.SetNVItem(nItem,bBuf,DIAG_NV_ITEM_SIZE,nContext);
                    }
                }
                else
                {
                    if(m_PhoneDiag.SendReadNVItem(nItem,bBuf,DIAG_NV_ITEM_SIZE))
                    {
                        myQCNFile.SetNVItem(nItem,bBuf,DIAG_NV_ITEM_SIZE);
                    }
                }
            }
            m_Curr++;
            SendProgressMsg();
        }
    }
    else
    {
        m_Curr  = 0;
        m_Total = NV_MAX_I;
        SendProgressMsg();

        BYTE bBuf[DIAG_NV_ITEM_SIZE];
        for(int i=NV_ESN_I;i<NV_MAX_I && m_bRunning;i++)
        {
            memset(bBuf,0,DIAG_NV_ITEM_SIZE);
            if(m_PhoneDiag.SendReadNVItem((nv_items_enum_type)i,bBuf,DIAG_NV_ITEM_SIZE))
            {
                myQCNFile.SetNVItem((nv_items_enum_type)i,bBuf,DIAG_NV_ITEM_SIZE);
            }
            for(int j=1;j<m_pDloadConfig->GetSubNum();j++)
            {
                memset(bBuf,0,DIAG_NV_ITEM_SIZE);
                if(m_PhoneDiag.SendReadNVItemExt((nv_items_enum_type)i,bBuf,DIAG_NV_ITEM_SIZE,j))
                {
                    myQCNFile.SetNVItem((nv_items_enum_type)i,bBuf,DIAG_NV_ITEM_SIZE,j);
                }
            }
            m_Curr++;
            SendProgressMsg();
        }
    }
    myQCNFile.Close();
    m_szProgName.Empty();
    return TRUE;
}

BOOL DLoadThread::NVRestore(void)
{
    CQcnFile            myQCNFile;
    BYTE                bBuf[DIAG_NV_ITEM_SIZE];
    nv_items_enum_type  nItem;
    UINT                nContext;

    if(!m_bNeedReset)
    {
        WaitMS(10000);
    }
    
    if(!m_PhoneDiag.EnterSPCMode())
    {
        WaitMS(8000);
        if(!m_PhoneDiag.EnterSPCMode())
        {
            m_nStatus = DLOAD_STATUS_NVRESTORE;
            LOG_OUTPUT(_T("DLoadThread::NVRestore Failed EnterSPCMode\n"));
            return FALSE;
        }
    }

    if(FALSE == m_PhoneDiag.SendModeChangeCmd(CMDIAG_MODE_OFFLINE_D_F))
    {
        m_nStatus = DLOAD_STATUS_NVRESTORE;
        LOG_OUTPUT(_T("DLoadThread::NVRestore Failed SendModeChangeCmd\n"));
        return FALSE;
    }
    
    m_bNeedReset = TRUE;

    if(!m_szNVAuto.IsEmpty() && m_bRunning)
    {
        m_szProgName = m_szNVAuto;
        
        if(FALSE == myQCNFile.Open(m_szNVAuto))
        {
            m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
            return FALSE;
        }

        if(myQCNFile.GetCount()>0 && m_bRunning)
        {
            m_Curr  = 0;
            m_Total = myQCNFile.GetCount();
            SendProgressMsg();
            
            for(int i=0;i<(int)myQCNFile.GetCount() && m_bRunning;i++)
            {
                if(myQCNFile.GetNVItem(i, &nItem, bBuf, DIAG_NV_ITEM_SIZE, &nContext))
                {
                    if(0 != nContext)
                    {
                        if(FALSE == m_PhoneDiag.SendWriteNVItemExt(nItem, bBuf, DIAG_NV_ITEM_SIZE, nContext))
                        {
                            m_nStatus = DLOAD_STATUS_NVRESTORE;
                            LOG_OUTPUT(_T("DLoadThread::NVRestore Failed SendWriteNVItemExt %d\n"),nItem);
                            //return FALSE;
                        }
                    }
                    else
                    {
                        if(FALSE == m_PhoneDiag.SendWriteNVItem(nItem, bBuf, DIAG_NV_ITEM_SIZE))
                        {
                            m_nStatus = DLOAD_STATUS_NVRESTORE;
                            LOG_OUTPUT(_T("DLoadThread::NVRestore Failed SendWriteNVItem %d\n"),nItem);
                            //return FALSE;
                        }
                    }
                }
                m_Curr++;
                SendProgressMsg();
            }
        }
        myQCNFile.Close();
    }
    
    // Restore Extra NV file
    if(!m_pDloadConfig->GetNVFile().IsEmpty() && m_bRunning)
    {
        m_szProgName = m_pDloadConfig->GetNVFile();

        CQcnFile &myNVQcn = m_pDloadConfig->GetNVQcn();
        if(FALSE == myNVQcn.IsValid())
        {
            m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
            return FALSE;
        }

        if(myNVQcn.GetCount()>0 && m_bRunning)
        {
            m_Curr  = 0;
            m_Total = myNVQcn.GetCount();
            SendProgressMsg();
            
            for(int i=0;i<(int)myNVQcn.GetCount() && m_bRunning;i++)
            {
                if(myNVQcn.GetNVItem(i, &nItem, bBuf, DIAG_NV_ITEM_SIZE, &nContext))
                {
                    if(0 != nContext)
                    {
                        if(FALSE == m_PhoneDiag.SendWriteNVItemExt(nItem, bBuf, DIAG_NV_ITEM_SIZE, nContext))
                        {
                            m_nStatus = DLOAD_STATUS_NVRESTORE;
                            LOG_OUTPUT(_T("DLoadThread::NVRestore Failed SendWriteNVItemEx %d\n"),nItem);
                            //return FALSE;
                        }
                    }
                    else
                    {
                        if(FALSE == m_PhoneDiag.SendWriteNVItem(nItem, bBuf, DIAG_NV_ITEM_SIZE))
                        {
                            m_nStatus = DLOAD_STATUS_NVRESTORE;
                            LOG_OUTPUT(_T("DLoadThread::NVRestore Failed SendWriteNVItem %d\n"),nItem);
                            //return FALSE;
                        }
                    }
                }
                m_Curr++;
                SendProgressMsg();
            }
        }
    }
    m_szProgName.Empty();
    return TRUE;
}

void DLoadThread::SyncMBNPath(void)
{
    m_szMBNPath = m_pDloadConfig->GetMBNPath();
    if(!m_szVersion.IsEmpty())
    {
        while(1)
        {
            CString szFlahInVer;
            int nIdx = m_szVersion.ReverseFind(_T('-'));
            if(nIdx)
            {
                szFlahInVer = m_szVersion.GetBuffer()+nIdx+1;
            }
            else
            {
                break;
            }
            
            if(szFlahInVer.GetLength() < 5)//必须是5个或5个以上字符的才认为是一个正确的Flash Name
            {
                break;
            }
            
            if(PathFileExists(m_szMBNPath+szFlahInVer))
            {
                m_szMBNPath += szFlahInVer+_T("\\");
                LOG_OUTPUT(_T("SyncMBNPath %s - %s\n"),m_szMBNPath,m_szVersion);
                return;
            }
            break;
        }
    }

    if(!m_szFlashHD.IsEmpty())
    {
        if(PathFileExists(m_szMBNPath+m_szFlashHD))
        {
            m_szMBNPath += m_szFlashHD+_T("\\");
            LOG_OUTPUT(_T("SyncMBNPath %s\n"),m_szMBNPath);
            return;
        }

        if(PathFileExists(m_szMBNPath+m_szFlashHD.GetAt(0)))
        {
            m_szMBNPath = m_szMBNPath+m_szFlashHD.GetAt(0)+_T("\\");
            LOG_OUTPUT(_T("SyncMBNPath %s\n"),m_szMBNPath);
            return;
        }
    }
}

BOOL DLoadThread::MemDump(void)
{
    CFile       myFile;
    CFileStatus myFileStatus;
    CString     szName;

    m_szProgName.Format(_T("MEMDUMP0x%X-0x%X.bin"),m_pDloadConfig->GetStartAddr(),m_pDloadConfig->GetReadSize()); 
    szName = m_pDloadConfig->GetMBNPath()+m_szProgName;
    
    if(FALSE == myFile.Open(szName, CFile::modeCreate|CFile::modeWrite))
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::MemDump Failed Open %s\n"),szName);
        return FALSE;
    }
    
    if(FALSE == m_bRunning || FALSE == m_PhoneDiag.SendAPHelloReq(&aphello))
    {
        m_nStatus = DLOAD_STATUS_FAILED;
        LOG_OUTPUT(_T("DLoadThread::MemDump Failed SendAPHelloReq\n"));
        return FALSE;
    }

    m_szProgName += _T(" - ");
    for(int i=0; i<aphello.flashnamelen; i++)
    {
        m_szProgName.AppendChar(aphello.flashname[i]);
    }
    
    m_Curr  = 0;
    m_Total = (DWORD)m_pDloadConfig->GetReadSize();
    SendProgressMsg();
    
    DWORD dwAddress = m_pDloadConfig->GetStartAddr();
    UINT wMaxLen = MIN(DEFAULT_MAX_FRAME_SIZE, aphello.preferred_block_size);
    UINT wLen;
    diag_ap_read_rsp_type myReadRsp;
    BOOL bRet = TRUE;
    while(m_bRunning)
    {
        if(m_Curr >= m_Total)
        {
            m_Curr = m_Total;
            SendProgressMsg();
            break;
        }
        
        wLen = MIN(wMaxLen,(m_Total-m_Curr));
        if(FALSE == m_PhoneDiag.SendAPReadReq(&myReadRsp,dwAddress,(WORD)wLen))
        {
            m_nStatus = DLOAD_STATUS_FAILED;
            bRet = FALSE;
            LOG_OUTPUT(_T("DLoadThread::MemDump Failed SendAPReadReq\n"));
            break;
        }
        
        myFile.Write(myReadRsp.data,wLen);
        dwAddress += wLen;
        m_Curr += wLen;
        if(m_Curr%0x10000 == 0)
        {
            SendProgressMsg();
        }
    }
    
    myFile.Close();
    if(bRet)
    {
        m_szProgName.Empty();
    }
    return bRet;
}

BOOL DLoadThread::DLoadEMMCImages(void)
{
    CXMLFile    myXMLFile;
    CXMLFile    myPatchXMLFile;
    CString     szName;
    string      tab,item;
    CString     szFileName;
    DWORD       num_partition_sectors;
    DWORD       size_in_KB;
    DWORD       start_byte_hex;
    DWORD       start_sector;
    DWORD       file_sector_offset;
    DWORD       physical_partition_number;
    bool        sparse;
    DWORD       dwSize;
    BOOL        bRet = TRUE;
    //CString     mySaveName;

    szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetEMMCProgram();
    if(m_bRunning == FALSE || !myXMLFile.openXMLDoc(szName))
    {
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage Failed Open %s\n"),szName);
        return FALSE;
    }
    
    HANDLE hDev = OpenDisk(m_EMMCPath);
    if (hDev == INVALID_HANDLE_VALUE)
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage Failed Open %s\n"),m_EMMCPath);
        return FALSE;
    }
    
    dwSize = sizeof(DISK_GEOMETRY_EX)+512-1;
    PDISK_GEOMETRY_EX lpDiskGeomtry = (PDISK_GEOMETRY_EX)new BYTE[dwSize];
    if(!GetDiskGeometry(hDev, lpDiskGeomtry, dwSize))
    {
        bRet = FALSE;
        goto EXIT;
    }
    
    DWORD NUM_DISK_SECTORS = (DWORD)(lpDiskGeomtry->DiskSize.QuadPart/lpDiskGeomtry->Geometry.BytesPerSector);
    m_dwSectorSize = lpDiskGeomtry->Geometry.BytesPerSector;
    LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage sectors %d %d\n"),NUM_DISK_SECTORS,lpDiskGeomtry->Geometry.BytesPerSector);
    
    //mySaveName = _T(".OK");
    //DLoadEMMCSaveImages(hDev,mySaveName,NUM_DISK_SECTORS);

    tab = myXMLFile.getTabFirst();
	while(m_bRunning && !tab.empty())
	{
        if(strcmp(tab.c_str(),"program") == 0)
        {
            szFileName = myXMLFile.getTabPropEx("filename");
            if(szFileName.IsEmpty())
            {
                tab = myXMLFile.getTabNext();
                continue;
            }
            num_partition_sectors       = StringToInt(myXMLFile.getTabProp("num_partition_sectors"));
            size_in_KB                  = StringToInt(myXMLFile.getTabProp("size_in_KB"));
            start_sector                = StringToInt(myXMLFile.getTabProp("start_sector"));
            file_sector_offset          = StringToInt(myXMLFile.getTabProp("file_sector_offset"));
            physical_partition_number   = StringToInt(myXMLFile.getTabProp("physical_partition_number"));
            sscanf(myXMLFile.getTabProp("start_byte_hex").c_str(),"0x%x",&start_byte_hex);
            if(strcmp(myXMLFile.getTabProp("start_byte_hex").c_str(),"true") == 0)
            {
                sparse = true;
            }
            else
            {
                sparse = false;
            }

            if(FALSE == DLoadEMMCImage(hDev, szFileName,num_partition_sectors,start_sector,file_sector_offset))
            {
                bRet = FALSE;
                break;
            }
        }
        else if(strcmp(tab.c_str(),"zeroout") == 0)
        {
            szFileName = myXMLFile.getTabPropEx("label");
            num_partition_sectors       = StringToInt(myXMLFile.getTabProp("num_partition_sectors"));
            start_sector                = StringToInt(myXMLFile.getTabProp("start_sector"),"NUM_DISK_SECTORS",NUM_DISK_SECTORS);
            if(FALSE == DLoadEMMCZeroout(hDev, szFileName, start_sector, num_partition_sectors))
            {
                bRet = FALSE;
                break;
            }
        }
		tab = myXMLFile.getTabNext();
	}

    if(m_pDloadConfig->GetEMMCPatch().IsEmpty())
    {
        goto EXIT;
    }
    
    szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetEMMCPatch();
    if(m_bRunning == FALSE || !myPatchXMLFile.openXMLDoc(szName))
    {
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage Failed Open %s\n"),szName);
        bRet = FALSE;
        goto EXIT;
    }
    
    DWORD byte_offset, size_in_bytes,value;
    tab = myPatchXMLFile.getTabFirst();
	while(m_bRunning && !tab.empty())
	{
        if(strcmp(tab.c_str(),"patch") == 0)
        {
            szFileName = myPatchXMLFile.getTabPropEx("filename");
            if(szFileName.IsEmpty())
            {
                tab = myPatchXMLFile.getTabNext();
                continue;
            }
            byte_offset                 = StringToInt(myPatchXMLFile.getTabProp("byte_offset"));
            start_sector                = StringToInt(myPatchXMLFile.getTabProp("start_sector"));
            size_in_bytes               = StringToInt(myPatchXMLFile.getTabProp("size_in_bytes"));
            physical_partition_number   = StringToInt(myPatchXMLFile.getTabProp("physical_partition_number"));
            value                       = StringToInt(myPatchXMLFile.getTabProp("value"),"NUM_DISK_SECTORS",NUM_DISK_SECTORS);
            
            if(FALSE == DLoadEMMCPatch(hDev, start_sector,byte_offset,size_in_bytes,(BYTE *)&value))
            {
                bRet = FALSE;
                break;
            }
        }
		tab = myPatchXMLFile.getTabNext();
	}
    //mySaveName = _T(".NOK");
    //DLoadEMMCSaveImages(hDev,mySaveName,NUM_DISK_SECTORS);
EXIT:
    delete lpDiskGeomtry;
    CloseDisk(hDev);
    return m_bRunning&&bRet;
}

BOOL DLoadThread::DLoadEMMCImage(HANDLE hDev, CString &szName, DWORD num_partition_sectors, DWORD start_sector, DWORD file_sector_offset)
{
    BOOL        bRet = TRUE;
    CFile       myFile;
    int         nMaxLen = WIRTE_BUFFER_LEN;
    int         nLen,nWrite;
    DWORD       nCB;
    BYTE       *pbData;

    LOG_OUTPUT(_T("program file %s, file offset %d, start sector %d, sectors %d\n"),szName,file_sector_offset,start_sector,num_partition_sectors);
    
    m_szProgName = szName;
    szName = m_pDloadConfig->GetMBNPath()+szName;     
        
    if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::shareDenyWrite))
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage Failed Open %s\n"),szName);
        return FALSE;
    }
    if(file_sector_offset)
    {
        myFile.Seek(file_sector_offset*m_dwSectorSize,CFile::begin);
    }
    LockVolume(hDev);
    SetFilePointer(hDev, m_dwSectorSize * start_sector, 0, FILE_BEGIN);
    
    m_Curr  = 0;
    m_Total = (DWORD)myFile.GetLength();
    SendProgressMsg();
    while(m_bRunning&&num_partition_sectors>0)
    {
        nLen = myFile.Read(m_bData, nMaxLen);
        if(nLen == 0)
        {
            SendProgressMsg();
            break;
        }
        
        pbData  = m_bData;
        nWrite = ((nLen+(m_dwSectorSize-1))/m_dwSectorSize)*m_dwSectorSize;
        
        // Sending
        while(nWrite>0&&m_bRunning&&num_partition_sectors>0)
        {
            LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage WriteFile ADDR 0x%x Size %d\n"),SetFilePointer(hDev, 0, 0, FILE_CURRENT),nWrite);
            if(!WriteFile(hDev, m_bData, nWrite, (LPDWORD)&nCB, NULL))
            {
                DWORD dwErr = GetLastError();
                LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage Failed WriteFile %d\n"),dwErr);
            }
            else
            {
                pbData += nCB;
                nWrite -= nCB;
                num_partition_sectors -= nCB/m_dwSectorSize;
                SetFilePointer(hDev, nCB, 0, FILE_CURRENT);
            }
        }
        
        m_Curr += nLen;
        if(m_Curr%0x10000 == 0)
        {
            SendProgressMsg();
        }
    }
    UnlockVolume(hDev);
    myFile.Close();
    return bRet;
}

BOOL DLoadThread::DLoadEMMCPatch(HANDLE hDev, DWORD start_sector, DWORD byte_offset, DWORD size_in_bytes, BYTE *value)
{
    BOOL        bRet = TRUE;
    DWORD       nCB;
    
    m_szProgName = _T("Patch");
    
    LOG_OUTPUT(_T("Patch start sector %d, offset %d\n"),start_sector,byte_offset);
    LockVolume(hDev);
    
    m_Curr  = 0;
    m_Total = 100;
    SendProgressMsg();
    
    SetFilePointer(hDev, m_dwSectorSize * start_sector, 0, FILE_BEGIN);
    if(!ReadFile(hDev, m_bData, m_dwSectorSize, (LPDWORD)&nCB, NULL))
    {
        DWORD dwErr = GetLastError();
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCPatch Failed ReadFile %d\n"),dwErr);
        bRet = FALSE;
        goto EXIT;
    }

    memcpy(&m_bData[byte_offset],value,size_in_bytes);
    m_Curr  = 50;
    m_Total = 100;
    SendProgressMsg();
    
    SetFilePointer(hDev, m_dwSectorSize * start_sector, 0, FILE_BEGIN);
    if(!WriteFile(hDev, m_bData, m_dwSectorSize, (LPDWORD)&nCB, NULL))
    {
        DWORD dwErr = GetLastError();
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCPatch Failed WriteFile %d\n"),dwErr);
        bRet = FALSE;
        goto EXIT;
    }
EXIT:
    m_Curr  = 100;
    m_Total = 100;
    SendProgressMsg();
    UnlockVolume(hDev);
    return bRet;
}

BOOL DLoadThread::DLoadEMMCZeroout(HANDLE hDev, CString &szName, DWORD start_sector, DWORD num_partition_sectors)
{
    BOOL        bRet = TRUE;
    int         nMaxLen = WIRTE_BUFFER_LEN;
    int         nWrite;
    DWORD       nCB;
    BYTE       *pbData;
    
    m_szProgName = szName;
    
    LOG_OUTPUT(_T("Zero Out file %s start sector %d, sectors %d\n"),szName, start_sector,num_partition_sectors);
    LockVolume(hDev);
    SetFilePointer(hDev, m_dwSectorSize * start_sector, 0, FILE_BEGIN);
    
    memset(m_bData,0,WIRTE_BUFFER_LEN);
    m_Curr  = 0;
    m_Total = num_partition_sectors;
    SendProgressMsg();
    while(m_bRunning)
    {
        if(num_partition_sectors == 0)
        {
            SendProgressMsg();
            break;
        }
        
        pbData  = m_bData;
        nWrite = MIN(num_partition_sectors*m_dwSectorSize,WIRTE_BUFFER_LEN);
        
        // Sending
        while(nWrite>0&&m_bRunning)
        {
            LOG_OUTPUT(_T("DLoadThread::DLoadEMMCZeroout WriteFile ADDR 0x%x Size %d\n"),SetFilePointer(hDev, 0, 0, FILE_CURRENT),nWrite);
            if(!WriteFile(hDev, m_bData, nWrite, (LPDWORD)&nCB, NULL))
            {
                DWORD dwErr = GetLastError();
                LOG_OUTPUT(_T("DLoadThread::DLoadEMMCZeroout Failed WriteFile %d\n"),dwErr);
            }
            else
            {
                pbData += nCB;
                nWrite -= nCB;
                num_partition_sectors -= nCB/m_dwSectorSize;
                SetFilePointer(hDev, nCB, 0, FILE_CURRENT);
            }
        }
        
        m_Curr = m_Total-num_partition_sectors;
        SendProgressMsg();
    }
    UnlockVolume(hDev);
    return bRet;
}

BOOL DLoadThread::DLoadEMMCSaveImages(HANDLE hDev, CString &szSuffix, DWORD NUM_DISK_SECTORS)
{
    CXMLFile    myXMLFile;
    CString     szName;
    string      tab,item;
    CString     szFileName;
    DWORD       num_partition_sectors;
    DWORD       start_sector;
    BOOL        bRet = TRUE;

    szName = m_pDloadConfig->GetMBNPath()+m_pDloadConfig->GetEMMCProgram();
    if(m_bRunning == FALSE || !myXMLFile.openXMLDoc(szName))
    {
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCSaveImages Failed Open %s\n"),szName);
        return FALSE;
    }
    
    tab = myXMLFile.getTabFirst();
	while(m_bRunning && !tab.empty())
	{
        if(strcmp(tab.c_str(),"program") == 0)
        {
            szFileName = myXMLFile.getTabPropEx("filename");
            if(szFileName.IsEmpty())
            {
                tab = myXMLFile.getTabNext();
                continue;
            }
            num_partition_sectors       = StringToInt(myXMLFile.getTabProp("num_partition_sectors"));
            start_sector                = StringToInt(myXMLFile.getTabProp("start_sector"));
            
            CFileStatus status;
            CFile::GetStatus(m_pDloadConfig->GetMBNPath()+szFileName,status);
            
            if(FALSE == DLoadEMMCSave(hDev, szFileName+szSuffix,start_sector,(int32)status.m_size))
            {
                bRet = FALSE;
                break;
            }
        }
		tab = myXMLFile.getTabNext();
	}
    return m_bRunning&&bRet;
}

BOOL DLoadThread::DLoadEMMCSave(HANDLE hDev, CString &szName, DWORD start_sector, int32 nSize)
{
    BOOL        bRet = TRUE;
    CFile       myFile;
    int         nMaxLen = WIRTE_BUFFER_LEN;
    DWORD       nCB;
    int32       nSectors = (nSize+m_dwSectorSize-1)/m_dwSectorSize;
    int32       nReadLen;

    LOG_OUTPUT(_T("save file %s, start sector %d, nSize %d\n"),szName,start_sector,nSize);

    m_szProgName = szName;
    szName = m_pDloadConfig->GetMBNPath()+szName;     
    
    if(FALSE == myFile.Open(szName, CFile::modeRead|CFile::modeWrite|CFile::modeCreate))
    {
        m_nStatus = DLOAD_STATUS_CANNOTOPENFILE;
        LOG_OUTPUT(_T("DLoadThread::DLoadEMMCImage Failed Open %s\n"),szName);
        return FALSE;
    }
    LockVolume(hDev);
    SetFilePointer(hDev, m_dwSectorSize * start_sector, 0, FILE_BEGIN);
    
    m_Curr  = 0;
    m_Total = nSectors;
    SendProgressMsg();

    while(m_bRunning&&nSize>0)
    {
        nReadLen = MIN(nMaxLen,(nSectors*(int32)m_dwSectorSize));
        bRet = ReadFile(hDev, m_bData, nReadLen,(LPDWORD)&nCB, NULL);
        if(!bRet || nCB == 0)
        {
            SendProgressMsg();
            break;
        }
        
        nSize -= nCB;
        nSectors -= nCB/m_dwSectorSize;
        SetFilePointer(hDev, nCB, 0, FILE_CURRENT);
        if(nSize > 0)
        {
            myFile.Write(m_bData,nCB);
        }
        else
        {
            myFile.Write(m_bData,nCB+nSize);
        }
        
        m_Curr = m_Total-nSectors;
        SendProgressMsg();
    }
    UnlockVolume(hDev);
    myFile.Close();
    return TRUE;
}

BOOL DLoadThread::DLoadFindEMMC(DWORD nHubIndex, DWORD nPortIndex)
{
    CUSBPorts myEMMCDev;
    BOOL bRet = FALSE;
    
    if(!myEMMCDev.EnumUsbDeviceByIndexInit(&udisk_usb_class_id,m_UsbPorts.GetHubIndex(),m_UsbPorts.GetPortIndex(),3))
    {
        return FALSE;
    }
    
    do
    {
        bRet = myEMMCDev.EnumUsbDeviceByIndexNext(&udisk_usb_class_id);
        if(bRet)
        {
            m_EMMCPath = myEMMCDev.GetUsbPath();
            break;
        }
    }while(bRet);
    myEMMCDev.EnumUsbDeviceByIndexEnd();
    return bRet;
}

HANDLE DLoadThread::OpenDisk(LPCTSTR filename) 
{  
    HANDLE hDisk; 
    
    // 打开设备  
    hDisk = ::CreateFile(filename, // 文件名  
        GENERIC_READ | GENERIC_WRITE, // 读写方式   
        FILE_SHARE_READ | FILE_SHARE_WRITE, // 共享方式 
        NULL, // 默认的安全描述符 
        OPEN_EXISTING, // 创建方式 
        0, // 不需设置文件属性  
        NULL); // 不需参照模板文件 
    return hDisk; 
}

void DLoadThread::CloseDisk(HANDLE hDisk)
{
    CloseHandle(hDisk);
}

BOOL DLoadThread::GetDisksProperty(HANDLE hDisk, PSTORAGE_DEVICE_DESCRIPTOR pDevDesc)
{
    STORAGE_PROPERTY_QUERY    Query;    
    DWORD dwOutBytes;                
    BOOL bResult;                    
 
    Query.PropertyId = StorageDeviceProperty;
    Query.QueryType = PropertyStandardQuery;
 
    bResult = ::DeviceIoControl(hDisk,            
            IOCTL_STORAGE_QUERY_PROPERTY,            
            &Query, sizeof(STORAGE_PROPERTY_QUERY),    
            pDevDesc, pDevDesc->Size,                
            &dwOutBytes,                            
            (LPOVERLAPPED)NULL);                    
 
    return bResult;
}

// 获取磁盘参数  
BOOL DLoadThread::GetDiskGeometry(HANDLE hDisk, PDISK_GEOMETRY_EX lpGeometryEx, DWORD dwSize) 
{  
    DWORD dwOutBytes;  
    BOOL bResult; 
    // 用IOCTL_DISK_GET_DRIVE_GEOMETRY取磁盘参数 
    bResult = ::DeviceIoControl(hDisk, // 设备句柄   
        IOCTL_DISK_GET_DRIVE_GEOMETRY_EX, // 取磁盘参数  
        NULL, 0, // 不需要输入数据  
        lpGeometryEx, dwSize, // 输出数据缓冲区 
        &dwOutBytes, // 输出数据长度  
        (LPOVERLAPPED)NULL); // 用同步I/O 
    return bResult; 
}

// 将卷锁定  
BOOL DLoadThread::LockVolume(HANDLE hDisk) 
{  
    DWORD dwOutBytes;  
    BOOL bResult; 

    // 用FSCTL_LOCK_VOLUME锁卷 
    bResult = ::DeviceIoControl(hDisk, // 设备句柄 
        FSCTL_LOCK_VOLUME, // 锁卷 
        NULL, 0, // 不需要输入数据  
        NULL, 0, // 不需要输出数据 
        &dwOutBytes, // 输出数据长度  
        (LPOVERLAPPED)NULL); // 用同步I/O 
    return bResult; 
}

// 将卷解锁  
BOOL DLoadThread::UnlockVolume(HANDLE hDisk) 
{  
    DWORD dwOutBytes;  
    BOOL bResult; 
    
    // 用FSCTL_UNLOCK_VOLUME开卷锁 
    bResult = ::DeviceIoControl(hDisk, // 设备句柄  
        FSCTL_UNLOCK_VOLUME, // 开卷锁 
        NULL, 0, // 不需要输入数据 
        NULL, 0, // 不需要输出数据 
        &dwOutBytes, // 输出数据长度  
        (LPOVERLAPPED)NULL); // 用同步I/O 
    return bResult; 
} 

int32 DLoadThread::StringToInt(string &szExpre, char *szReplace, int32 nReplace)
{
    int nIdx;
    int32 nRet = 0;

    if(szExpre.empty())
    {
        return nRet;
    }
    
    if(NULL == szReplace)
    {
        return atoi(szExpre.c_str());
    }
    
    nIdx = szExpre.find(szReplace);
    if(nIdx >= 0)
    {
        string szNew = szExpre.c_str()+strlen(szReplace);
        nRet = nReplace+atoi(szNew.c_str());
    }
    else
    {
        nRet = atoi(szExpre.c_str());
    }
    return nRet;
}