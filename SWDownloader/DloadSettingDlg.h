#pragma once
#include "afxwin.h"
#include "DloadConfig.h"

#define CDLOADSETTING_REFRESH_TIMER     1
// CDloadSettingDlg 对话框

class CDloadSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CDloadSettingDlg)

public:
	CDloadSettingDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDloadSettingDlg();
    virtual BOOL OnInitDialog();

// 对话框数据
	enum { IDD = IDD_SETTING_DIALOG };

private:
    void            UpdateConfig(void);
    void            SyncConfig(void);

    CDloadConfig   *m_pDloadConfig;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
    CEdit m_cNVEdit;
    CEdit m_cNVFileEdit;
    CEdit m_cEFSEdit;
    CButton m_cNVBackupCheck;
    CButton m_cNVRestoreCheck;
    CButton m_cEFSBackupCheck;
    CButton m_cEFSRestoreCheck;
    CButton m_cMBNCheck;
    CButton m_cEraseCheck;
    CButton m_cDBLCheck;
    CButton m_cFSBLCheck;
    CButton m_cOSBLCheck;
    CButton m_cAMSSCheck;
    CButton m_cCEFSCheck;
    CButton m_cAPPSBLCheck;
    CButton m_cAPPSCheck;
    CButton m_cAPPSCEFSCheck;
    CButton m_cOBLCheck;
    CButton m_cPBLCheck;
    CButton m_cQCSBLCheck;
    CButton m_cOEMSBLCheck;
    CButton m_cFOTAUICheck;
    CButton m_cDSP1Check;
    CButton m_cDSP2Check;
    CButton m_cFLASHBINCheck;
    CButton m_cRAWCheck;
    CButton m_cCUSTOMCheck;
    CButton m_cARDCheck;
    CButton m_cARDBOOTCheck;
    CButton m_cARDSYSCheck;
    CButton m_cARDUSERCheck;
    CButton m_cARDRCYCheck;
    CButton m_cARDCLRCACHECheck;
    CButton m_cLogEnableCheck;
    CButton m_cLockedCheck;
    CButton m_cFACTCheck;
    CButton m_cMsimageCheck;
    
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedDefaultButton();
    afx_msg void OnBnClickedNvpathButton();
    afx_msg void OnBnClickedEfsButton();
    afx_msg void OnBnClickedNvfileButton();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnBnClickedMbnCheck();
//    afx_msg void OnSetFocus(CWnd* pOldWnd);
    afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
    afx_msg void OnBnClickedArdCheck();
    CButton m_cMemDumpCheck;
    CEdit m_cAddrEdit;
    CEdit m_cSizeEdit;
    afx_msg void OnEnChangeAddrEdit();
    afx_msg void OnEnChangeSizeEdit();
    bool IsCodeAvaliable(CString &szCode);
    afx_msg void OnBnClickedMemdumpCheck();
    afx_msg void OnBnClickedFactCheck();
};
