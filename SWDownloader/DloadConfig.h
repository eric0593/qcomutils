#pragma once
#include "CIniFile.h"
#include "CChipset.h"
#include "SerialComm.h"
#include "QcnFile.h"

#define DLOAD_HISTORY_NUM                   10
#define DLOAD_CFG_FILE                      _T(".\\DloadConfig.ini")
#define DLOAD_CFG_COM_SECTION               _T("COMSETTING")
#define DLOAD_CFG_COMNUM_KEY                _T("COMNUM")
#define DLOAD_CFG_COMNO_KEY                 _T("COMPORT%d")
#define DLOAD_CFG_DLOAD_SECTION             _T("DLOADSETTING")
#define DLOAD_CFG_CHIPSET_KEY               _T("CHIPSET")
#define DLOAD_CFG_MBNPATH_KEY               _T("MBNPATH%d")
#define DLOAD_CFG_NVPATH_KEY                _T("NVPATH")
#define DLOAD_CFG_EFSPATH_KEY               _T("EFSPATH")
#define DLOAD_CFG_MBN_KEY                   _T("DLOADMBN")
#define DLOAD_CFG_FACT_KEY                  _T("DLOADFACT")
#define DLOAD_CFG_ERASE_KEY                 _T("ERASEFLASH")
#define DLOAD_CFG_PBL_KEY                   _T("DLOADPBL")
#define DLOAD_CFG_QCSBL_KEY                 _T("DLOADQCSBL")
#define DLOAD_CFG_OEMSBL_KEY                _T("DLOADOEMSBL")
#define DLOAD_CFG_AMSS_KEY                  _T("DLOADAMSS")
#define DLOAD_CFG_APPS_KEY                  _T("DLOADAPPS")
#define DLOAD_CFG_OBL_KEY                   _T("DLOADOBL")
#define DLOAD_CFG_FOTAUI_KEY                _T("DLOADFOTAUI")
#define DLOAD_CFG_CEFS_KEY                  _T("DLOADCEFS")
#define DLOAD_CFG_APPSBL_KEY                _T("DLOADAPPSBL")
#define DLOAD_CFG_APPS_CEFS_KEY             _T("DLOADAPPS_CEFS")
#define DLOAD_CFG_FLASH_BIN_KEY             _T("DLOADFLASH_BIN")
#define DLOAD_CFG_DSP1_KEY                  _T("DLOADDSP1")
#define DLOAD_CFG_CUSTOM_KEY                _T("DLOADCUSTOM")
#define DLOAD_CFG_DBL_KEY                   _T("DLOADDBL")
#define DLOAD_CFG_FSBL_KEY                  _T("DLOADFSBL")
#define DLOAD_CFG_OSBL_KEY                  _T("DLOADOSBL")
#define DLOAD_CFG_DSP2_KEY                  _T("DLOADDSP2")
#define DLOAD_CFG_RAW_KEY                   _T("DLOADRAW")
#define DLOAD_CFG_MSIMAGE_KEY               _T("DLOADMSIMAGE")
#define DLOAD_CFG_ARD_KEY                   _T("DLOADARD")
#define DLOAD_CFG_ARDBOOT_KEY               _T("DLOADARDBOOT")
#define DLOAD_CFG_ARDSYS_KEY                _T("DLOADARDSYS")
#define DLOAD_CFG_ARDUSER_KEY               _T("DLOADARDUSER")
#define DLOAD_CFG_ARDRCY_KEY                _T("DLOADARDRCY")
#define DLOAD_CFG_ARDCLRCACHE_KEY           _T("DLOADARDCLRCACHE")
#define DLOAD_CFG_NVBACKUP_KEY              _T("NVBACKUP")
#define DLOAD_CFG_NVRESTORE_KEY             _T("NVRESTORE")
#define DLOAD_CFG_EFSBACKUP_KEY             _T("EFSBACKUP")
#define DLOAD_CFG_EFSRESTORE_KEY            _T("EFSRESTORE")
#define DLOAD_CFG_NVFILE_KEY                _T("NVFILE")
#define DLOAD_CFG_AUTODLOAD_KEY             _T("AUTODLOAD")
#define DLOAD_CFG_LOGENABLE_KEY             _T("LOGENABLE")
#define DLOAD_CFG_LOCKED_KEY                _T("LOCKED")

#define DLOAD_DEFAULT_NVPATH                _T(".\\nv\\")
#define DLOAD_DEFAULT_EFSPATH               _T(".\\efs\\")



class CDloadConfig
{
private:
    CDloadConfig(void);
    ~CDloadConfig(void);

public:
    static int              Create(CDloadConfig **ppDloadConfig);
    static int              Release(void);
    void                    ResetConfig(void);
    int                     GetCOMNum(void)                     {return m_nCOMNum;};
    int                     GetCOMAvailNum(void);
    int                     GetCOMPort(int nIdx)                {return m_pCOMList[nIdx];};
    void                    SetCOMPort(int nIdx, int nPort)     {m_pCOMList[nIdx] = nPort;};
    int                     GetCOMIdxByID(int nPort);
    int                     GetChipsetIdx(void)                 {return m_nChipsetIdx;};
    void                    SetChipsetIdx(int nIdx)             {m_nChipsetIdx = nIdx;};
    CString                &GetNVPath(void)                     {return m_szNVPath;};
    void                    SetNVPath(const CString &szPath);
    CString                &GetNVFile(void)                     {return m_szNVFile;};
    void                    SetNVFile(const CString &szPath)    {m_szNVFile = szPath;};
    CString                &GetEFSPath(void)                    {return m_szEFSPath;};
    void                    SetEFSPath(const CString &szPath);
    CString                &GetMBNPath(void)                    {return (m_szMBNPath.GetCount()>0)?m_szMBNPath[0]:m_Empty;};
    CStringArray           &GetMBNPathHist(void)                {return m_szMBNPath;};
    void                    SetMBNPath(const CString &szPath, int nIdx=0, BOOL bUpdateIni=TRUE);
    BOOL                    IsDloadMBN(void)                    {return m_bDloadMBN;};
    void                    SetDloadMBN(BOOL bEnable)           {m_bDloadMBN = bEnable;};
    BOOL                    IsDloadFACT(void)                   {return m_bDloadFACT;};
    void                    SetDloadFACT(BOOL bEnable)          {m_bDloadFACT = bEnable;};
    BOOL                    IsEraseFlash(void)                  {return m_bEraseFlash && IsMultiMBN() == CHIPSET_MULTIBOOT_NONE;};
    void                    SetEraseFlash(BOOL bEnable)         {m_bEraseFlash = bEnable;};
    BOOL                    IsDloadPBL(void)                    {return m_bDloadPBL && IsMultiMBN() == CHIPSET_MULTIBOOT_1 && !GetPBLName().IsEmpty();};
    void                    SetDloadPBL(BOOL bEnable)           {m_bDloadPBL = bEnable;};
    BOOL                    IsDloadQCSBL(void)                  {return m_bDloadQCSBL && IsMultiMBN() == CHIPSET_MULTIBOOT_1 && !GetQCSBLName().IsEmpty() && !GetQCSBLHDCFGName().IsEmpty();};
    void                    SetDloadQCSBL(BOOL bEnable)         {m_bDloadQCSBL = bEnable;};
    BOOL                    IsDloadOEMSBL(void)                 {return m_bDloadOEMSBL && IsMultiMBN() == CHIPSET_MULTIBOOT_1 && !GetOEMSBLName().IsEmpty() && !GetOEMSBLHDName().IsEmpty();};
    void                    SetDloadOEMSBL(BOOL bEnable)        {m_bDloadOEMSBL = bEnable;};
    BOOL                    IsDloadAMSS(void)                   {return m_bDloadAMSS && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetAMSSName().IsEmpty() && !(GetAMSSHDName().IsEmpty() && IsMultiMBN() == CHIPSET_MULTIBOOT_1);};
    void                    SetDloadAMSS(BOOL bEnable)          {m_bDloadAMSS = bEnable;};
    BOOL                    IsDloadAPPS(void)                   {return m_bDloadAPPS  && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetAPPSName().IsEmpty() && !(GetAPPSHDName().IsEmpty() && IsMultiMBN() == CHIPSET_MULTIBOOT_1);};
    void                    SetDloadAPPS(BOOL bEnable)          {m_bDloadAPPS = bEnable;};
    BOOL                    IsDloadOBL(void)                    {return m_bDloadOBL && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetOBLName().IsEmpty();};
    void                    SetDloadOBL(BOOL bEnable)           {m_bDloadOBL = bEnable;};
    BOOL                    IsDloadFOTAUI(void)                 {return m_bDloadFOTAUI && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetFOTAUIName().IsEmpty();};
    void                    SetDloadFOTAUI(BOOL bEnable)        {m_bDloadFOTAUI = bEnable;};
    BOOL                    IsDloadCEFS(void)                   {return m_bDloadCEFS && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetCEFSName().IsEmpty();};
    void                    SetDloadCEFS(BOOL bEnable)          {m_bDloadCEFS = bEnable;};
    BOOL                    IsDloadAPPSBL(void)                 {return m_bDloadAPPSBL  && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetAPPSBLName().IsEmpty() && !(GetAPPSBLHDName().IsEmpty() && IsMultiMBN() == CHIPSET_MULTIBOOT_1);};
    void                    SetDloadAPPSBL(BOOL bEnable)        {m_bDloadAPPSBL = bEnable;};
    BOOL                    IsDloadAPPSCEFS(void)               {return m_bDloadAPPSCEFS && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetAPPSCEFSName().IsEmpty();};
    void                    SetDloadAPPSCEFS(BOOL bEnable)      {m_bDloadAPPSCEFS = bEnable;};
    BOOL                    IsDloadFLASHBIN(void)               {return m_bDloadFLASHBIN && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetFLASHBINName().IsEmpty();};
    void                    SetDloadFLASHBIN(BOOL bEnable)      {m_bDloadFLASHBIN = bEnable;};
    BOOL                    IsDloadDSP1(void)                   {return m_bDloadDSP1 && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetDSP1Name().IsEmpty();};
    void                    SetDloadDSP1(BOOL bEnable)          {m_bDloadDSP1 = bEnable;};
    BOOL                    IsDloadCUSTOM(void)                 {return m_bDloadCUSTOM && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetCUSTOMName().IsEmpty();};
    void                    SetDloadCUSTOM(BOOL bEnable)        {m_bDloadCUSTOM = bEnable;};
    BOOL                    IsDloadDBL(void)                    {return m_bDloadDBL && IsMultiMBN() == CHIPSET_MULTIBOOT_2 && !GetDBLName().IsEmpty();};
    void                    SetDloadDBL(BOOL bEnable)           {m_bDloadDBL = bEnable;};
    BOOL                    IsDloadFSBL(void)                   {return m_bDloadFSBL && IsMultiMBN() == CHIPSET_MULTIBOOT_2 && !GetFSBLName().IsEmpty();};
    void                    SetDloadFSBL(BOOL bEnable)          {m_bDloadFSBL = bEnable;};
    BOOL                    IsDloadOSBL(void)                   {return m_bDloadOSBL && IsMultiMBN() == CHIPSET_MULTIBOOT_2 && !GetOSBLName().IsEmpty();};
    void                    SetDloadOSBL(BOOL bEnable)          {m_bDloadOSBL = bEnable;};
    BOOL                    IsDloadDSP2(void)                   {return m_bDloadDSP2 && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetDSP2Name().IsEmpty();};
    void                    SetDloadDSP2(BOOL bEnable)          {m_bDloadDSP2 = bEnable;};
    BOOL                    IsDloadRAW(void)                    {return m_bDloadRAW && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetRAWName().IsEmpty();};
    void                    SetDloadRAW(BOOL bEnable)           {m_bDloadRAW = bEnable;};
    BOOL                    IsDloadMsimage(void)                {return m_bDloadMsimage && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetMsimageName().IsEmpty();};
    void                    SetDloadMsimage(BOOL bEnable)       {m_bDloadMsimage = bEnable;};
    BOOL                    IsDloadARD(void)                    {return m_bDloadARD && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetARDBOOTName().IsEmpty();};
    void                    SetDloadARD(BOOL bEnable)           {m_bDloadARD = bEnable;};
    BOOL                    IsDloadARDBOOT(void)                {return m_bDloadARDBOOT && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetARDBOOTName().IsEmpty();};
    void                    SetDloadARDBOOT(BOOL bEnable)       {m_bDloadARDBOOT = bEnable;};
    BOOL                    IsDloadARDSYS(void)                 {return m_bDloadARDSYS && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetARDSYSName().IsEmpty();};
    void                    SetDloadARDSYS(BOOL bEnable)        {m_bDloadARDSYS = bEnable;};
    BOOL                    IsDloadARDUSER(void)                {return m_bDloadARDUSER && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetARDUSERName().IsEmpty();};
    void                    SetDloadARDUSER(BOOL bEnable)       {m_bDloadARDUSER = bEnable;};
    BOOL                    IsDloadARDRCY(void)                 {return m_bDloadARDRCY && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE && !GetARDRCYName().IsEmpty();};
    void                    SetDloadARDRCY(BOOL bEnable)        {m_bDloadARDRCY = bEnable;};
    BOOL                    IsDloadARDCLRCACHE(void)            {return m_bDloadARDCLRCACHE && IsMultiMBN() != CHIPSET_MULTIBOOT_NONE;};
    void                    SetDloadARDCLRCACHE(BOOL bEnable)   {m_bDloadARDCLRCACHE = bEnable;};
    BOOL                    IsNVBackup(void)                    {return m_bNVBackup;};
    void                    SetNVBackup(BOOL bEnable)           {m_bNVBackup = bEnable;};
    BOOL                    IsNVRestore(void)                   {return m_bNVRestore;};
    void                    SetNVRestore(BOOL bEnable)          {m_bNVRestore = bEnable;};
    BOOL                    IsEFSBackup(void)                   {return m_bEFSBackup;};
    void                    SetEFSBackup(BOOL bEnable)          {m_bEFSBackup = bEnable;};
    BOOL                    IsEFSRestore(void)                  {return m_bEFSRestore;};
    void                    SetEFSRestore(BOOL bEnable)         {m_bEFSRestore = bEnable;};
    BOOL                    IsAutoDLoad(void)                   {return m_bAutoDLoad;};
    void                    SetAutoDLoad(BOOL bEnable)          {m_bAutoDLoad = bEnable;};
    BOOL                    IsLogEnable(void)                   {return m_bLogEnable;};
    void                    SetLogEnable(BOOL bEnable)          {m_bLogEnable = bEnable;};
    BOOL                    IsLocked(void)                      {return m_bLocked;};
    void                    SetLocked(BOOL bEnable)             {m_bLocked = bEnable;};
    BOOL                    IsMemDump(void)                     {return m_bMemDump;};
    void                    SetMemDump(BOOL bEnable)            {m_bMemDump = bEnable;};
    dword                   GetStartAddr(void)                  {return m_dwStartAddr;};
    void                    SetStartAddr(dword dwStart)         {m_dwStartAddr = dwStart;};
    dword                   GetReadSize(void)                   {return m_dwReadSize;};
    void                    SetReadSize(dword dwSize)           {m_dwReadSize = dwSize;};

    BYTE                    IsMultiMBN(void)                    {return m_pChipset->IsMultiMBN(m_nChipsetIdx);};
    BYTE                    GetSecMode(void)                    {return m_pChipset->GetSecMode(m_nChipsetIdx);};
    BOOL                    IsJumbBoot(DWORD *pJumpAddr)        {return m_pChipset->IsJumbBoot(m_nChipsetIdx,pJumpAddr);};
    CString                &GetARMPRGName(void)                 {return m_pChipset->GetARMPRGName(m_nChipsetIdx);};
    CString                &GetPartitionName(void)              {return m_pChipset->GetPartitionName(m_nChipsetIdx);};
    CString                &GetPBLName(void)                    {return m_pChipset->GetDBLName(m_nChipsetIdx);};
    CString                &GetQCSBLHDCFGName(void)             {return m_pChipset->GetQCSBLHDCFGName(m_nChipsetIdx);};
    CString                &GetQCSBLName(void)                  {return m_pChipset->GetQCSBLName(m_nChipsetIdx);};
    CString                &GetOEMSBLHDName(void)               {return m_pChipset->GetOEMSBLHDName(m_nChipsetIdx);};
    CString                &GetOEMSBLName(void)                 {return m_pChipset->GetOEMSBLName(m_nChipsetIdx);};
    CString                &GetAMSSHDName(void)                 {return m_pChipset->GetAMSSHDName(m_nChipsetIdx);};
    CString                &GetAMSSName(void)                   {return m_pChipset->GetAMSSName(m_nChipsetIdx);};
    CString                &GetAPPSHDName(void)                 {return m_pChipset->GetAPPSHDName(m_nChipsetIdx);};
    CString                &GetAPPSName(void)                   {return m_pChipset->GetAPPSName(m_nChipsetIdx);};
    CString                &GetOBLName(void)                    {return m_pChipset->GetOBLName(m_nChipsetIdx);};
    CString                &GetFOTAUIName(void)                 {return m_pChipset->GetFOTAUIName(m_nChipsetIdx);};
    CString                &GetCEFSName(void)                   {return m_pChipset->GetCEFSName(m_nChipsetIdx);};
    CString                &GetAPPSBLHDName(void)               {return m_pChipset->GetAPPSBLHDName(m_nChipsetIdx);};
    CString                &GetAPPSBLName(void)                 {return m_pChipset->GetAPPSBLName(m_nChipsetIdx);};
    CString                &GetAPPSCEFSName(void)               {return m_pChipset->GetAPPSCEFSName(m_nChipsetIdx);};
    CString                &GetFLASHBINName(void)               {return m_pChipset->GetFLASHBINName(m_nChipsetIdx);};
    CString                &GetDSP1Name(void)                   {return m_pChipset->GetDSP1Name(m_nChipsetIdx);};
    CString                &GetCUSTOMName(void)                 {return m_pChipset->GetCUSTOMName(m_nChipsetIdx);};
    CString                &GetDBLName(void)                    {return m_pChipset->GetDBLName(m_nChipsetIdx);};
    CString                &GetFSBLName(void)                   {return m_pChipset->GetFSBLName(m_nChipsetIdx);};
    CString                &GetOSBLName(void)                   {return m_pChipset->GetOSBLName(m_nChipsetIdx);};
    CString                &GetDSP2Name(void)                   {return m_pChipset->GetDSP2Name(m_nChipsetIdx);};
    CString                &GetRAWName(void)                    {return m_pChipset->GetRAWName(m_nChipsetIdx);};
    CString                &GetARDBOOTName(void)                {return m_pChipset->GetARDBOOTName(m_nChipsetIdx);};
    CString                &GetARDSYSName(void)                 {return m_pChipset->GetARDSYSName(m_nChipsetIdx);};
    CString                &GetARDUSERName(void)                {return m_pChipset->GetARDUSERName(m_nChipsetIdx);};
    CString                &GetARDRCYName(void)                 {return m_pChipset->GetARDRCYName(m_nChipsetIdx);};
    CString                &GetFactName(void)                   {return m_pChipset->GetFactName(m_nChipsetIdx);};
    CString                &GetNVSample(void)                   {return m_pChipset->GetNVSample(m_nChipsetIdx);};
    CString                &GetEMMCProgram(void)                {return m_pChipset->GetEMMCProgram(m_nChipsetIdx);};
    CString                &GetEMMCPatch(void)                  {return m_pChipset->GetEMMCPatch(m_nChipsetIdx);};
    CString                &GetMsimageName(void)                {return m_pChipset->GetMsimageName(m_nChipsetIdx);};
    int                     GetSubNum(void)                     {return m_pChipset->GetSubNum(m_nChipsetIdx);};
    BOOL                    IsEMMC(void)                        {return m_pChipset->IsEMMC(m_nChipsetIdx);};
    BOOL                    IsUSBBoot(void)                     {return m_pChipset->IsUSBBoot(m_nChipsetIdx);};
    BOOL                    IsUnframe(void)                     {return m_pChipset->IsUnframe(m_nChipsetIdx);};
    CQcnFile               &GetSampleQcn(void);
    CQcnFile               &GetNVQcn(void);

private:
    void                    LoadIniConfig(CIniFile *pIniFile);
    void                    SaveIniConfig(CIniFile *pIniFile);

private:
    CString                 m_Empty;
    CIniFile               *m_pIniFile;
    CIniFile               *m_pCustIniFile;
    CChipset               *m_pChipset;
    CQcnFile                m_SampleQcn;
    CQcnFile                m_NVQcn;
    int                     m_nCOMNum;
    int                     m_pCOMList[COM_MAX];

    // Chipset Info
    int                     m_nChipsetIdx;
    
    // Backup Info
    CString                 m_szNVPath;
    CString                 m_szNVFile;
    CString                 m_szEFSPath;
    CStringArray            m_szMBNPath;    // MBN文件所在路径
    
    // Operation step
    BOOL                    m_bDloadMBN;
    BOOL                    m_bDloadFACT;
    BOOL                    m_bEraseFlash;
    BOOL                    m_bDloadPBL;
    BOOL                    m_bDloadQCSBL;
    BOOL                    m_bDloadOEMSBL;
    BOOL                    m_bDloadAMSS;
    BOOL                    m_bDloadAPPS;
    BOOL                    m_bDloadOBL;
    BOOL                    m_bDloadFOTAUI;
    BOOL                    m_bDloadCEFS;
    BOOL                    m_bDloadAPPSBL;
    BOOL                    m_bDloadAPPSCEFS;
    BOOL                    m_bDloadFLASHBIN;
    BOOL                    m_bDloadDSP1;
    BOOL                    m_bDloadCUSTOM;
    BOOL                    m_bDloadDBL;
    BOOL                    m_bDloadFSBL;
    BOOL                    m_bDloadOSBL;
    BOOL                    m_bDloadDSP2;
    BOOL                    m_bDloadRAW;
    BOOL                    m_bDloadMsimage;
    BOOL                    m_bDloadARD;
    BOOL                    m_bDloadARDBOOT;
    BOOL                    m_bDloadARDSYS;
    BOOL                    m_bDloadARDUSER;
    BOOL                    m_bDloadARDRCY;
    BOOL                    m_bDloadARDCLRCACHE;
    BOOL                    m_bNVBackup;
    BOOL                    m_bNVRestore;
    BOOL                    m_bEFSBackup;
    BOOL                    m_bEFSRestore;
    BOOL                    m_bAutoDLoad;
    BOOL                    m_bLogEnable;
    BOOL                    m_bLocked;

    // Data Read Config
    BOOL                    m_bMemDump;
    dword                   m_dwStartAddr;
    dword                   m_dwReadSize;

    // 只允许存在一个COMManager实例
    static CDloadConfig    *m_pDloadConfig;
    static int              m_nRefCnt;
};
