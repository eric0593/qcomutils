/*
 * Copyright (C) 2008 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the 
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _FASTBOOT_H_
#define _FASTBOOT_H_
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <ctype.h>

#include "usb_fastboot.h"
#include "bootimg.h"
#include "zipfile.h"
#include "USBPorts.h"

#define FB_COMMAND_SZ       64
#define FB_RESPONSE_SZ      64
#define CMD_MAX_LEN         (MAX_PATH+100)
#define CMD_MAX_NUM         20

typedef struct Action Action;

struct Action 
{
    unsigned op;
    Action *next;

    char cmd[64];    
    void *data;
    unsigned size;
    bool bfree;

    const char *msg;
    int (*func)(Action *a, int status, char *resp);

    double start;
};

class CFastboot
{
public:
	CFastboot(void);
    ~CFastboot(void);
    
    int     Exec(char *pExecStr, void *pThread);
    void    SetTotal(DWORD dwTotal);
    void    SetCurr(DWORD dwCurr);
    BOOL    IsRunning(void);
    void    SleepMS(UINT nMS);
    void    SetHubPortIndex(ULONG HubIndex, ULONG PortIndex)    {ParentHubIndex = HubIndex; ParentPortIndex = PortIndex;};
    void    EnableImageLimit(BOOL bLimit)   {bLimitSize = bLimit;};
    
private:
    /* protocol.c - fastboot protocol */
    int fb_command(usb_handle *usb, const char *cmd);
    int fb_command_response(usb_handle *usb, const char *cmd, char *response);
    int fb_download_data(usb_handle *usb, const void *data, unsigned size);
    char *fb_get_error(void);
    int check_response(usb_handle *usb, unsigned size, 
                          unsigned data_okay, char *response);
    int _command_send(usb_handle *usb, const char *cmd,
                         const void *data, unsigned size,
                         char *response);
    
    /* engine.c - high level command queue engine */
    void fb_queue_flash(const char *ptn, void *data, unsigned sz);;
    void fb_queue_erase(const char *ptn);
    //void fb_queue_require(const char *var, int invert, unsigned nvalues, const char **value);
    //void fb_queue_display(const char *var, const char *prettyname);
    void fb_queue_reboot(void);
    void fb_queue_command(const char *cmd, const char *msg);
    void fb_queue_download(const char *name, void *data, unsigned size);
    //void fb_queue_notice(const char *notice);
    void fb_execute_queue(usb_handle *usb);
    void fb_clean_queue(void);
    Action *queue_action(unsigned op, const char *fmt, ...);
    
    // USB
    usb_handle *usb_open(ifc_match_func callback);
    int usb_close(usb_handle *h);
    int usb_read(usb_handle *h, void *_data, int len);
    int usb_write(usb_handle *h, const void *_data, int len);
    int recognized_device(usb_handle* handle, ifc_match_func callback, TCHAR *devname);
    usb_handle* do_usb_open(const wchar_t* interface_name);
    void usb_cleanup_handle(usb_handle* handle);
    void usb_kick(usb_handle* handle);
    usb_handle *find_usb_device(ifc_match_func callback);
    void sleep(int seconds);
    
private:
    char *find_item(const char *item, const char *product);
    usb_handle *open_device(void);
    void *load_bootable_image(unsigned page_size, const char *kernel, const char *ramdisk,
                          unsigned *sz, const char *cmdline);
    void *unzip_file(zipfile_t zip, const char *name, unsigned *sz);
    char *strip(char *s);
    void do_update_signature(zipfile_t zip, char *fn);
    void do_update(char *fn);
    void do_send_signature(char *fn);
    void do_flashall(void);
    int do_oem_command(int argc, char **argv);
    int fastboot_main(int argc, char **argv);

private:
    //Callback
    static int match_fastboot(usb_ifc_info *info, void *puser, TCHAR *devname);

private:
    usb_handle *usb;
    const char *serial;
    const char *product;
    const char *cmdline;
    int wipe_data;
    unsigned short vendor_id;

    unsigned base_addr;
    void *g_pThread;
    usb_handle *open_usb;

    Action *action_list;
    Action *action_last;

    char g_ERROR[128];
    GUID usb_class_id;

    CUSBPorts myUSBPorts;
    LONG ParentHubIndex;
    LONG ParentPortIndex;
    BOOL bLimitSize;
};
#endif
