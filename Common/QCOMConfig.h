/*=================================================================================================

  FILE : QCOMConfig.H

  SERVICES: Define some user type and macro.

  GENERAL DESCRIPTION:
    
  INITIALIZATION AND SEQUENCING REQUIREMENTS:
  
====================================================================================================*/
#if !defined(_QCOMCONFIG_H__)
#define _QCOMCONFIG_H__
/*===========================================================================

                  INCLUDE FILES FOR MODULE
  
===========================================================================*/
#include "windows.h"

#endif  //_QCOMCONFIG_H__