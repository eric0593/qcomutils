// SNWriterDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "SNWriter.h"
#include "SNWriterDlg.h"
#include "Log.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MY_UPDATEINFO    (WM_USER+1)
#define WM_MY_UPDATESTATE   (WM_USER+2)

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CSNWriterDlg 对话框




CSNWriterDlg::CSNWriterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSNWriterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSNWriterDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_BUTTON_OK, m_cOKButton);
    DDX_Control(pDX, IDC_COMBO_COMLIST, m_cCOMCombo);
    DDX_Control(pDX, IDC_EDIT_IMEI1, m_cIMEI1Edit);
    DDX_Control(pDX, IDC_EDIT_IMEI2, m_cIMEI2Edit);
    DDX_Control(pDX, IDC_IMEI2_CHECK, m_IMEI2Check);
    DDX_Control(pDX, IDC_STATIC_IMEI1LABLE, m_cIMEI1Static);
    DDX_Control(pDX, IDC_STATIC_IMEI2LABLE, m_cIMEI2Static);
    DDX_Control(pDX, IDC_IMEI_15, m_IMEI15Check);
    DDX_Control(pDX, IDC_STATIC_STATUS, m_cStatus);
    DDX_Control(pDX, IDC_STATIC_IMEI1VAL, m_cIMEI1ValStatic);
    DDX_Control(pDX, IDC_STATIC_IMEI2VAL, m_cIMEI2ValStatic);
    DDX_Control(pDX, IDC_STATIC_IMEI1STATUS, m_cIMEI1StatusStatic);
    DDX_Control(pDX, IDC_STATIC_IMEI2STATUS, m_cIMEI2StatusStatic);
    DDX_Control(pDX, IDC_RANDOMSPC_CHECK, m_RandomSPCCheck);
}

BEGIN_MESSAGE_MAP(CSNWriterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_BN_CLICKED(IDC_BUTTON_OK, &CSNWriterDlg::OnBnClickedButtonOk)
    ON_WM_DESTROY()
    ON_WM_CTLCOLOR()
    ON_WM_TIMER()
    ON_MESSAGE(WM_MY_UPDATESTATE, OnMyUpdateState) 
    ON_CBN_DROPDOWN(IDC_COMBO_COMLIST, &CSNWriterDlg::OnCbnDropdownComboComlist)
    ON_CBN_CLOSEUP(IDC_COMBO_COMLIST, &CSNWriterDlg::OnCbnCloseupComboComlist)
    ON_BN_CLICKED(IDC_IMEI2_CHECK, &CSNWriterDlg::OnBnClickedImei2Check)
    ON_BN_CLICKED(IDC_RADIO_IMEI, &CSNWriterDlg::OnBnClickedRadioImei)
    ON_BN_CLICKED(IDC_RADIO_MEID, &CSNWriterDlg::OnBnClickedRadioMeid)
    ON_BN_CLICKED(IDC_RADIO_ESN, &CSNWriterDlg::OnBnClickedRadioEsn)
    ON_EN_CHANGE(IDC_EDIT_IMEI1, &CSNWriterDlg::OnEnChangeEditImei1)
    ON_EN_CHANGE(IDC_EDIT_IMEI2, &CSNWriterDlg::OnEnChangeEditImei2)
//    ON_CBN_KILLFOCUS(IDC_COMBO_COMLIST, &CSNWriterDlg::OnCbnKillfocusComboComlist)
ON_BN_CLICKED(IDC_IMEI_15, &CSNWriterDlg::OnBnClickedImei15)
ON_BN_CLICKED(IDC_BUTTON_CLEAR, &CSNWriterDlg::OnBnClickedButtonClear)
ON_WM_RBUTTONDOWN()
ON_BN_CLICKED(IDC_RANDOMSPC_CHECK, &CSNWriterDlg::OnBnClickedRandomspcCheck)
ON_WM_CHAR()
ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CSNWriterDlg 消息处理程序

BOOL CSNWriterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
    for(int i=0; i<DIAG_PHONEMODE_MAX ;i++)
    {
        CString szTemp;
        switch(i){
        case DIAG_PHONEMODE_CDMA:
        case DIAG_PHONEMODE_WCDMA:
            szTemp.LoadString(IDS_COM_CONNECT);
            break;
        case DIAG_PHONEMODE_DLOAD:
        case DIAG_PHONEMODE_EDLOAD:
        case DIAG_PHONEMODE_ARMPRG:
            szTemp.LoadString(IDS_COM_DLOAD);
            break;
        case DIAG_PHONEMODE_NOPORT:
        case DIAG_PHONEMODE_NONE:
        default:
            szTemp.LoadString(IDS_COM_NOPHONE);
            break;
        }
        m_arMode.InsertAt(i,szTemp);
    }

    m_DefCOMID = GetPrivateProfileInt(DIAG_INI_APP_NAME,DIAG_INI_COM_CURR_KEY,DIAG_INI_COM_CURR_DEF_INT,DIAG_INI_FILE);
    if(m_DefCOMID == DIAG_INI_COM_CURR_DEF_INT)
    {
        WritePrivateProfileString(DIAG_INI_APP_NAME,DIAG_INI_COM_CURR_KEY,DIAG_INI_COM_CURR_DEF_STR,DIAG_INI_FILE);
    }
    m_DefCOMID--;

    m_nPhoneStatus = IDS_COM_NOPHONE;
    
    m_bIMEI2Enable = GetPrivateProfileInt(SN_INI_APP_NAME,SN_INI_IMEI2_KEY,SN_INI_IMEI2_DEF_INT,DIAG_INI_FILE);
    if(m_bIMEI2Enable == SN_INI_IMEI2_DEF_INT)
    {
        WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_IMEI2_KEY,SN_INI_IMEI2_DEF_STR,DIAG_INI_FILE);
    }

    m_bIMEI15Enable = GetPrivateProfileInt(SN_INI_APP_NAME,SN_INI_IMEI15_KEY,SN_INI_IMEI15_DEF_INT,DIAG_INI_FILE);
    if(m_bIMEI15Enable == SN_INI_IMEI15_DEF_INT)
    {
        WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_IMEI15_KEY,SN_INI_IMEI15_DEF_STR,DIAG_INI_FILE);
    }
    m_nIMEIMode = GetPrivateProfileInt(SN_INI_APP_NAME,SN_INI_MODE_KEY,SN_INI_MODE_DEF_INT,DIAG_INI_FILE);
    if(m_nIMEIMode == SN_INI_MODE_DEF_INT)
    {
        WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_MODE_KEY,SN_INI_MODE_DEF_STR,DIAG_INI_FILE);
    }
    m_bRandomSPCEnable = GetPrivateProfileInt(SN_INI_APP_NAME,SN_INI_RANDOMSPC_KEY,SN_INI_RANDOMSPC_DEF_INT,DIAG_INI_FILE);
    if(m_bRandomSPCEnable == SN_INI_RANDOMSPC_DEF_INT)
    {
        WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_RANDOMSPC_KEY,SN_INI_RANDOMSPC_DEF_STR,DIAG_INI_FILE);
    }

    CString szTemp;

    m_IMEI2Check.SetCheck(m_bIMEI2Enable);
    m_IMEI15Check.SetCheck(m_bIMEI15Enable);
    m_RandomSPCCheck.SetCheck(m_bRandomSPCEnable);
    m_cIMEI2Edit.EnableWindow(m_bIMEI2Enable);
    m_cIMEI2Edit.SetLimitText(CHEXEDIT_LIM_IMEI+m_bIMEI15Enable);
    szTemp.Format(_T("IMEI2(%d/%d):"),m_cIMEI2Edit.GetWindowTextLength(),m_cIMEI2Edit.GetLimitText());
    m_cIMEI2Static.SetWindowText(szTemp);
    m_cIMEI1Edit.SetInputMode(m_nIMEIMode);
    m_cIMEI1Edit.SetIMEI15(m_bIMEI15Enable);
    m_cIMEI1Edit.SetFocus();

    switch(m_nIMEIMode){
    case CHEXEDIT_MODE_MEID:
        ((CButton *)GetDlgItem(IDC_RADIO_MEID))->SetCheck(TRUE);//选上
        szTemp.Format(_T("MEID(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        break;
    case CHEXEDIT_MODE_ESN:
        ((CButton *)GetDlgItem(IDC_RADIO_ESN))->SetCheck(TRUE);//选上
        szTemp.Format(_T("ESN(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        break;
    default:
    case CHEXEDIT_MODE_IMEI:
        ((CButton *)GetDlgItem(IDC_RADIO_IMEI))->SetCheck(TRUE);//选上
        szTemp.Format(_T("IMEI1(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        break;
    }
      
    m_BmpPass.LoadBitmap(IDB_PASS);
    m_BmpFail.LoadBitmap(IDB_FAIL); 
    m_cStatus.ModifyStyle(0x0F,SS_BITMAP|SS_CENTERIMAGE);

    COMManager::Create(&m_pCOMMgr);
    
    m_pCOMMgr->RefreshCOMList(m_cCOMCombo, m_arMode, m_DefCOMID);
    m_nCOMPortIdx = m_cCOMCombo.GetCurSel();
    m_pCOMMgr->RegStateChangeCB(m_DefCOMID, PhoneStateCB, this);

    SetTimer(SETFOCUS_TIMER_ID,SETFOCUS_TIME,NULL);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CSNWriterDlg::OnDestroy()
{
    CDialog::OnDestroy();

    CString szTemp;
    szTemp.Format(_T("%d"),m_IMEI2Check.GetCheck());
    WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_IMEI2_KEY,szTemp,DIAG_INI_FILE);
    
    szTemp.Format(_T("%d"),m_IMEI15Check.GetCheck());
    WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_IMEI15_KEY,szTemp,DIAG_INI_FILE);
    
    szTemp.Format(_T("%d"),m_RandomSPCCheck.GetCheck());
    WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_RANDOMSPC_KEY,szTemp,DIAG_INI_FILE);

    szTemp.Format(_T("%d"),m_nIMEIMode);
    WritePrivateProfileString(SN_INI_APP_NAME,SN_INI_MODE_KEY,szTemp,DIAG_INI_FILE);

    szTemp.Format(_T("%d"),m_DefCOMID+1);
    WritePrivateProfileString(DIAG_INI_APP_NAME,DIAG_INI_COM_CURR_KEY,szTemp,DIAG_INI_FILE);

    COMManager::Release();
}

void CSNWriterDlg::OnCancel()
{
    CDialog::OnCancel();
}

void CSNWriterDlg::OnOK()
{
    HWND   hWnd=::GetFocus();   
    int   iID=::GetDlgCtrlID(hWnd);
    
    if(IDC_COMBO_COMLIST == iID)
    {
        OnCbnCloseupComboComlist();
    }
    else if(m_bIMEI2Enable)
    {
        if(iID == IDC_EDIT_IMEI1)
        {
            m_cIMEI2Edit.SetFocus();
        }
        else if(iID == IDC_EDIT_IMEI2)
        {
            m_bWriteFromOKBtn = FALSE;
            WriteSN();
        }
    }
    else
    {
        m_bWriteFromOKBtn = FALSE;
        WriteSN();
    }

    m_cIMEI1Edit.SetSel(0,m_cIMEI1Edit.GetWindowTextLength());
    m_cIMEI2Edit.SetSel(0,m_cIMEI2Edit.GetWindowTextLength());
}

void CSNWriterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSNWriterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CSNWriterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CSNWriterDlg::WriteSN(void)
{
    BOOL    bRet = TRUE;
    CString szStatus;

    if(m_nPhoneStatus != IDS_COM_CONNECT)
    {
        szStatus.LoadString(IDS_ERROR_NOPHONE);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }

    if(m_pCOMMgr->SetUsed(m_DefCOMID, TRUE, this))
    {
        if(FALSE == m_PhoneDiag.StartDiag(m_DefCOMID))
        {
            m_pCOMMgr->SetUsed(m_DefCOMID, FALSE, this);
        }
    }
    
    if(FALSE == m_PhoneDiag.EnterSPCMode())
    {
        szStatus.LoadString(IDS_SPC_FAILED);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
        if(m_PhoneDiag.IsDiagStarted())
        {
            m_PhoneDiag.StopDiag();
            m_pCOMMgr->SetUsed(m_DefCOMID, FALSE, this);
        }
        return FALSE;
    }
    
    switch(m_nIMEIMode){
    case CHEXEDIT_MODE_MEID:
        WriteMEID();
        break;
    case CHEXEDIT_MODE_ESN:
        WriteESN();
        break;
    default:
    case CHEXEDIT_MODE_IMEI:
        bRet = WriteIMEI();
        break;
    }
    
    if(bRet)
    {
        m_cIMEI1Edit.SetFocus();
    }
    ReadSN();
    if(m_PhoneDiag.IsDiagStarted())
    {
        m_PhoneDiag.StopDiag();
        m_pCOMMgr->SetUsed(m_DefCOMID, FALSE, this);
    }
    return bRet;
}

void CSNWriterDlg::ReadSN(void)
{
    if(m_nPhoneStatus != IDS_COM_CONNECT)
    {
        return;
    }
    
    if(m_pCOMMgr->SetUsed(m_DefCOMID, TRUE, this))
    {
        if(FALSE == m_PhoneDiag.StartDiag(m_DefCOMID))
        {
            m_pCOMMgr->SetUsed(m_DefCOMID, FALSE, this);
        }
    }
    CString szSN;
    switch(m_nIMEIMode){
    case CHEXEDIT_MODE_MEID:
        szSN = ReadMEID();
        if(m_bIMEI2Enable)
        {
            CString szIMEI2;
            szIMEI2 = ReadIMEI2();
            m_cIMEI2ValStatic.SetWindowText(szIMEI2);
        }
        break;
    case CHEXEDIT_MODE_ESN:
        szSN = ReadESN();
        if(m_bIMEI2Enable)
        {
            CString szIMEI2;
            szIMEI2 = ReadIMEI2();
            m_cIMEI2ValStatic.SetWindowText(szIMEI2);
        }
        break;
    default:
    case CHEXEDIT_MODE_IMEI:
        szSN = ReadIMEI();
        break;
    }
    m_cIMEI1ValStatic.SetWindowText(szSN);
    if(m_PhoneDiag.IsDiagStarted())
    {
        m_PhoneDiag.StopDiag();
        m_pCOMMgr->SetUsed(m_DefCOMID, FALSE, this);
    }
}

void CSNWriterDlg::OnBnClickedButtonOk()
{
    m_bWriteFromOKBtn = TRUE;
    
    m_cIMEI1Edit.GetWindowText(m_szInput1);
    m_cIMEI2Edit.GetWindowText(m_szInput2);

    if(TRUE == WriteSN())
    {
        // Write the next number
        if(m_nIMEIMode == CHEXEDIT_MODE_IMEI)
        {
            if(!m_szInput1.IsEmpty())
            {
                int nUpper = 1;
                for(int i=m_szInput1.GetLength()-1;i>=0;i--)
                {
                    TCHAR ch = m_szInput1.GetAt(i);
                    if(nUpper == 0)
                    {
                        break;
                    }
                    
                    if(ch == '9')
                    {
                        m_szInput1.SetAt(i, '0');
                        nUpper = 1;
                    }
                    else
                    {
                        ch++;
                        m_szInput1.SetAt(i, ch);
                        nUpper = 0;
                    }
                }
                m_cIMEI1Edit.SetWindowText(m_szInput1);
            }

            if(!m_szInput2.IsEmpty())
            {
                int nUpper = 1;
                for(int i=m_szInput2.GetLength()-1;i>=0;i--)
                {
                    TCHAR ch = m_szInput2.GetAt(i);
                    if(nUpper == 0)
                    {
                        break;
                    }
                    
                    if(ch == '9')
                    {
                        m_szInput2.SetAt(i, '0');
                        nUpper = 1;
                    }
                    else
                    {
                        ch++;
                        m_szInput2.SetAt(i, ch);
                        nUpper = 0;
                    }
                }
                m_cIMEI2Edit.SetWindowText(m_szInput2);
            }
        }
        else
        {
            if(!m_szInput1.IsEmpty())
            {
                int nUpper = 1;
                for(int i=m_szInput1.GetLength()-1;i>=0;i--)
                {
                    TCHAR ch = m_szInput1.GetAt(i);
                    if(nUpper == 0)
                    {
                        break;
                    }
                    
                    if(ch == 'F' || ch == 'f')
                    {
                        m_szInput1.SetAt(i, '0');
                        nUpper = 1;
                    }
                    else
                    {
                        if(ch == '9')
                        {
                            ch = 'A';
                        }
                        else
                        {
                            ch++;
                        }
                        m_szInput1.SetAt(i, ch);
                        nUpper = 0;
                    }
                }
                m_cIMEI1Edit.SetWindowText(m_szInput1);
            }

            if(!m_szInput2.IsEmpty())
            {
                int nUpper = 1;
                for(int i=m_szInput2.GetLength()-1;i>=0;i--)
                {
                    TCHAR ch = m_szInput1.GetAt(i);
                    if(nUpper == 0)
                    {
                        break;
                    }
                    
                    if(ch == 'F' || ch == 'f')
                    {
                        m_szInput1.SetAt(i, '0');
                        nUpper = 1;
                    }
                    else
                    {
                        if(ch == '9')
                        {
                            ch = 'A';
                        }
                        else
                        {
                            ch++;
                        }
                        m_szInput2.SetAt(i, ch);
                        nUpper = 0;
                    }
                }
                m_cIMEI2Edit.SetWindowText(m_szInput2);
            }
        }

        m_cIMEI1Edit.SetSel(0,m_cIMEI1Edit.GetWindowTextLength());
        m_cIMEI2Edit.SetSel(0,m_cIMEI2Edit.GetWindowTextLength());
    }
}

void CSNWriterDlg::PhoneStateChange(int nPortID, int nNewState)
{
    int nNewPhoneStatus = IDS_COM_NOPHONE;
    int nPort   = 0;

    nPort  = m_pCOMMgr->GetCOMPortID(m_cCOMCombo, m_nCOMPortIdx);
    LOG_OUTPUT(_T("CSNWriterDlg::PhoneStateChange %d PORT %d-%d\n"),nNewState,nPortID,nPort);
    if(nPort != nPortID)
    {
        return;
    }

    if(nNewState != DIAG_PHONEMODE_NONE)
    {
        nNewPhoneStatus = IDS_COM_CONNECT;
        if(nNewState == DIAG_PHONEMODE_DLOAD || nNewState == DIAG_PHONEMODE_EDLOAD ||nNewState == DIAG_PHONEMODE_ARMPRG)
        {
            nNewPhoneStatus = IDS_COM_DLOAD;
        }
    }
    
    if(nNewPhoneStatus != m_nPhoneStatus || nNewPhoneStatus == IDS_COM_NOPHONE)
    {
        m_nPhoneStatus = nNewPhoneStatus;
        if(m_nPhoneStatus == IDS_COM_NOPHONE)
        {
            PhoneDisConnected();
        }
        else
        {
            NewPhoneConnected();
        }
    }

    m_pCOMMgr->RefreshCOMSel(m_cCOMCombo, m_nCOMPortIdx, m_arMode);
}

void CSNWriterDlg::OnTimer(UINT_PTR nIDEvent)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    if(SETFOCUS_TIMER_ID == nIDEvent)
    {
        m_cIMEI1Edit.SetFocus();
        KillTimer(SETFOCUS_TIMER_ID);
    }
    
    CDialog::OnTimer(nIDEvent);
}

void CSNWriterDlg::OnCbnDropdownComboComlist()
{
    m_pCOMMgr->RefreshCOMList(m_cCOMCombo, m_arMode, m_DefCOMID);
}

void CSNWriterDlg::OnCbnCloseupComboComlist()
{
    if(m_nCOMPortIdx != m_cCOMCombo.GetCurSel())
    {
        m_nPhoneStatus = IDS_COM_NOPHONE;
        m_nCOMPortIdx = m_cCOMCombo.GetCurSel();

        m_pCOMMgr->UnregStateChangeCB(m_DefCOMID, PhoneStateCB, this);
        m_DefCOMID = m_pCOMMgr->GetCOMPortID(m_cCOMCombo, m_nCOMPortIdx);
        m_pCOMMgr->RegStateChangeCB(m_DefCOMID, PhoneStateCB, this);
    }
    m_cIMEI1Edit.SetFocus();
}

void CSNWriterDlg::OnBnClickedImei2Check()
{
    m_bIMEI2Enable = m_IMEI2Check.GetCheck();
    m_cIMEI2Edit.EnableWindow(m_bIMEI2Enable);
    m_cIMEI1Edit.SetFocus();
}

void CSNWriterDlg::OnBnClickedRadioImei()
{
    if(m_nIMEIMode != CHEXEDIT_MODE_IMEI)
    {
        m_nIMEIMode = CHEXEDIT_MODE_IMEI;
        m_cIMEI1Edit.SetInputMode(m_nIMEIMode);
        m_cIMEI2Edit.EnableWindow(m_bIMEI2Enable);

        CString szTemp;
        szTemp.Format(_T("IMEI1(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        szTemp.Format(_T("IMEI2(%d/%d):"),m_cIMEI2Edit.GetWindowTextLength(),m_cIMEI2Edit.GetLimitText());
        m_cIMEI2Static.SetWindowText(szTemp);
        if(m_nPhoneStatus == IDS_COM_CONNECT)
        {
            ReadSN();
        }
    }
    m_cIMEI1Edit.SetFocus();
}

void CSNWriterDlg::OnBnClickedRadioMeid()
{
    if(m_nIMEIMode != CHEXEDIT_MODE_MEID)
    {
        m_nIMEIMode = CHEXEDIT_MODE_MEID;
        m_cIMEI1Edit.SetInputMode(m_nIMEIMode);
        m_cIMEI2Edit.EnableWindow(m_bIMEI2Enable);

        CString szTemp;
        szTemp.Format(_T("MEID(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        if(m_nPhoneStatus == IDS_COM_CONNECT)
        {
            ReadSN();
        }
    }
    m_cIMEI1Edit.SetFocus();
}

void CSNWriterDlg::OnBnClickedRadioEsn()
{
    if(m_nIMEIMode != CHEXEDIT_MODE_ESN)
    {
        m_nIMEIMode = CHEXEDIT_MODE_ESN;
        m_cIMEI1Edit.SetInputMode(m_nIMEIMode);
        m_cIMEI2Edit.EnableWindow(m_bIMEI2Enable);

        CString szTemp;
        szTemp.Format(_T("ESN(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        if(m_nPhoneStatus == IDS_COM_CONNECT)
        {
            ReadSN();
        }
    }
    m_cIMEI1Edit.SetFocus();
}

BOOL CSNWriterDlg::IsHexAvaliable(CString &szCode)
{
	if(szCode.IsEmpty())
	{
		return FALSE;
    }
	
	for(int i=0; i<szCode.GetLength(); i++)
	{
		TCHAR c = szCode.GetAt(i);
		if((c>_T('9') || c<_T('0'))&&(c>_T('F') || c<_T('A')))
		{
			return FALSE;
		}
	}
	return TRUE;
}

BOOL CSNWriterDlg::IsNumAvaliable(CString &szCode)
{
	if(szCode.IsEmpty())
	{
		return FALSE;
	}
	
	for(int i=0; i<szCode.GetLength(); i++)
	{
		TCHAR c = szCode.GetAt(i);
		if((c>_T('9') || c<_T('0')))
		{
			return FALSE;
		}
	}
	return TRUE;
}

byte CSNWriterDlg::CharToHex(TCHAR ch)
{
    byte Hex = 0;

    if(ch >= '0' && ch <= '9')
    {
        Hex = ch - '0';
    }
    else if(ch >= 'A' && ch <= 'F')
    {
        Hex = 0xA+(ch - 'A');
    }
    else if(ch >= 'a' && ch <= 'f')
    {
        Hex = 0xA + (ch - 'a');
    }
    return Hex;
}

TCHAR CSNWriterDlg::HexToChar(byte Hex)
{
    TCHAR ch = '0';

    if(Hex >= 0 && Hex <= 9)
    {
        ch = Hex + '0';
    }
    else if(Hex >= 0xA && Hex <= 0xF)
    {
        ch = (Hex-0xA) + 'A';
    }
    return ch;
}

BOOL CSNWriterDlg::IsIMEIAvaliable(CString &szCode)
{
	if(!IsNumAvaliable(szCode))
	{
		return FALSE;
	}
	
    if(szCode.GetLength() != (CHEXEDIT_LIM_IMEI+m_bIMEI15Enable))
    {
        return FALSE;
    }
	return TRUE;
}

BOOL CSNWriterDlg::WriteIMEI()
{
    BOOL bRet = WriteIMEI1();

    if(bRet && m_bIMEI2Enable)
    {
        bRet = WriteIMEI2();
        if(!bRet)
        {
            m_cIMEI2Edit.SetFocus();
        }
    }
    else
    {
        m_cIMEI1Edit.SetFocus();
    }

    if(bRet)
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpPass);
    }
    else
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
    }
	return bRet;
}

BOOL CSNWriterDlg::WriteIMEI1()
{
    CString         szStatus;
    CString         szIMEI;
    nv_ue_imei_type IMEI;

    m_cIMEI1Edit.GetWindowText(szIMEI);

    if(!IsIMEIAvaliable(szIMEI))
    {
        szStatus.LoadString(IDS_ERROR_FORMAT);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }
    
    FormatIMEI(szIMEI, &IMEI);
    if(FALSE == m_PhoneDiag.SendWriteNVItem(NV_UE_IMEI_I,&IMEI,sizeof(IMEI)))
    {
        szStatus.LoadString(IDS_ERROR_WRITE);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }

    m_szIMEI1 = szIMEI;
    RecordHistory(szIMEI);
    szStatus.LoadString(IDS_SUCCESS);
    m_cIMEI1StatusStatic.SetWindowText(szStatus);
	return TRUE;
}

BOOL CSNWriterDlg::WriteIMEI2()
{
    CString         szStatus;
    CString         szIMEI;
    nv_ue_imei_type IMEI;
    
    m_cIMEI2Edit.GetWindowText(szIMEI);

    if(m_szIMEI1 == szIMEI)
    {
        m_cIMEI2Edit.SetWindowText(_T(""));
        szStatus.LoadString(IDS_SAMEAS_IMEI1);
        m_cIMEI2StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }

    if(!IsIMEIAvaliable(szIMEI))
    {
        szStatus.LoadString(IDS_ERROR_FORMAT);
        m_cIMEI2StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }
    
    FormatIMEI(szIMEI, &IMEI);
    if(FALSE == m_PhoneDiag.SendWriteNVItemExt(NV_UE_IMEI_I,&IMEI,sizeof(IMEI),1))
    {
        szStatus.LoadString(IDS_ERROR_WRITE);
        m_cIMEI2StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }
    
    RecordHistory(szIMEI);
    szStatus.LoadString(IDS_SUCCESS);
    m_cIMEI2StatusStatic.SetWindowText(szStatus);
	return TRUE;
}

CString CSNWriterDlg::ReadIMEI(void)
{
    CString szIMEI1;
    szIMEI1 = ReadIMEI1();
    if(m_bIMEI2Enable)
    {
        CString szIMEI2;
        szIMEI2 = ReadIMEI2();
        m_cIMEI2ValStatic.SetWindowText(szIMEI2);
    }
    return szIMEI1;
}

CString CSNWriterDlg::ReadIMEI1()
{
    CString         szIMEI;
    nv_ue_imei_type IMEI;

    if(FALSE == m_PhoneDiag.SendReadNVItem(NV_UE_IMEI_I,&IMEI,sizeof(IMEI)))
    {
        return szIMEI;
    }

    StringIMEI(szIMEI, &IMEI);
    m_szIMEI1 = szIMEI;
    if(!m_bIMEI15Enable)
    {
        m_szIMEI1.Delete(14,1);
    }
	return szIMEI;
}

CString CSNWriterDlg::ReadIMEI2()
{
    CString         szIMEI;
    nv_ue_imei_type IMEI;

    if(FALSE == m_PhoneDiag.SendReadNVItemExt(NV_UE_IMEI_I,&IMEI,sizeof(IMEI),1))
    {
        return szIMEI;
    }
    
    StringIMEI(szIMEI, &IMEI);
	return szIMEI;
}

void CSNWriterDlg::FormatIMEI(CString &szCode, nv_ue_imei_type *pIMEI)
{
    byte SP;
    if(!m_bIMEI15Enable)
    {
        SP = CalcIMEISP(szCode);
    }
    else
    {
        SP = szCode.GetAt(14) - '0';
    }

    pIMEI->ue_imei[0] = 0x08;
    pIMEI->ue_imei[1] = 0x0A | ((szCode.GetAt(0) - '0')<<4); // IMEI固定第一位是0xA
    pIMEI->ue_imei[2] = ((szCode.GetAt(1) - '0')) | ((szCode.GetAt(2) - '0')<<4);
    pIMEI->ue_imei[3] = ((szCode.GetAt(3) - '0')) | ((szCode.GetAt(4) - '0')<<4);
    pIMEI->ue_imei[4] = ((szCode.GetAt(5) - '0')) | ((szCode.GetAt(6) - '0')<<4);
    pIMEI->ue_imei[5] = ((szCode.GetAt(7) - '0')) | ((szCode.GetAt(8) - '0')<<4);
    pIMEI->ue_imei[6] = ((szCode.GetAt(9) - '0')) | ((szCode.GetAt(10) - '0')<<4);
    pIMEI->ue_imei[7] = ((szCode.GetAt(11) - '0')) | ((szCode.GetAt(12) - '0')<<4);
    pIMEI->ue_imei[8] = ((szCode.GetAt(13) - '0')) | (SP<<4);

}

void CSNWriterDlg::StringIMEI(CString &szCode, nv_ue_imei_type *pIMEI)
{
    TCHAR ch;
    szCode.Empty();
    ch = ((pIMEI->ue_imei[1]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[2]&0x0F))+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[2]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[3]&0x0F))+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[3]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[4]&0x0F))+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[4]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[5]&0x0F))+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[5]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[6]&0x0F))+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[6]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[7]&0x0F))+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[7]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[8]&0x0F))+'0';
    szCode.AppendChar(ch);
    ch = ((pIMEI->ue_imei[8]&0xF0)>>4)+'0';
    szCode.AppendChar(ch);
}

void CSNWriterDlg::ShowMessageBox(int nResID)
{
    CString szTitle;
    CString szText;

    szTitle.LoadString(IDS_ERROR);
    szText.LoadString(nResID);

    MessageBox(szText, szTitle, MB_OK);
}

void CSNWriterDlg::NewPhoneConnected(void)
{
    m_szIMEI1.Empty();
    m_cStatus.SetBitmap(NULL);
    m_cIMEI1StatusStatic.SetWindowText(_T(""));
    m_cIMEI2StatusStatic.SetWindowText(_T(""));
    if(!m_bWriteFromOKBtn)
    {
        m_cIMEI1Edit.SetWindowText(_T(""));
        m_cIMEI2Edit.SetWindowText(_T(""));
    }
    ReadSN();
}

void CSNWriterDlg::PhoneDisConnected(void)
{
    m_cStatus.SetBitmap(NULL);
    m_cIMEI1StatusStatic.SetWindowText(_T(""));
    m_cIMEI2StatusStatic.SetWindowText(_T(""));
    if(!m_bWriteFromOKBtn)
    {
        m_cIMEI1Edit.SetWindowText(_T(""));
        m_cIMEI2Edit.SetWindowText(_T(""));
    }
}

byte CSNWriterDlg::CalcIMEISP(CString &szCode)
{
    // 计算校验位
    // IMEI校验码算法：
    // (1).将偶数位数字分别乘以2，分别计算个位数和十位数之和
    // (2).将奇数位数字相加，再加上上一步算得的值
    // (3).如果得出的数个位是0则校验位为0，否则为10减去个位数
    // 如：35 89 01 80 69 72 41 偶数位乘以2得到5*2=10 9*2=18 1*2=02 0*2=00 9*2=18 2*2=04 1*2=02,
    //    计算奇数位数字之和和偶数位个位十位之和，得到 3+(1+0)+8+(1+8)+0+(0+2)+8+(0+0)+6+(1+8)+7+(0+4)+4+(0+2)=63 
    // => 校验位 10-3 = 7
    byte SP;
    byte oH = 0, oL = 0;

    SP  = szCode.GetAt(1) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(3) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(5) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(7) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(9) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(11) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(13) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = 0;
    SP += szCode.GetAt(0) - '0';
    SP += szCode.GetAt(2) - '0';
    SP += szCode.GetAt(4) - '0';
    SP += szCode.GetAt(6) - '0';
    SP += szCode.GetAt(8) - '0';
    SP += szCode.GetAt(10) - '0';
    SP += szCode.GetAt(12) - '0';
    SP += oH + oL;

    SP = SP%10;
    if(SP > 0)
    {
        SP = 10 - SP;
    }
    return SP;
}

void CSNWriterDlg::RecordHistory(CString &szCurrCode)
{
    CStdioFile  myHistFile;
    CTime       myTime;
    CString     szFileName;
    byte SP;
    
    myTime = CTime::GetCurrentTime();
    switch(m_nIMEIMode){
    case CHEXEDIT_MODE_MEID:
        szFileName.Format(_T(".\\MEID_%04d_%02d_%02d.txt"),myTime.GetYear(),myTime.GetMonth(),myTime.GetDay());
        //SP = CalcIMEISP(szCurrCode);
        //szCurrCode.AppendChar(HexToChar(SP));
        break;
    case CHEXEDIT_MODE_ESN:
        szFileName.Format(_T(".\\ESN_%04d_%02d_%02d.txt"),myTime.GetYear(),myTime.GetMonth(),myTime.GetDay());
        break;
    default:
    case CHEXEDIT_MODE_IMEI:
        szFileName.Format(_T(".\\IMEI_%04d_%02d_%02d.txt"),myTime.GetYear(),myTime.GetMonth(),myTime.GetDay());
        if(!m_bIMEI15Enable)
        {
            SP = CalcIMEISP(szCurrCode);
            szCurrCode.AppendChar(SP+'0');
        }
        break;
    }

    if(m_bRandomSPCEnable)
    {
        CString m_szSPC;
        int nRandom;
        nv_sec_code_type spc;

        srand((int)time(NULL));
        nRandom = rand();
        m_szSPC.Format(_T("%06d"),nRandom);
        
        for(int i=0;i<NV_SEC_CODE_SIZE;i++)
        {
            spc.digits[i] = (byte)m_szSPC.GetAt(i);
        }

        if(FALSE == m_PhoneDiag.SendWriteNVItem(NV_SEC_CODE_I,&spc,sizeof(spc)))
        {
            szCurrCode = _T("000000");
        }

        szCurrCode += _T("\t");
        szCurrCode += m_szSPC;
    }
    szCurrCode += _T("\n");
    myHistFile.Open(szFileName,CStdioFile::modeReadWrite|CStdioFile::modeCreate|CStdioFile::modeNoTruncate);
    myHistFile.SeekToEnd();
    myHistFile.WriteString(szCurrCode);
    myHistFile.Close();
}

BOOL CSNWriterDlg::IsMEIDAvaliable(CString &szCode)
{
    if(!IsHexAvaliable(szCode))
	{
		return FALSE;
	}
	
    if(szCode.GetLength() != CHEXEDIT_LIM_MEID)
    {
        return FALSE;
    }
	return TRUE;
}

BOOL CSNWriterDlg::WriteMEID(void)
{
    CString         szStatus;
    CString         szMEID;
    byte            MEID[8];

    m_cIMEI1Edit.GetWindowText(szMEID);
    
    if(!IsMEIDAvaliable(szMEID))
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
        szStatus.LoadString(IDS_ERROR_FORMAT);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }

    memset(MEID,0,sizeof(MEID));
    FormatMEID(szMEID, MEID);
    if(FALSE == m_PhoneDiag.SendWriteNVItem(NV_MEID_I,MEID,sizeof(MEID)))
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
        szStatus.LoadString(IDS_ERROR_WRITE);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }
    
    RecordHistory(szMEID);
    szStatus.LoadString(IDS_SUCCESS);
    m_cIMEI1StatusStatic.SetWindowText(szStatus);

    BOOL bRet = TRUE;
    if(m_bIMEI2Enable)
    {
        bRet = WriteIMEI2();
        if(!bRet)
        {
            m_cIMEI2Edit.SetFocus();
        }
    }
    else
    {
        m_cIMEI1Edit.SetFocus();
    }

    if(bRet)
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpPass);
    }
    else
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
    }
    return TRUE;
}

CString CSNWriterDlg::ReadMEID(void)
{
    CString         szMEID;
    byte            MEID[8];

    memset(MEID,0,sizeof(MEID));
    if(FALSE == m_PhoneDiag.SendReadNVItem(NV_MEID_I,MEID,sizeof(MEID)))
    {
        return szMEID;
    }
    
    StringMEID(szMEID, MEID);
	return szMEID;
}

void CSNWriterDlg::FormatMEID(CString &szCode, byte *pMEID)
{
    byte SP = CalcMEIDSP(szCode);
    pMEID[0] = CharToHex(szCode.GetAt(13))|(CharToHex(szCode.GetAt(12))<<4);
    pMEID[1] = CharToHex(szCode.GetAt(11))|(CharToHex(szCode.GetAt(10))<<4);
    pMEID[2] = CharToHex(szCode.GetAt(9))|(CharToHex(szCode.GetAt(8))<<4);
    pMEID[3] = CharToHex(szCode.GetAt(7))|(CharToHex(szCode.GetAt(6))<<4);
    pMEID[4] = CharToHex(szCode.GetAt(5))|(CharToHex(szCode.GetAt(4))<<4);
    pMEID[5] = CharToHex(szCode.GetAt(3))|(CharToHex(szCode.GetAt(2))<<4);
    pMEID[6] = CharToHex(szCode.GetAt(1))|(CharToHex(szCode.GetAt(0))<<4);
    pMEID[7] = 0;
}

void CSNWriterDlg::StringMEID(CString &szCode, byte *pMEID)
{
    szCode.Empty();
    szCode.Insert(0,HexToChar(pMEID[0]&0x0F));
    szCode.Insert(0,HexToChar((pMEID[0]&0xF0)>>4));
    szCode.Insert(0,HexToChar(pMEID[1]&0x0F));
    szCode.Insert(0,HexToChar((pMEID[1]&0xF0)>>4));
    szCode.Insert(0,HexToChar(pMEID[2]&0x0F));
    szCode.Insert(0,HexToChar((pMEID[2]&0xF0)>>4));
    szCode.Insert(0,HexToChar(pMEID[3]&0x0F));
    szCode.Insert(0,HexToChar((pMEID[3]&0xF0)>>4));
    szCode.Insert(0,HexToChar(pMEID[4]&0x0F));
    szCode.Insert(0,HexToChar((pMEID[4]&0xF0)>>4));
    szCode.Insert(0,HexToChar(pMEID[5]&0x0F));
    szCode.Insert(0,HexToChar((pMEID[5]&0xF0)>>4));
    szCode.Insert(0,HexToChar(pMEID[6]&0x0F));
    szCode.Insert(0,HexToChar((pMEID[6]&0xF0)>>4));
    //szCode.Insert(0,HexToChar(pMEID[7]&0x0F));
    //szCode.Insert(0,HexToChar((pMEID[7]&0xF0)>>4));
}

byte CSNWriterDlg::CalcMEIDSP(CString &szCode)
{
    // 计算校验位
    // MEID校验码算法：
    // (1).将偶数位数字分别乘以2，分别计算个位数和十位数之和，注意是16进制数
    // (2).将奇数位数字相加，再加上上一步算得的值
    // (3).如果得出的数个位是0则校验位为0，否则为10(这里的10是16进制)减去个位数
    // 如：AF 01 23 45 0A BC DE 偶数位乘以2得到F*2=1E 1*2=02 3*2=06 5*2=0A A*2=14 C*2=1C E*2=1C,
    // 计算奇数位数字之和和偶数位个位十位之和，得到 A+(1+E)+0+2+2+6+4+A+0+(1+4)+B+(1+8)+D+(1+C)=64 
    // => 校验位 10-4 = C

    byte SP;
    byte oH = 0, oL = 0;

    SP  = CharToHex(szCode.GetAt(1));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(3));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(5));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(7));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(9));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(11));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(13));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = 0;
    SP += CharToHex(szCode.GetAt(0));
    SP += CharToHex(szCode.GetAt(2));
    SP += CharToHex(szCode.GetAt(4));
    SP += CharToHex(szCode.GetAt(6));
    SP += CharToHex(szCode.GetAt(8));
    SP += CharToHex(szCode.GetAt(10));
    SP += CharToHex(szCode.GetAt(12));
    SP += oH + oL;

    SP = SP%0x10;
    if(SP > 0)
    {
        SP = 0x10 - SP;
    }
    return SP;
}


BOOL CSNWriterDlg::IsESNAvaliable(CString &szCode)
{
    if(!IsHexAvaliable(szCode))
	{
		return FALSE;
	}
	
    if(szCode.GetLength() != CHEXEDIT_LIM_ESN)
    {
        return FALSE;
    }
	return TRUE;
}

BOOL CSNWriterDlg::WriteESN(void)
{
    CString         szStatus;
    CString         szESN;
    dword           ESN;

    m_cIMEI1Edit.GetWindowText(szESN);

    if(!IsESNAvaliable(szESN))
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
        szStatus.LoadString(IDS_ERROR_FORMAT);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }
    
    FormatESN(szESN, &ESN);
    if(FALSE == m_PhoneDiag.SendWriteNVItem(NV_ESN_I,&ESN,sizeof(ESN)))
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
        szStatus.LoadString(IDS_ERROR_WRITE);
        m_cIMEI1StatusStatic.SetWindowText(szStatus);
        return FALSE;
    }
    
    RecordHistory(szESN);
    szStatus.LoadString(IDS_SUCCESS);
    m_cIMEI1StatusStatic.SetWindowText(szStatus);

    BOOL bRet = TRUE;
    if(m_bIMEI2Enable)
    {
        bRet = WriteIMEI2();
        if(!bRet)
        {
            m_cIMEI2Edit.SetFocus();
        }
    }
    else
    {
        m_cIMEI1Edit.SetFocus();
    }

    if(bRet)
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpPass);
    }
    else
    {
        m_cStatus.SetBitmap((HBITMAP)m_BmpFail);
    }
    return TRUE;
}

CString CSNWriterDlg::ReadESN(void)
{
    CString         szESN;
    dword           ESN;

    if(FALSE == m_PhoneDiag.SendReadNVItem(NV_ESN_I,&ESN,sizeof(ESN)))
    {
        return szESN;
    }
    
    StringESN(szESN, &ESN);
	return szESN;
}

void CSNWriterDlg::FormatESN(CString &szCode, dword *pESN)
{
    *pESN = _tcstol(szCode,NULL,16);
}

void CSNWriterDlg::StringESN(CString &szCode, dword *pESN)
{
    szCode.Format(_T("%08X"),*pESN);
}

void CSNWriterDlg::OnEnChangeEditImei1()
{
    CString szTemp;

    switch(m_nIMEIMode){
    case CHEXEDIT_MODE_MEID:
        szTemp.Format(_T("MEID(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        break;
    case CHEXEDIT_MODE_ESN:
        szTemp.Format(_T("ESN(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        break;
    default:
    case CHEXEDIT_MODE_IMEI:
        szTemp.Format(_T("IMEI1(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
        break;
    }
}

void CSNWriterDlg::OnEnChangeEditImei2()
{
    CString szTemp;
    szTemp.Format(_T("IMEI2(%d/%d):"),m_cIMEI2Edit.GetWindowTextLength(),m_cIMEI2Edit.GetLimitText());
    m_cIMEI2Static.SetWindowText(szTemp);
}

void CSNWriterDlg::OnBnClickedImei15()
{
    m_bIMEI15Enable = m_IMEI15Check.GetCheck();
    if(m_nIMEIMode == CHEXEDIT_MODE_IMEI)
    {
        m_cIMEI1Edit.SetIMEI15(m_bIMEI15Enable);
    }
    m_cIMEI2Edit.SetLimitText(CHEXEDIT_LIM_IMEI+m_bIMEI15Enable);
    m_cIMEI2Edit.SetWindowText(_T(""));
    
    CString szTemp;
    if(m_nIMEIMode == CHEXEDIT_MODE_IMEI)
    {
        szTemp.Format(_T("IMEI1(%d/%d):"),m_cIMEI1Edit.GetWindowTextLength(),m_cIMEI1Edit.GetLimitText());
        m_cIMEI1Static.SetWindowText(szTemp);
    }
    szTemp.Format(_T("IMEI2(%d/%d):"),m_cIMEI2Edit.GetWindowTextLength(),m_cIMEI2Edit.GetLimitText());
    m_cIMEI2Static.SetWindowText(szTemp);

    m_cIMEI1Edit.SetFocus();
}

void CSNWriterDlg::PhoneStateCB(LPVOID pParam, int nPortID, int nNewState)
{
    CSNWriterDlg *pThis = (CSNWriterDlg *)pParam;
    LOG_OUTPUT(_T("CSNWriterDlg::PhoneStateCB COM%d %d\n"),nPortID+1,nNewState);
    pThis->PostMessage(WM_MY_UPDATESTATE, (WPARAM)nPortID, (LPARAM)nNewState);
}

LRESULT CSNWriterDlg::OnMyUpdateState(WPARAM wParam, LPARAM lParam)
{
    PhoneStateChange((int)wParam,(int)lParam);
    return 0;
}
void CSNWriterDlg::OnBnClickedButtonClear()
{
    m_cIMEI1Edit.SetWindowText(_T(""));
    m_cIMEI2Edit.SetWindowText(_T(""));
    //m_cIMEI1Static.SetWindowText(_T(""));
    //m_cIMEI2Static.SetWindowText(_T(""));
    m_cIMEI1StatusStatic.SetWindowText(_T(""));
    m_cIMEI2StatusStatic.SetWindowText(_T(""));
    m_cStatus.SetBitmap(NULL);

    m_cIMEI1Edit.SetFocus();
}

void CSNWriterDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    OnBnClickedButtonClear();

    CDialog::OnRButtonDown(nFlags, point);
}

void CSNWriterDlg::OnBnClickedRandomspcCheck()
{
    m_bRandomSPCEnable = m_RandomSPCCheck.GetCheck();
    
    m_cIMEI1Edit.SetFocus();
}

