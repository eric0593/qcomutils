//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SNWriter.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IMEIWRITER_DIALOG           102
#define IDS_COM_NOPHONE                 102
#define IDS_COM_CONNECT                 103
#define IDS_COM_DLOAD                   104
#define IDS_SUCCESS                     105
#define IDS_FAILED                      106
#define IDS_OFFLINE_FAILED              107
#define IDS_SPC_FAILED                  108
#define IDS_ERROR                       109
#define IDS_IMEI_FORMAT                 110
#define IDS_ERROR_FORMAT                110
#define IDS_WRITE_ERROR                 111
#define IDS_ERROR_WRITE                 111
#define IDS_SAMEAS_IMEI1                112
#define IDS_ERROR_NOPHONE               113
#define IDR_MAINFRAME                   128
#define IDD_SNWRITER_DIALOG             129
#define IDB_PASS                        130
#define IDB_FAIL                        131
#define IDC_COMBO_COMLIST               1000
#define IDC_EDIT_IMEI1                  1001
#define IDC_EDIT_IMEI2                  1002
#define IDC_STATIC_COMSTATUS            1003
#define IDC_STATIC_COMLABLE             1004
#define IDC_STATIC_IMEI1LABLE           1005
#define IDC_STATIC_IMEI2LABLE           1006
#define IDC_STATIC_IMEI1STATUS          1007
#define IDC_STATIC_COMSTATUS3           1008
#define IDC_BUTTON_OK                   1009
#define IDC_BUTTON_CLEAR                1010
#define IDC_IMEI2_CHECK                 1011
#define IDC_RADIO_IMEI                  1012
#define IDC_RADIO_MEID                  1013
#define IDC_RADIO_ESN                   1014
#define IDC_IMEI_15                     1015
#define IDC_RANDOMSPC_CHECK             1016
#define IDC_BUTTON_CLEAR2               1017
#define IDC_BUTTON_NEXT                 1017
#define IDC_STATIC_STATUS               1018
#define IDC_STATIC_IMEI1VAL             1019
#define IDC_STATIC_IMEI2VAL             1022
#define IDC_STATIC_IMEI2STATUS          1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
