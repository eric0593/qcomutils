#pragma once

#define CHEXEDIT_MODE_IMEI  0
#define CHEXEDIT_MODE_MEID  1
#define CHEXEDIT_MODE_ESN   2

#define CHEXEDIT_LIM_IMEI   14
#define CHEXEDIT_LIM_MEID   14
#define CHEXEDIT_LIM_ESN    8

// CHexEdit

class CHexEdit : public CEdit
{
	DECLARE_DYNAMIC(CHexEdit)

private:
    int m_nMode;
    int m_bIMEI15Enable;

public:
	CHexEdit();
	virtual ~CHexEdit();
    void SetInputMode(int nMode)    {if(m_nMode != nMode) {m_nMode = nMode;OnModeChange();}};
    int  GetInputMode(void)         {return m_nMode;};
    void SetIMEI15(int bEnable)     {m_bIMEI15Enable = bEnable;OnModeChange();};

private:
    void OnModeChange(void);

protected:
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
};


