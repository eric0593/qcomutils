/*===========================================================================

             N V   I T E M S   M E T A - C O M M E N T S   F I L E
DESCRIPTION

  This file contains HTORPC meta-comments that describe the union type
  nv_item_type defined in nv_items.h

REFERENCES
Copyright (c)  2005-2008 by QUALCOMM, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/core/services/nv/remote_restricted/main/latest/inc/nv_items_meta.h#2 $ $DateTime: 2010/01/13 05:10:05 $ $Author: gsrikant $
   
when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/05/09    pc     Provided a pruned list of items supported for WM.
===========================================================================*/

/*===========================================================================
  
  W A R N I N G   |   W A R N I N G   |   W A R N I N G   |   W A R N I N G

  Nothing should appear in this header file before the following block of
  HTORPC meta-comments except for regular C comments and #define's.
  (ie. no #include's, no C declarations etc.)

  W A R N I N G   |   W A R N I N G   |   W A R N I N G   |   W A R N I N G

===========================================================================*/

/*~ PARTIAL nv_item_type */

/*~ CASE NV_A_KEY_I nv_item_type.a_key */
/*~ CASE NV_BD_ADDR_I nv_item_type.bd_addr */
/*~ CASE NV_BREW_CARRIER_ID_I nv_item_type.brew_carrier_id */
/*~ CASE NV_BREW_PRIVACY_POLICY_I nv_item_type.brew_privacy_policy */
/*~ CASE NV_DCVSAPPS_NV_I nv_item_type.dcvsapps_nv */
/*~ CASE NV_DIAG_DEFAULT_BAUDRATE_I nv_item_type.diag_default_baudrate */
/*~ CASE NV_DIAG_FTM_MODE_SWITCH_I nv_item_type.diag_ftm_mode_switch */
/*~ CASE NV_DIAG_SPC_TIMEOUT_I nv_item_type.diag_spc_timeout */
/*~ CASE NV_DIAG_SPC_UNLOCK_TTL_I nv_item_type.diag_spc_unlock_ttl */
/*~ CASE NV_DIR_NUMBER_I nv_item_type.dir_number */
/*~ CASE NV_DIR_NUMBER_PCS_I nv_item_type.mob_dir_number */
/*~ CASE NV_DISPLAY_UPDATE_POWER_TEST_MODE_I nv_item_type.display_update_power_test_mode */
/*~ CASE NV_ERR_FATAL_OPTIONS_I nv_item_type.err_fatal_options */
/*~ CASE NV_ESN_I nv_item_type.esn */
/*~ CASE NV_FTM_MODE_I nv_item_type.ftm_mode */
/*~ CASE NV_GPRS_ANITE_GCF_I nv_item_type.gprs_anite_gcf */
/*~ CASE NV_GSM_AMR_CALL_CONFIG_I nv_item_type.gsm_amr_call_config */
/*~ CASE NV_IMSI_11_12_I nv_item_type.imsi_11_12 */
/*~ CASE NV_IMSI_MCC_I nv_item_type.imsi_mcc */
/*~ CASE NV_MEID_I nv_item_type.meid */
/*~ CASE NV_MIN1_I nv_item_type.min1 */
/*~ CASE NV_MIN2_I nv_item_type.min2 */
/*~ CASE NV_MOB_MODEL_I nv_item_type.mob_model */
/*~ CASE NV_NAME_NAM_I nv_item_type.name_nam */
/*~ CASE NV_PREF_MODE_I nv_item_type.pref_mode */
/*~ CASE NV_QVP_APP_DEFAULT_CAPABILITY_TYPE_I nv_item_type.qvp_app_default_capability_type */
/*~ CASE NV_RTSP_PROXY_SERVER_ADDR_ALTERNATE_I nv_item_type.rtsp_proxy_server_addr_alternate */
/*~ CASE NV_RTSP_PROXY_SERVER_ADDR_I nv_item_type.rtsp_proxy_server_addr */
/*~ CASE NV_SW_VERSION_INFO_I nv_item_type.sw_version_info */
/*~ CASE NV_UE_IMEI_I nv_item_type.ue_imei */
/*~ CASE NV_MGRF_SUPPORTED_I nv_item_type.mgrf_supported */
/*~ CASE NV_IMSI_I nv_item_type.imsi */
/*~ CASE NV_TIME_TOD_OFFSET_I nv_item_type.time_tod_offset */
/*~ CASE NV_SECTIME_TIME_OFFSETS_I nv_item_type.sectime_time_offsets */
/*~ CASE NV_FTM_MODE_BOOT_COUNT_I nv_item_type.ftm_mode_boot_count */
/*~ CASE NV_DIAG_DIAGBUF_TX_SLEEP_TIME_NV_I nv_item_type.diag_diagbuf_tx_sleep_time_nv */
/*~ CASE NV_DIAG_DIAGBUF_TX_SLEEP_THRESHOLD_EXT_I nv_item_type.diag_diagbuf_tx_sleep_threshold_ext */
/*~ CASE NV_ENABLE_FASTTRACK_I nv_item_type.enable_fasttrack */
/*~ CASE NV_ECC_LIST_I nv_item_type.ecc_list */
/*~ CASE NV_VOICE_PRIV_I nv_item_type.voice_priv */
/*~ CASE NV_ENS_ENABLED_I nv_item_type.ens_enabled */
/*~ CASE NV_HOME_SID_NID_I nv_item_type.home_sid_nid */
/*~ CASE NV_RTRE_CONFIG_I nv_item_type.rtre_config */
/*~ CASE NV_DATA_CALL_OVER_EHRPD_ONLY_I nv_item_type.data_call_over_ehrpd_only */


