#pragma once
#include "afx.h"
#include "CIniFile.h"

#define CHIPSET_MULTIBOOT_NONE              0
#define CHIPSET_MULTIBOOT_1                 1
#define CHIPSET_MULTIBOOT_2                 2
#define CHIPSET_INFO_FILE                   _T(".\\ChipsetInfo.ini")
#define CHIPSET_INFO_KEYWORDS_KEY           _T("KEYWORDS")
#define CHIPSET_INFO_MULTIMBN_KEY           _T("MULTIMBN")
#define CHIPSET_INFO_SECMODE_KEY            _T("SECMODE")
#define CHIPSET_INFO_JUMPBOOT_KEY           _T("JUMPBOOT")
#define CHIPSET_INFO_JUMPADDR_KEY           _T("JUMPADDR")
#define CHIPSET_INFO_ARMPRGNAME_KEY         _T("ARMPRGNAME")
#define CHIPSET_INFO_PARTITION_KEY          _T("PARTITION")

#define CHIPSET_INFO_PBLNAME_KEY            _T("PBLNAME")
#define CHIPSET_INFO_QCSBLHDCFGNAME_KEY     _T("QCSBLHDCFGNAME")
#define CHIPSET_INFO_QCSBLNAME_KEY          _T("QCSBLNAME")
#define CHIPSET_INFO_OEMSBLHDNAME_KEY       _T("OEMSBLHDNAME")
#define CHIPSET_INFO_OEMSBLNAME_KEY         _T("OEMSBLNAME")
#define CHIPSET_INFO_AMSSHDNAME_KEY         _T("AMSSHDNAME")
#define CHIPSET_INFO_AMSSNAME_KEY           _T("AMSSNAME")
#define CHIPSET_INFO_APPSHDNAME_KEY         _T("APPSHDNAME")
#define CHIPSET_INFO_APPSNAME_KEY           _T("APPSNAME")
#define CHIPSET_INFO_OBLNAME_KEY            _T("OBLNAME")
#define CHIPSET_INFO_FOTAUINAME_KEY         _T("FOTAUINAME")
#define CHIPSET_INFO_CEFSNAME_KEY           _T("CEFSNAME")
#define CHIPSET_INFO_APPSBLHDNAME_KEY       _T("APPSBLHDNAME")
#define CHIPSET_INFO_APPSBLNAME_KEY         _T("APPSBLNAME")
#define CHIPSET_INFO_APPS_CEFSNAME_KEY      _T("APPS_CEFSNAME")
#define CHIPSET_INFO_FLASH_BINNAME_KEY      _T("FLASH_BINNAME")
#define CHIPSET_INFO_DSP1NAME_KEY           _T("DSP1NAME")
#define CHIPSET_INFO_CUSTOMNAME_KEY         _T("CUSTOMNAME")
#define CHIPSET_INFO_DBLNAME_KEY            _T("DBLNAME")
#define CHIPSET_INFO_FSBLNAME_KEY           _T("FSBLNAME")
#define CHIPSET_INFO_OSBLNAME_KEY           _T("OSBLNAME")
#define CHIPSET_INFO_DSP2NAME_KEY           _T("DSP2NAME")
#define CHIPSET_INFO_RAWNAME_KEY            _T("RAWNAME")
#define CHIPSET_INFO_MSIMAGENAME_KEY        _T("MSIMAGENAME")

#define CHIPSET_INFO_ARDBOOTNAME_KEY        _T("ARDBOOTNAME")
#define CHIPSET_INFO_ARDSYSNAME_KEY         _T("ARDSYSNAME")
#define CHIPSET_INFO_ARDUSERNAME_KEY        _T("ARDUSERNAME")
#define CHIPSET_INFO_ARDRCYNAME_KEY         _T("ARDRCYNAME")

#define CHIPSET_INFO_FACTNAME_KEY           _T("FACTNAME")
#define CHIPSET_INFO_NVSAMPLE_KEY           _T("NVSAMPLE")
#define CHIPSET_INFO_ISEMMC_KEY             _T("ISEMMC")
#define CHIPSET_INFO_ISUSBBOOT_KEY          _T("ISUSBBOOT")
#define CHIPSET_INFO_ISUNFRAME_KEY          _T("ISUNFRAME")
#define CHIPSET_INFO_SUBNUM_KEY             _T("SUBNUM")
#define CHIPSET_INFO_EMMCPROGRAM_KEY        _T("EMMCPROGRAM")
#define CHIPSET_INFO_EMMCPATCH_KEY          _T("EMMCPATCH")

typedef struct
{
    CString         m_szKeywords;

    // MBN download Mode
    BOOL            m_bIsUnframe;
    BOOL            m_bIsUSBBoot;
    BOOL            m_bISEMMC;
    BYTE            m_bSubNum;      // SIM卡个数
    BYTE            m_bMultiMBN;
    BYTE            m_bSecMode;
    BOOL            m_bJumpBoot;    // 下载非MultiMBN的时候是否跳过BOOT区域
    DWORD           m_bJumpAddr;    // 跳过BOOT区域之后的起始地址
    
    // MBN download info
    CString         m_szARMPRGName; // ARM PRG文件名字，不包括路径，完整路径为m_szMBNPath+m_szARMPRGName
    CString         m_szPartition;  // 分区表
    CString         m_szPBLName;
    CString         m_szQCSBLHDCFGName;
    CString         m_szQCSBLName;
    CString         m_szOEMSBLHDName;
    CString         m_szOEMSBLName;
    CString         m_szAMSSHDName;
    CString         m_szAMSSName;
    CString         m_szAPPSHDName;
    CString         m_szAPPSName;
    CString         m_szOBLName;
    CString         m_szFOTAUIName;
    CString         m_szCEFSName;
    CString         m_szAPPSBLHDName;
    CString         m_szAPPSBLName;
    CString         m_szAPPSCEFSName;
    CString         m_szFLASHBINName;
    CString         m_szDSP1Name;
    CString         m_szCUSTOMName;
    CString         m_szDBLName;
    CString         m_szFSBLName;
    CString         m_szOSBLName;
    CString         m_szDSP2Name;
    CString         m_szRAWName;
    CString         m_szARDBOOTName;
    CString         m_szARDSYSName;
    CString         m_szARDUSERName;
    CString         m_szARDRCYName;
    CString         m_szFactName;
    CString         m_szNVSample;
    CString         m_szEMMCProgram;
    CString         m_szMsimageName;
    CString         m_szEMMCPatch;
}ChipsetInfo;

class CChipset
{
private:
	CChipset(void);
    ~CChipset(void);

public:
    static int      Create(CChipset **ppChipset);
    static int      Release(void);

    void            RefreshChipsetList(CComboBox &cChipsetCombo, int nIndex);
    CString        &GetChipsetName(int nChipsetIdx);
    CString        &GetKeywords(int nChipsetIdx);
    BYTE            IsMultiMBN(int nChipsetIdx);
    BYTE            GetSecMode(int nChipsetIdx);
    BOOL            IsJumbBoot(int nChipsetIdx, DWORD *pJumpAddr);
    CString        &GetARMPRGName(int nChipsetIdx);
    CString        &GetPartitionName(int nChipsetIdx);
    CString        &GetPBLName(int nChipsetIdx);
    CString        &GetQCSBLHDCFGName(int nChipsetIdx);
    CString        &GetQCSBLName(int nChipsetIdx);
    CString        &GetOEMSBLHDName(int nChipsetIdx);
    CString        &GetOEMSBLName(int nChipsetIdx);
    CString        &GetAMSSHDName(int nChipsetIdx);
    CString        &GetAMSSName(int nChipsetIdx);
    CString        &GetAPPSHDName(int nChipsetIdx);
    CString        &GetAPPSName(int nChipsetIdx);
    CString        &GetOBLName(int nChipsetIdx);
    CString        &GetFOTAUIName(int nChipsetIdx);
    CString        &GetCEFSName(int nChipsetIdx);
    CString        &GetAPPSBLHDName(int nChipsetIdx);
    CString        &GetAPPSBLName(int nChipsetIdx);
    CString        &GetAPPSCEFSName(int nChipsetIdx);
    CString        &GetFLASHBINName(int nChipsetIdx);
    CString        &GetDSP1Name(int nChipsetIdx);
    CString        &GetCUSTOMName(int nChipsetIdx);
    CString        &GetDBLName(int nChipsetIdx);
    CString        &GetFSBLName(int nChipsetIdx);
    CString        &GetOSBLName(int nChipsetIdx);
    CString        &GetDSP2Name(int nChipsetIdx);
    CString        &GetRAWName(int nChipsetIdx);
    CString        &GetARDBOOTName(int nChipsetIdx);
    CString        &GetARDSYSName(int nChipsetIdx);
    CString        &GetARDUSERName(int nChipsetIdx);
    CString        &GetARDRCYName(int nChipsetIdx);
    CString        &GetFactName(int nChipsetIdx);
    CString        &GetNVSample(int nChipsetIdx);
    CString        &GetEMMCProgram(int nChipsetIdx);
    CString        &GetEMMCPatch(int nChipsetIdx);
    CString        &GetMsimageName(int nChipsetIdx);
    BYTE            GetSubNum(int nChipsetIdx);
    BOOL            IsEMMC(int nChipsetIdx);
    BOOL            IsUSBBoot(int nChipsetIdx);
    BOOL            IsUnframe(int nChipsetIdx);

private:
    CIniFile       *m_pIniFile;
    ChipsetInfo    *m_pChipsetInfo;
    CStringArray    m_ChipsetNameArr;
    CString         m_szEmpty;

    // 只允许存在一个COMManager实例
    static CChipset*m_pChipset;
    static int      m_nRefCnt;
};