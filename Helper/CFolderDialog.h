//------------------------------------------------------
// ͷ�ļ�
#pragma once

class CFolderDialog {
public:
    CFolderDialog( 
        int             rootDirFlag = CSIDL_DESKTOP, 
        const CString&  focusDir = _T( "" ),
        const CString&  title = _T( "" ),
        DWORD           browseInfoFlag = 0
        );

    INT_PTR DoModal();
    const CString& GetPath() const { return m_szFinalPath; }

private:
    void OnCallback( HWND hWnd, UINT uMsg, LPARAM lParam );
    void SetFocusDirectory();

private:
    const int           m_nRootDirFlag;
    const CString       m_szFocusDir;
    const CString       m_szTitle;
    const DWORD         m_dwBrowseInfoFlag;

    HWND                m_hDialog;
    CString             m_szFinalPath;

    friend int CALLBACK BrowseDirectoryCallback( HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData );
};

