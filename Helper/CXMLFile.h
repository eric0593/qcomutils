#pragma once
#include <libxml.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <string>

using namespace std;

#define NULL_STR ""

class CXMLFile
{
    //存储文件全路径名
    string dbFileName;

    //xml
    xmlDocPtr pxmlDoc;
    //xml根元素
    xmlNodePtr root_node;

    //链表遍历指针
    xmlNodePtr tab_ipc;
    xmlNodePtr item_ipc;

    int getItemsCount(xmlNodePtr fatherNode);
    xmlNodePtr addNode(xmlNodePtr fatherNode, string name);
    xmlNodePtr findNode(xmlNodePtr fatherNode, int index);
    xmlNodePtr findNode(xmlNodePtr fatherNode, string name);
    xmlNodePtr findNodeNext(xmlNodePtr startNode, xmlNodePtr* ipc);
    xmlNodePtr findTab(string tab);
    xmlNodePtr findItemInTab(string tab,string item);
    string getNodeName(xmlNodePtr node);
    string getNodeContent(xmlNodePtr node);
    
public:

    CXMLFile(void);
    ~CXMLFile(void);
    bool openXMLDoc(CString filename);
    bool newXMLDoc(CString filename = _T(""));
    bool openXMLDoc(string filename);
    bool newXMLDoc(string filename = "",string rootname = "ROOT");

    bool addTab(string tab);
    bool editTabName(string tab_old, string tab_new);
    bool delTab(string tab);

    bool addItem(string tab,string item,string content);
    bool editItemName(string tab,string item_old,string item_new);
    bool delItem(string tab,string item);
    bool editItemContent(string tab,string item,string psw_new);

    int getTabsCount(void);
    int getItemsCountInTab(string tab);

    string getTab(int index);
    string getItemInTab(string tab, int index);

    string getTabFirst(void);
    string getTabNext(void);
    string getTabProp(string prop);
    CString getTabPropEx(string prop);

    string getItemInTabFirst(string tab);
    string getItemInTabNext(void);
    string getItemProp(string prop);
    CString getItemPropEx(string prop);

    string getContent(string tab,string item);
    CString getContentEx(string tab,string item);

    bool saveToFile(string filename = "", bool blankpad = true);
    bool saveToFile(CString filename = _T(""), bool blankpad = true);
    
    static void Init(void);
    static void CleanUp(void);
};
