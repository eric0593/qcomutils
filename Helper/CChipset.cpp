#include "StdAfx.h"
#include "CChipset.h"
#include <atlbase.h>

CChipset *CChipset::m_pChipset  = NULL;
int       CChipset::m_nRefCnt   = 0;

int CChipset::Create(CChipset **ppChipset)
{
    if(m_nRefCnt == 0)
    {
        m_pChipset = new CChipset();
        if(!ppChipset) m_nRefCnt++;
    }
    
    if(ppChipset)
    {
        *ppChipset = m_pChipset;
        m_nRefCnt++;
    }
    return m_nRefCnt;
}

int CChipset::Release(void)
{
    if(m_nRefCnt == 0)
    {
        return 0;
    }
    else if(m_nRefCnt == 1 && m_pChipset)
    {
        delete m_pChipset;
        m_pChipset = NULL;
    }
    m_nRefCnt--;
    return m_nRefCnt;
}

CChipset::CChipset(void)
{
    // ������д��ע���
    CRegKey myRegKey;
    if(myRegKey.Open(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Control\\UsbFlags")) == ERROR_SUCCESS)
    {
        byte  bData[16];
        ULONG nLen;
        CString myKey;
        for(int i= 0x9000;i<0x9050;i++)
        {
            myKey.Format(_T("IgnoreHWSerNum05C6%04X"),i);
            nLen = sizeof(bData);
            if(myRegKey.QueryBinaryValue(myKey,bData,&nLen) != ERROR_SUCCESS)
            {
                bData[0] = 1;
                myRegKey.SetBinaryValue(myKey,bData,1);
            }
        }
        myRegKey.Close();
    }
    
    m_pIniFile = new CIniFile(CHIPSET_INFO_FILE);
    m_pIniFile->GetSections(m_ChipsetNameArr);
    m_pChipsetInfo = new ChipsetInfo[m_ChipsetNameArr.GetCount()];
    for(int i=0; i<m_ChipsetNameArr.GetCount(); i++)
    {
        m_pChipsetInfo[i].m_szKeywords      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_KEYWORDS_KEY);
        m_pChipsetInfo[i].m_bMultiMBN       = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_MULTIMBN_KEY);
        m_pChipsetInfo[i].m_bSecMode        = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_SECMODE_KEY);
        m_pChipsetInfo[i].m_bJumpBoot       = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_JUMPBOOT_KEY);
        m_pChipsetInfo[i].m_bJumpAddr       = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_JUMPADDR_KEY);
        m_pChipsetInfo[i].m_szARMPRGName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_ARMPRGNAME_KEY);
        m_pChipsetInfo[i].m_szPartition     = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_PARTITION_KEY);
        m_pChipsetInfo[i].m_szPBLName       = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_PBLNAME_KEY);
        m_pChipsetInfo[i].m_szQCSBLHDCFGName= m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_QCSBLHDCFGNAME_KEY);
        m_pChipsetInfo[i].m_szQCSBLName     = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_QCSBLNAME_KEY);
        m_pChipsetInfo[i].m_szOEMSBLHDName  = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_OEMSBLHDNAME_KEY);
        m_pChipsetInfo[i].m_szOEMSBLName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_OEMSBLNAME_KEY);
        m_pChipsetInfo[i].m_szAMSSHDName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_AMSSHDNAME_KEY);
        m_pChipsetInfo[i].m_szAMSSName      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_AMSSNAME_KEY);
        m_pChipsetInfo[i].m_szAPPSHDName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_APPSHDNAME_KEY);
        m_pChipsetInfo[i].m_szAPPSName      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_APPSNAME_KEY);
        m_pChipsetInfo[i].m_szOBLName       = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_OBLNAME_KEY);
        m_pChipsetInfo[i].m_szFOTAUIName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_FOTAUINAME_KEY);
        m_pChipsetInfo[i].m_szCEFSName      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_CEFSNAME_KEY);
        m_pChipsetInfo[i].m_szAPPSBLHDName  = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_APPSBLHDNAME_KEY);
        m_pChipsetInfo[i].m_szAPPSBLName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_APPSBLNAME_KEY);
        m_pChipsetInfo[i].m_szAPPSCEFSName  = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_APPS_CEFSNAME_KEY);
        m_pChipsetInfo[i].m_szFLASHBINName  = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_FLASH_BINNAME_KEY);
        m_pChipsetInfo[i].m_szDSP1Name      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_DSP1NAME_KEY);
        m_pChipsetInfo[i].m_szCUSTOMName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_CUSTOMNAME_KEY);
        m_pChipsetInfo[i].m_szDBLName       = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_DBLNAME_KEY);
        m_pChipsetInfo[i].m_szFSBLName      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_FSBLNAME_KEY);
        m_pChipsetInfo[i].m_szOSBLName      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_OSBLNAME_KEY);
        m_pChipsetInfo[i].m_szDSP2Name      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_DSP2NAME_KEY);
        m_pChipsetInfo[i].m_szRAWName       = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_RAWNAME_KEY);
        m_pChipsetInfo[i].m_szARDBOOTName   = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_ARDBOOTNAME_KEY);
        m_pChipsetInfo[i].m_szARDSYSName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_ARDSYSNAME_KEY);
        m_pChipsetInfo[i].m_szARDUSERName   = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_ARDUSERNAME_KEY);
        m_pChipsetInfo[i].m_szARDRCYName    = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_ARDRCYNAME_KEY);
        m_pChipsetInfo[i].m_szFactName      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_FACTNAME_KEY);
        m_pChipsetInfo[i].m_szNVSample      = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_NVSAMPLE_KEY);
        m_pChipsetInfo[i].m_szEMMCProgram   = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_EMMCPROGRAM_KEY);
        m_pChipsetInfo[i].m_szEMMCPatch     = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_EMMCPATCH_KEY);
        m_pChipsetInfo[i].m_szMsimageName   = m_pIniFile->GetValue(m_ChipsetNameArr[i],     CHIPSET_INFO_MSIMAGENAME_KEY);
        m_pChipsetInfo[i].m_bISEMMC         = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_ISEMMC_KEY);
        m_pChipsetInfo[i].m_bIsUSBBoot      = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_ISUSBBOOT_KEY);
        m_pChipsetInfo[i].m_bIsUnframe      = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_ISUNFRAME_KEY);
        m_pChipsetInfo[i].m_bSubNum         = m_pIniFile->GetIntValue(m_ChipsetNameArr[i],  CHIPSET_INFO_SUBNUM_KEY);
    }
}

CChipset::~CChipset(void)
{
    if(m_pIniFile)
    {
        delete m_pIniFile;
    }

    if(m_pChipsetInfo)
    {
        delete [] m_pChipsetInfo;
    }
}
    
void CChipset::RefreshChipsetList(CComboBox &cChipsetCombo, int nIndex)
{
    int nSel = cChipsetCombo.GetCurSel();
    
    if(nSel<0)
    {
        nSel = 0;
    }

    cChipsetCombo.ResetContent();
    for(int i=0; i<m_ChipsetNameArr.GetCount(); i++)
    {
        if(nIndex == i)
        {
            nSel = i;
        }
        cChipsetCombo.AddString(m_ChipsetNameArr[i]);
    }
    cChipsetCombo.SetCurSel(nSel);
}

CString &CChipset::GetChipsetName(int nChipsetIdx)
{
    if(nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_ChipsetNameArr[nChipsetIdx];
    }
    return m_szEmpty;
}

CString &CChipset::GetKeywords(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szKeywords;
    }
    return m_szEmpty;
}

BYTE CChipset::IsMultiMBN(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_bMultiMBN;
    }
    return FALSE;
}

BYTE CChipset::GetSecMode(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_bSecMode;
    }
    return 0;
}

BOOL CChipset::IsJumbBoot(int nChipsetIdx, DWORD *pJumpAddr)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        if(pJumpAddr)
        {
            *pJumpAddr = m_pChipsetInfo[nChipsetIdx].m_bJumpAddr;
        }
        return m_pChipsetInfo[nChipsetIdx].m_bJumpBoot;
    }
    return FALSE;
}

CString &CChipset::GetARMPRGName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szARMPRGName;
    }
    return m_szEmpty;
}

CString &CChipset::GetPartitionName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szPartition;
    }
    return m_szEmpty;
}

CString &CChipset::GetPBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szPBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetQCSBLHDCFGName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szQCSBLHDCFGName;
    }
    return m_szEmpty;
}

CString &CChipset::GetQCSBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szQCSBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetOEMSBLHDName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szOEMSBLHDName;
    }
    return m_szEmpty;
}

CString &CChipset::GetOEMSBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szOEMSBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetAMSSHDName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szAMSSHDName;
    }
    return m_szEmpty;
}

CString &CChipset::GetAMSSName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szAMSSName;
    }
    return m_szEmpty;
}

CString &CChipset::GetAPPSHDName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szAPPSHDName;
    }
    return m_szEmpty;
}

CString &CChipset::GetAPPSName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szAPPSName;
    }
    return m_szEmpty;
}

CString &CChipset::GetOBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szOBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetFOTAUIName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szFOTAUIName;
    }
    return m_szEmpty;
}

CString &CChipset::GetCEFSName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szCEFSName;
    }
    return m_szEmpty;
}

CString &CChipset::GetAPPSBLHDName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szAPPSBLHDName;
    }
    return m_szEmpty;
}

CString &CChipset::GetAPPSBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szAPPSBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetAPPSCEFSName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szAPPSCEFSName;
    }
    return m_szEmpty;
}

CString &CChipset::GetFLASHBINName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szFLASHBINName;
    }
    return m_szEmpty;
}

CString &CChipset::GetDSP1Name(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szDSP1Name;
    }
    return m_szEmpty;
}

CString &CChipset::GetCUSTOMName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szCUSTOMName;
    }
    return m_szEmpty;
}

CString &CChipset::GetDBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szDBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetFSBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szFSBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetOSBLName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szOSBLName;
    }
    return m_szEmpty;
}

CString &CChipset::GetDSP2Name(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szDSP2Name;
    }
    return m_szEmpty;
}

CString &CChipset::GetRAWName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szRAWName;
    }
    return m_szEmpty;
}

CString &CChipset::GetARDBOOTName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szARDBOOTName;
    }
    return m_szEmpty;
}

CString &CChipset::GetARDSYSName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szARDSYSName;
    }
    return m_szEmpty;
}

CString &CChipset::GetARDUSERName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szARDUSERName;
    }
    return m_szEmpty;
}

CString &CChipset::GetARDRCYName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szARDRCYName;
    }
    return m_szEmpty;
}

CString &CChipset::GetFactName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szFactName;
    }
    return m_szEmpty;
}

CString &CChipset::GetNVSample(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szNVSample;
    }
    return m_szEmpty;
}

CString &CChipset::GetEMMCProgram(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szEMMCProgram;
    }
    return m_szEmpty;
}

CString &CChipset::GetEMMCPatch(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szEMMCPatch;
    }
    return m_szEmpty;
}

CString &CChipset::GetMsimageName(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_szMsimageName;
    }
    return m_szEmpty;
}

BYTE CChipset::GetSubNum(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_bSubNum;
    }
    return 0;
}

BOOL CChipset::IsEMMC(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_bISEMMC;
    }
    return FALSE;
}

BOOL CChipset::IsUSBBoot(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_bIsUSBBoot;
    }
    return FALSE;
}

BOOL CChipset::IsUnframe(int nChipsetIdx)
{
    if(m_pChipsetInfo && nChipsetIdx<m_ChipsetNameArr.GetCount())
    {
        return m_pChipsetInfo[nChipsetIdx].m_bIsUnframe;
    }
    return FALSE;
}