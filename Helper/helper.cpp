/*====================================================================================================

                                       ==MODIFY HISTOY FOR FILE==

  when         who     what, where, why
  ----------   ---     ----------------------------------------------------------
  03/08/2004   tyz      create
  
=====================================================================================================*/

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*===========================================================================

                  INCLUDE FILES FOR MODULE
  
===========================================================================*/
#include "stdafx.h"
#include "locale.h"
#include "helper.h"
#include "fs_diag_i.h"

/*===========================================================================

                  DEFINITIONS AND CONSTANTS
  
===========================================================================*/
#define MAX_FILENAME      FS_FILENAME_MAX_LENGTH_P

/*===========================================================================

                     FUNCTION DEFINITION
  
===========================================================================*/
//length is the dest string length in byte
int UTF8TOTCHAR(TCHAR* dest, const char* pSource, int length)
{
   int      rlength;
   
   setlocale(LC_CTYPE,".OCP");    //设定本地语言集。
   //memset(dest, 0, MAX_FILENAME*2);
   
   //UTF8 to Unicode
   rlength = MultiByteToWideChar(
                                   CP_UTF8,                // code page
                                   0,                      // character-type options
                                   pSource,                // address of string to map
                                   -1,                     // number of bytes in string
                                   dest,                   // address of wide-character buffer
                                   MAX_FILENAME            // size of buffer
                                );     
   
   return rlength;
}

//length is the dest string count in byte
int TCHARTOUTF8(char* dest, const TCHAR* pSource, int length)
{
   wchar_t  temp[MAX_FILENAME] = {0};
   int      rlength;
   
   setlocale(LC_CTYPE,".OCP");    //设定本地语言集。
   
   //Unicode to UTF8
   rlength = WideCharToMultiByte(
                                   CP_UTF8,                    // code page
                                   0,                          // performance and mapping flags
                                   pSource,                       // address of wide-character string
                                   -1,                         // number of characters in string
                                   dest,                       // address of buffer for new string
                                   length,                     // size of buffer
                                   NULL,                       // address of default for unmappable characters
                                   NULL                        // address of flag set when default char. used
                                );
   
   return rlength;
}

BOOL IsMP3Valid(CString name)
{
   int            i, j, counter, block;               //i指示当前比较buffer中哪个字节，j表示本次比较的起始位置，
   BOOL           tag1 = FALSE, tag2 = FALSE;  //tag1表示有没有找到真正的framHeader, tag2表示该文件是不是有效的文件
   unsigned long  framHeader;
   unsigned char  buf[4096] = {0};             //对128kbps,44.1k采样率的mp3大概有10帧
   CFile          file;

   if(!file.Open(name, CFile::modeRead | CFile::shareDenyNone))
   {  
      return FALSE;
   }
   file.Read(buf, 4096);
   block = 0;
   j = 0;
   i = j;
   while(tag1 == FALSE && block < 4)
   {
      counter = 0;
      for(i = j;i < 4096; i++)
      {  //寻找第一字节为0xff和第二个字高3位为全1且低5位不全为1（因为有的mp3中很长的0xff的无效数据）的数据段
         if(buf[i] == 0xff && (buf[i+1]&0xe0) == 0xe0 && buf[i+1] != 0xff)     
         {
            framHeader = *((unsigned long*) &buf[i]);    //如果有这样的数据段，假定它是framHeader
            counter ++;   
            break;
         }
      }
      
      j = i + 1;                                   //将本次搜索中第一次发现可能是framHeader，位置+1记录下来；如果证明不是真正的framHeader，下次就从这里开始搜索。
      i += 4;
//用计算匹配次数的方法判断是不是真正的帧头，并不严谨，比较好的办法是根据假定帧计算帧长度，检查当前位置+帧长的位置是不是匹配。
      for(;i < 4096; i++)                 
      {  //比较以前用0xfffffc00与将比较的数据作“按位与”，对于同一个文件的帧头掩码中为0的部分是可能改变的，
         //比如第9位Padding bit,表明该帧是否增加一个占位字节。占位字节的目的是使数据率更精确
         //对帧头而言掩码应该是0xfffffc00，但long型数据的低字节读的是文件的低字节，所以实际的掩码是0x00fcffff
         if(((*((unsigned long*) &buf[i])) & 0x00fcffff) == (framHeader & 0x00fcffff))
         {
            counter ++;
            i += 3 ;                                     //如果和framHeader匹配，则也假定为另一个framHeader。framHeader的长度是4byte，所以+3+1 （+1在for中加）；
         }
      }
      if( 3 < counter && counter < 80)                   //确认这个frameheader是正确的。比较模糊
      {
         tag1 = TRUE;
      }
      else
      {
         if(j < 4096)
         {
            i = j;
         }
         else
         {
            file.Read(buf, 4096);                           //读取后续4k数据到buffer
            j = 0;
            i = j;
            block ++;
         }
      }
   }
   
   if( (framHeader & 0x00001a00) == 0x00001a00 || (framHeader & 0x00001200) == 0x00001200)  //0x00001a00表示mpeg version1 and layer3；0x00001200表示mpeg version2 and layer3
   {
      tag2 = tag1;      
   }

   file.Close();
   return tag2;
}

BOOL RemoveDir(CString sourceDirName)
{
   SHFILEOPSTRUCT FileOp = {0};
   
   sourceDirName += '\\';
   sourceDirName.SetAt(sourceDirName.GetLength()-1,'\0'); //该string有两个null，这是fileop要求的结尾格式。
   
   FileOp.hwnd = NULL;
   FileOp.wFunc = FO_DELETE;
   FileOp.pFrom = sourceDirName;
   FileOp.pTo = NULL;
   FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT ;
   FileOp.fAnyOperationsAborted = FALSE;
   FileOp.hNameMappings = NULL;
   FileOp.lpszProgressTitle = NULL;
   
   return !SHFileOperation(&FileOp);
}

BOOL CopyDirectory(CString sourceDirName, CString destDirName)
{
   SHFILEOPSTRUCT FileOp = {0};
   
   sourceDirName += "\\*.*\\";
   destDirName += '\\';
   sourceDirName.SetAt(sourceDirName.GetLength()-1,'\0'); //该string有两个null，这是fileop要求的结尾格式。
   destDirName.SetAt(destDirName.GetLength()-1,'\0');
   
   FileOp.hwnd = NULL;
   FileOp.wFunc = FO_COPY;
   FileOp.pFrom = sourceDirName;
   FileOp.pTo = destDirName;
   FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR | FOF_NOERRORUI | FOF_SILENT ;
   FileOp.fAnyOperationsAborted = FALSE;
   FileOp.hNameMappings = NULL;
   FileOp.lpszProgressTitle = NULL;
   
   return !SHFileOperation(&FileOp);
}

LPCTSTR GetLpctstrFromCString(LPCTSTR lpszCString)
{
    return lpszCString;
}