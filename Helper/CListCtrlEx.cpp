// ListCtrlEx.cpp : implementation file
//

#include "stdafx.h"
#include "CListCtrlEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx

CListCtrlEx::CListCtrlEx()
{
}

CListCtrlEx::~CListCtrlEx()
{
}


BEGIN_MESSAGE_MAP(CListCtrlEx, CListCtrl)
	//{{AFX_MSG_MAP(CListCtrlEx)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnEndlabeledit)
	ON_WM_LBUTTONDOWN()
	ON_WM_DESTROY()
    ON_CBN_CLOSEUP(CLISTCTRLEX_COMBO_ID, OnCbnCloseup)
    ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx message handlers

void CListCtrlEx::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	if (GetFocus() != this) SetFocus();
	
	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CListCtrlEx::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	if (GetFocus() != this) SetFocus();
	
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CListCtrlEx::ShowSubCtrl(int nItem, int nSubItem,DWORD dwCtrlType/* = COLUMN_STYLE_EDIT*/)
{
    // The returned pointer should not be saved
	
    // Make sure that the item is visible
    if (!EnsureVisible (nItem, TRUE)) return;
	
    // Make sure that nCol is valid
    CHeaderCtrl* pHeader = (CHeaderCtrl*) GetDlgItem(0);
    int nColumnCount = pHeader->GetItemCount();
    if (nSubItem >= nColumnCount || GetColumnWidth (nSubItem) < 5)
	{
		return;
	}
	
    // Get the column offset
    int Offset = 0;
    for (int iColumn = 0; iColumn < nSubItem; iColumn++)
		Offset += GetColumnWidth (iColumn);
	
    CRect Rect;
    GetItemRect (nItem, &Rect, LVIR_BOUNDS);
	
    // Now scroll if we need to expose the column
    CRect ClientRect;
    GetClientRect (&ClientRect);
    if (Offset + Rect.left < 0 || Offset + Rect.left > ClientRect.right)
    {
		CSize Size;
		if (Offset + Rect.left > 0)
			Size.cx = -(Offset - Rect.left);
		else
			Size.cx = Offset - Rect.left;
		Size.cy = 0;
		Scroll (Size);
		Rect.left -= Size.cx;
    }
	
    // Get nSubItem alignment
    LV_COLUMN lvCol;
    lvCol.mask = LVCF_FMT;
    GetColumn (nSubItem, &lvCol);
    DWORD dwStyle;
    if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		dwStyle = ES_LEFT;
    else if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		dwStyle = ES_RIGHT;
    else dwStyle = ES_CENTER;
	
    Rect.left += Offset;
    Rect.right = Rect.left + GetColumnWidth (nSubItem);
    if (Rect.right > ClientRect.right)
		Rect.right = ClientRect.right;
	
    dwStyle |= WS_BORDER | WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL;

	CString strCulText=GetItemText (nItem, nSubItem);
	if(dwCtrlType==COLUMN_STYLE_EDIT)
	{
		m_pEdit = new CListCellEdit (nItem, nSubItem, strCulText);
		m_pEdit->Create (dwStyle, Rect, this, CLISTCTRLEX_EDIT_ID);
	}
	if(dwCtrlType==COLUMN_STYLE_COMBOBOX)
	{
		//显示组合框
        if(m_arColumnInfo[nSubItem]->m_parComboString)
        {
		    CRect rc(Rect.left,Rect.top,Rect.right,Rect.bottom+100);
		    m_pCombo = new CListCellCombo(nItem,nSubItem,strCulText);
		    m_pCombo->Create(WS_CHILD|WS_VISIBLE|WS_VSCROLL|CBS_DROPDOWNLIST,	rc,this, CLISTCTRLEX_COMBO_ID);
		
			for(int i=0;i<m_arColumnInfo[nSubItem]->m_parComboString->GetSize();i++)
			{
                const CString &szStr = m_arColumnInfo[nSubItem]->m_parComboString->GetAt(i);
                if(!szStr.IsEmpty())
                {
				    m_pCombo->AddString(szStr);
                }
			}
        
            if(m_arColumnInfo[nSubItem]->m_nSelIdx < 0)
            {
 		        int nIndex=m_pCombo->SelectString(0,strCulText);
		        if(nIndex!=CB_ERR)
		        {
			        m_pCombo->SetCurSel(nIndex);
		        }
		        else
		        {
			        m_pCombo->SetCurSel(-1);
		        }
            }
            else
            {
                m_pCombo->SetCurSel(m_arColumnInfo[nSubItem]->m_nSelIdx);
            }
        }
	}	
}

void CListCtrlEx::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	LV_ITEM	*plvItem = &pDispInfo->item;
	
    if (plvItem->pszText != NULL)
    {
		SetItemText (plvItem->iItem, plvItem->iSubItem, plvItem->pszText);
    }
	
	*pResult = 0;
}

int CListCtrlEx::InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat, int nWidth, DWORD nStyle,CStringArray * pStringArray, int nSubItem)
{
	int nIndex=CListCtrl::InsertColumn(nCol, lpszColumnHeading, nFormat ,nWidth ,nSubItem );
	if(nIndex!=-1)
	{
		LPSUBITEMINFO pSII=new SUBITEMINFO;
		pSII->m_dwColumnType=nStyle;
		pSII->m_parComboString=pStringArray;
		m_arColumnInfo.InsertAt(nIndex,pSII);
	}
	return nIndex;
}

int CListCtrlEx::InsertColumn( int nCol, const LVCOLUMN* pColumn, DWORD nStyle,CStringArray * pStringArray)
{
	int nIndex=CListCtrl::InsertColumn(nCol,pColumn);
	if(nIndex!=-1)
	{
		LPSUBITEMINFO pSII=new SUBITEMINFO;
		pSII->m_dwColumnType=nStyle;
		pSII->m_parComboString=pStringArray;
		m_arColumnInfo.InsertAt(nIndex,pSII);
	}
	return nIndex;
}

int CListCtrlEx::UpdateColumn( int nCol, DWORD nStyle ,CStringArray * pStringArray, int nSel)
{
    LPSUBITEMINFO pSII=m_arColumnInfo.GetAt(nCol);
    if(pSII == NULL)
    {
        pSII=new SUBITEMINFO;
        pSII->m_dwColumnType=nStyle;
	    pSII->m_parComboString=pStringArray;
        pSII->m_nSelIdx = nSel;
        m_arColumnInfo.InsertAt(nCol,pSII);
        return 0;
    }
	pSII->m_dwColumnType=nStyle;
	pSII->m_parComboString=pStringArray;
    pSII->m_nSelIdx = nSel;
    return 0;
}

BOOL CListCtrlEx::DeleteColumn(int nCol )
{
	BOOL bResult=CListCtrl::DeleteColumn(nCol);
	if(bResult)
	{
		delete m_arColumnInfo.GetAt(nCol);
		m_arColumnInfo.RemoveAt(nCol);
	}
	return bResult;
}

void CListCtrlEx::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CListCtrl::OnLButtonDown(nFlags, point);
	// TODO: Add your message handler code here and/or call default
    int nItem;
    int nSubItem;
    if ((nItem = HitTestEx (point, nSubItem)) ==-1)
    {
		return;
	}
    
	switch(m_arColumnInfo[nSubItem]->m_dwColumnType)
	{
	case COLUMN_STYLE_GENERAL:
		break;
	case COLUMN_STYLE_EDIT:
		ShowSubCtrl(nItem,nSubItem,COLUMN_STYLE_EDIT);
		break;
	case COLUMN_STYLE_COMBOBOX:
        if(m_arColumnInfo[nSubItem]->m_parComboString && m_arColumnInfo[nSubItem]->m_parComboString->GetCount() == 0)
        {
            break;
        }
		ShowSubCtrl(nItem,nSubItem,COLUMN_STYLE_COMBOBOX);
		break;
	default:
		break;
	}
}

int CListCtrlEx::HitTestEx (CPoint& Point, int& nSubItem)
{
	nSubItem = 0;
	int ColumnNum = 0;
    int Row = HitTest (Point, NULL);
	
    // Make sure that the ListView is in LVS_REPORT
    if ((GetWindowLong (m_hWnd, GWL_STYLE) & LVS_TYPEMASK) != LVS_REPORT)
		return Row;
	
    // Get the top and bottom row visible
    Row = GetTopIndex();
    int Bottom = Row + GetCountPerPage();
    if (Bottom > GetItemCount())
		Bottom = GetItemCount();
    
    // Get the number of columns
    CHeaderCtrl* pHeader = (CHeaderCtrl*) GetDlgItem(0);
    int nColumnCount = pHeader->GetItemCount();
	
    // Loop through the visible rows
    for(; Row <= Bottom; Row++)
    {
		// Get bounding rect of item and check whether point falls in it.
		CRect Rect;
		GetItemRect (Row, &Rect, LVIR_BOUNDS);
		if (Rect.PtInRect (Point))
		{
			// Now find the column
			for (ColumnNum = 0; ColumnNum < nColumnCount; ColumnNum++)
			{
				int ColWidth = GetColumnWidth (ColumnNum);
				if (Point.x >= Rect.left && Point.x <= (Rect.left + ColWidth))
				{
					nSubItem = ColumnNum;
					return Row;
				}
				Rect.left += ColWidth;
			}
		}
    }
	
    return -1;
}

void CListCtrlEx::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
    CRect rc;
    CString str;

    CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
    COLORREF colorBg    = ::GetSysColor(COLOR_WINDOW);
    COLORREF colorText  = ::GetSysColor(COLOR_WINDOWTEXT);

    CDC MemDC; //首先定义一个显示设备对象   
    CBitmap MemBitmap;//定义一个位图对象   
    
    CRect rcClip, rcClient; 
    pDC->GetClipBox(&rcClip);
    rcClip.IntersectRect(rcClip, &lpDrawItemStruct->rcItem);
    GetClientRect(&rcClient); 

    MemDC.CreateCompatibleDC(pDC);
    //这时还不能绘图，因为没有地方画 ^_^   
    //下面建立一个与屏幕显示兼容的位图，至于位图的大小嘛，可以用窗口的大小   
    MemBitmap.CreateCompatibleBitmap(pDC,rcClient.Width(),rcClient.Height());   
    
    //将位图选入到内存显示设备中   
    //只有选入了位图的内存显示设备才有地方绘图，画到指定的位图上  
    MemDC.SelectObject(&MemBitmap);
    
    //设置大小和dc一样大 
    CRgn   rgnClip; 
    rgnClip.CreateRectRgnIndirect(&rcClip); 
    MemDC.SelectClipRgn(&rgnClip); 
    rgnClip.DeleteObject(); 
    
    MemDC.SetBkColor(colorBg); 
    MemDC.SetTextColor(colorText); 

    ::SetBkMode(MemDC,TRANSPARENT);

    for(int i=0; i<m_arColumnInfo.GetCount();i++)
    {
        GetSubItemRect(lpDrawItemStruct->itemID,i,LVIR_BOUNDS,rc);
        str = GetItemText(lpDrawItemStruct->itemID,i);
        
        if(i == 0 && m_arColumnInfo.GetCount()>1)
        {
            CRect rcTemp;
            GetSubItemRect(lpDrawItemStruct->itemID,1,LVIR_BOUNDS,rcTemp);
            rc.right = rcTemp.left;
        }

        // Get nSubItem alignment
        LV_COLUMN lvCol;
        lvCol.mask = LVCF_FMT;
        GetColumn (i, &lvCol);
        DWORD dwStyle;
        if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		    dwStyle = DT_LEFT;
        else if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		    dwStyle = DT_RIGHT;
        else dwStyle = DT_CENTER;

        if(m_arColumnInfo[i]->m_dwColumnType == COLUMN_STYLE_PROGRESS)
        {
            // 显示进度条的规则
            // 数字 非数字 数字 20-100 前面为Curr 后面为Total， 同时显示进度和字符
            // 首字符如果为@则不显示字符，如 @20-100，只显示进度条
            // PASS显示成功
            // FAIL显示失败
            if(str == _T("PASS"))
            {
                MemDC.FillSolidRect(rc,COLOR_PASS);
                ::DrawText(MemDC,str,str.GetLength(),rc,dwStyle | DT_VCENTER | DT_SINGLELINE);
            }
            else if(str == _T("FAIL"))
            {
                MemDC.FillSolidRect(rc,COLOR_FAIL);
                ::DrawText(MemDC,str,str.GetLength(),rc,dwStyle | DT_VCENTER | DT_SINGLELINE);
            }
            else if(!str.IsEmpty())
            {
                DWORD dwCurr = 0;
                DWORD dwTotal = 0;
                BOOL  bFindSplit = FALSE;
                BOOL  bShowString = TRUE;
                const TCHAR *pStr = str;
                CString szCurr;
                CString szTotal;
                
                if(*pStr == '@')
                {
                    bShowString = FALSE;
                    pStr++;
                }

                while(*pStr)
                {
                    if(*pStr >= '0' && *pStr <= '9')
                    {
                        if(bFindSplit)
                        {
                            szTotal.AppendChar(*pStr);
                        }
                        else
                        {
                            szCurr.AppendChar(*pStr);
                        }
                    }
                    else
                    {
                        bFindSplit = TRUE;
                    }
                    pStr++;
                }

                dwCurr = _tcstol(szCurr,NULL,10);
                dwTotal = _tcstol(szTotal,NULL,10);
                if(dwTotal > (DWORD)(rc.right-rc.left)*(rc.right-rc.left)) // 防止乘除越界
                {
                    dwCurr /= (rc.right-rc.left);
                    dwTotal /= (rc.right-rc.left);
                }
                if(dwCurr > dwTotal)
                {
                    dwCurr = dwTotal;
                }
                if(dwTotal && dwTotal >= dwCurr)
                {
                    CRect rcProg = rc;
                    rcProg.right = rcProg.left+(((rcProg.right-rcProg.left)*dwCurr)/dwTotal);
                    MemDC.FillSolidRect(rc,colorBg);
                    MemDC.FillSolidRect(rcProg,COLOR_PROGRESS);
                }
                else
                {
                    bShowString = TRUE;
                    MemDC.FillSolidRect(rc,colorBg);
                }
                
                if(bShowString)
                {
                    ::DrawText(MemDC,str,str.GetLength(),rc,dwStyle | DT_VCENTER | DT_SINGLELINE);
                }
            }
            else
            {
                MemDC.FillSolidRect(rc,colorBg);
            }
        }
        else
        {
            MemDC.FillSolidRect(rc,colorBg);
            ::DrawText(MemDC,str,str.GetLength(),rc,dwStyle | DT_VCENTER | DT_SINGLELINE);
        }
    }
    
    pDC->BitBlt(rcClip.left,rcClip.top,rcClip.Width(),rcClip.Height(),&MemDC,rcClip.left,rcClip.top,SRCCOPY);
    MemBitmap.DeleteObject();   
    MemDC.DeleteDC();
}

BOOL CListCtrlEx::OnEraseBkgnd(CDC* pDC)
{
    return TRUE;
}

void CListCtrlEx::OnCbnCloseup() 
{
    m_pCombo->DestroyWindow();
}

void CListCtrlEx::OnDestroy() 
{
	
	// TODO: Add your message handler code here
	int nCount=m_arColumnInfo.GetSize();
	for(int i=0;i<nCount;i++)
	{
		delete m_arColumnInfo.GetAt(i);
	}
	m_arColumnInfo.RemoveAll();
	
	CListCtrl::OnDestroy();
	
}

/////////////////////////////////////////////////////////////////////////////
// CListCellEdit

CListCellEdit::	CListCellEdit(int nItem, int nSubItem, CString strInitText)
{
    m_nItem = nItem;
    m_nSubItem = nSubItem;
    m_strInitText = strInitText;
	m_bEscape=FALSE;
}

CListCellEdit::~CListCellEdit()
{
}


BEGIN_MESSAGE_MAP(CListCellEdit, CEdit)
	//{{AFX_MSG_MAP(CListCellEdit)
	ON_WM_KILLFOCUS()
	ON_WM_NCDESTROY()
	ON_WM_CHAR()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListCellEdit message handlers

void CListCellEdit::OnKillFocus(CWnd* pNewWnd) 
{
	CEdit::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
	SetListItemText();

    DestroyWindow();
}

void CListCellEdit::OnNcDestroy() 
{
	CEdit::OnNcDestroy();
	
	// TODO: Add your message handler code here
    delete this;
}

BOOL CListCellEdit::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		if(pMsg->wParam == VK_RETURN
			|| pMsg->wParam == VK_DELETE
			|| pMsg->wParam == VK_ESCAPE
			|| GetKeyState( VK_CONTROL)
			)
		{
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			return TRUE;		    	// DO NOT process further
		}
	}
	
	return CEdit::PreTranslateMessage(pMsg);
}

void CListCellEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if( nChar == VK_ESCAPE || nChar == VK_RETURN)
	{
		if( nChar == VK_ESCAPE )
			m_bEscape = TRUE;
		GetParent()->SetFocus();

		return;
	}

	// Resize edit control if needed
	// Get text extent
	CString str;

	GetWindowText( str );
	CWindowDC dc(this);
	CFont *pFont = GetParent()->GetFont();
	CFont *pFontDC = dc.SelectObject( pFont );
	CSize size = dc.GetTextExtent( str );
	dc.SelectObject( pFontDC );
	size.cx += 5;			   	// add some extra buffer

	// Get client rect
	CRect rect, parentrect;
	GetClientRect( &rect );
	GetParent()->GetClientRect( &parentrect );

	// Transform rect to parent coordinates
	ClientToScreen( &rect );
	GetParent()->ScreenToClient( &rect );

	// Check whether control needs to be resized
	// and whether there is space to grow
	if( size.cx > rect.Width() )
	{
		if( size.cx + rect.left < parentrect.right )
			rect.right = rect.left + size.cx;
		else
			rect.right = parentrect.right;
		MoveWindow( &rect );
	}
	
	CEdit::OnChar(nChar, nRepCnt, nFlags);
}

int CListCellEdit::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// Set the proper font
	CFont* font = GetParent()->GetFont();
	SetFont(font);

	SetWindowText( m_strInitText );
	SetFocus();
	SetSel( 0, -1 );
	
	return 0;
}

void CListCellEdit::SetListItemText()
{
    CString Text;
    GetWindowText (Text);

    // Send Notification to parent of ListView ctrl
    LV_DISPINFO dispinfo;
    dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
    dispinfo.hdr.idFrom = GetDlgCtrlID();
    dispinfo.hdr.code = LVN_ENDLABELEDIT;

    dispinfo.item.mask = LVIF_TEXT;
    dispinfo.item.iItem = m_nItem;
    dispinfo.item.iSubItem = m_nSubItem;
    dispinfo.item.pszText = m_bEscape ? NULL : LPTSTR ((LPCTSTR) Text);
    dispinfo.item.cchTextMax = Text.GetLength();
    GetParent()->GetParent()->SendMessage (WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM) &dispinfo);
}
/////////////////////////////////////////////////////////////////////////////
// CListCellCombo

CListCellCombo::CListCellCombo()
{
}

CListCellCombo::CListCellCombo(int nItem,int nSubItem,CString strWindowText)
{
	m_iRowIndex=nItem;
	m_iColumnIndex=nSubItem;
	m_strWindowText=strWindowText;
}

CListCellCombo::~CListCellCombo()
{
}


BEGIN_MESSAGE_MAP(CListCellCombo, CComboBox)
	//{{AFX_MSG_MAP(CListCellCombo)
	ON_WM_KILLFOCUS()
	ON_WM_CREATE()
	ON_WM_NCDESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListCellCombo message handlers

BOOL CListCellCombo::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	// If the message if for "Enter" or "Esc"
	// Do not process
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			// DO NOT process further
			return TRUE;				
		}
	}
	
	return CComboBox::PreTranslateMessage(pMsg);
}

void CListCellCombo::OnKillFocus(CWnd* pNewWnd) 
{
	CComboBox::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
	CString str;
	GetWindowText(str);
	
	// Send Notification to parent of ListView ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = LVN_ENDLABELEDIT;
	
	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_iRowIndex;
	dispinfo.item.iSubItem = m_iColumnIndex;
	dispinfo.item.pszText = LPTSTR((LPCTSTR)str);
	dispinfo.item.cchTextMax = str.GetLength();
	
	GetParent()->SendMessage(WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);

	// Close the control
	DestroyWindow();
}

int CListCellCombo::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CComboBox::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	CFont* pFont = GetParent()->GetFont();
	SetFont(pFont);	
	SetFocus();
	
	return 0;
}

void CListCellCombo::OnNcDestroy() 
{
	CComboBox::OnNcDestroy();
	
	// TODO: Add your message handler code here
	delete this;
}
