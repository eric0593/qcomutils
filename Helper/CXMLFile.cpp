#include "StdAfx.h"
#include <string>
#include <stdlib.h>
#include "CXMLFile.h"
#include <libxml/globals.h>
#include <libxml/xmlmemory.h>

CXMLFile::CXMLFile()
{
    dbFileName.clear();
    pxmlDoc = NULL;
    tab_ipc = NULL;
    item_ipc = NULL;
}

CXMLFile::~CXMLFile()
{
    if(pxmlDoc)
        xmlFreeDoc(pxmlDoc);
}

void CXMLFile::Init(void)
{
    xmlInitParser();
}

void CXMLFile::CleanUp(void)
{
    xmlCleanupParser();
}

bool CXMLFile::openXMLDoc(string filename)
{
    /*parse the file and get the DOM */
    pxmlDoc = xmlReadFile(filename.c_str(),NULL,0);
    if (NULL == pxmlDoc)
        return false;

    /*Get the root element node */
    root_node = xmlDocGetRootElement(pxmlDoc);
    //检查确认当前文档中包含内容
    if (NULL == root_node)
        return false;

    //记录保存xml的文件名
    dbFileName = filename;

    return true;
}

bool CXMLFile::newXMLDoc(string filename, string rootname)
{
    /*
     * Creates a new document, a node and set it as a root node
     */
    pxmlDoc = xmlNewDoc(BAD_CAST "1.0");
    root_node = xmlNewNode(NULL, (const xmlChar *)rootname.c_str());
    xmlDocSetRootElement(pxmlDoc, root_node);

    if(!filename.empty())
    {
        /*
        * Dumping document to stdio or file
        */
        if(xmlSaveFormatFileEnc(filename.c_str(), pxmlDoc, "UTF-8", 1) < 0)
            return false;

        //记录保存xml的文件名
        dbFileName = filename;
    }else{
        dbFileName = NULL_STR;
    }
    return true;
}

xmlNodePtr CXMLFile::addNode(xmlNodePtr fatherNode, string name)
{
    if(!fatherNode || name.empty())
        return NULL;

    xmlNodePtr node = xmlNewChild(fatherNode, NULL, BAD_CAST name.c_str(), NULL);

    return node;
}


bool CXMLFile::addTab(string tab)
{
    //查找重名记录
    if(findTab(tab))
        return false;

    if(addNode(root_node,tab))
        return true;
    else
        return false;
}

bool CXMLFile::delTab(string tab)
{
    xmlNodePtr node = findTab(tab);
    //查找记录
    if(!node)
        return false;

    xmlUnlinkNode(node);
    xmlFreeNode(node);

    return true;
}

bool CXMLFile::editTabName(string tab_old, string tab_new)
{
    xmlNodePtr node;
    //查找记录
    if(!(node = findTab(tab_old)))
        return false;

    xmlNodeSetName(node,BAD_CAST tab_new.c_str());

    return true;
}

bool CXMLFile::addItem(string tab, string item, string content)
{
    xmlNodePtr tabnode = findTab(tab);
    if(!tabnode)
        return false;

    //查找重名记录
    xmlNodePtr node = findItemInTab(tab,item);
    if(node)
        return false;

    node = addNode(tabnode,item);
    xmlNodeSetContent(node,BAD_CAST content.c_str());

    return true;
}

bool CXMLFile::delItem(string tab, string item)
{
    xmlNodePtr node;
    //查找记录
    if(!(node = findItemInTab(tab,item)))
        return false;

    xmlUnlinkNode(node);
    xmlFreeNode(node);

    return true;
}

bool CXMLFile::editItemName(string tab, string item_old, string item_new)
{
    xmlNodePtr node;
    //查找记录
    if(!(node = findItemInTab(tab,item_old)))
        return false;

    xmlNodeSetName(node,BAD_CAST item_new.c_str());

    return true;
}

bool CXMLFile::editItemContent(string tab, string item, string content_new)
{
    xmlNodePtr node;
    //查找记录
    if(!(node = findItemInTab(tab,item)))
        return false;

    xmlNodeSetContent(node,BAD_CAST content_new.c_str());

    return true;
}

xmlNodePtr CXMLFile::findNode(xmlNodePtr fatherNode, string name)
{
    xmlNodePtr node;

    if(!fatherNode || name.empty())
        return NULL;

    for (node=fatherNode->children; node; node=node->next)
    {
        if (node->type != XML_ELEMENT_NODE)
            continue;

        if (0==xmlStrcmp(node->name, BAD_CAST name.c_str()))
            return node;
    }
    return NULL;
}


xmlNodePtr CXMLFile::findTab(string tab)
{
    if(tab.empty())
        return NULL;
    return findNode(root_node,tab);
}

xmlNodePtr CXMLFile::findItemInTab(string tab, string item)
{
    xmlNodePtr tabnode;
    if(!(tabnode=findTab(tab)))
        return NULL;

    if(item.empty())
        return NULL;

    return findNode(tabnode,item);
}

xmlNodePtr CXMLFile::findNodeNext(xmlNodePtr startNode, xmlNodePtr* ipc)
{
    if(!startNode || !ipc)
        return NULL;

    xmlNodePtr node = *ipc;

    for (node = startNode; node; node=node->next)
    {
        if (node->type != XML_ELEMENT_NODE)
            continue;
        
        *ipc = node;
        return node;
    }
    *ipc = NULL;
    return NULL;
}


string CXMLFile::getTabFirst()
{
    if(findNodeNext(root_node->children,&tab_ipc))
        return getNodeName(tab_ipc);
    else
        return NULL_STR;
}

string CXMLFile::getTabNext()
{
    if(!tab_ipc)
        return NULL_STR;

    if(findNodeNext(tab_ipc->next,&tab_ipc))
        return getNodeName(tab_ipc);
    else
        return NULL_STR;
}

string CXMLFile::getTabProp(string prop)
{
    if(!tab_ipc)
    {
        return NULL_STR;
    }

    xmlChar* szAttr = xmlGetProp(tab_ipc, BAD_CAST prop.c_str());
    
    if(szAttr)
    {
        string str = (char *)szAttr;
        xmlFree(szAttr);
        return str;
    }
    return NULL_STR;
}

CString CXMLFile::getTabPropEx(string prop)
{
    CString szStr;
    string str = getTabProp(prop);
    TCHAR  *pszStr = new TCHAR[str.length()+1];
    MultiByteToWideChar(CP_ACP,0,str.c_str(),-1,pszStr,str.length()+1);
    szStr = pszStr;
    delete []pszStr;
    return szStr;
}

string CXMLFile::getItemInTabFirst(string tab)
{
    xmlNodePtr tabnode;
    if(!(tabnode=findTab(tab)))
        return NULL_STR;

    if(findNodeNext(tabnode->children,&item_ipc))
        return getNodeName(item_ipc);
    else
        return NULL_STR;
}

string CXMLFile::getItemInTabNext()
{
    if(!item_ipc)
        return NULL_STR;

    if(findNodeNext(item_ipc->next,&item_ipc))
        return getNodeName(item_ipc);
    else
        return NULL_STR;
}

string CXMLFile::getItemProp(string prop)
{
    if(!item_ipc)
    {
        return NULL_STR;
    }

    xmlChar* szAttr = xmlGetProp(item_ipc, BAD_CAST prop.c_str());
    
    if(szAttr)
    {
        string str = (char *)szAttr;
        xmlFree(szAttr);
        return str;
    }
    return NULL_STR;
}

CString CXMLFile::getItemPropEx(string prop)
{
    CString szStr;
    string str = getItemProp(prop);
    TCHAR  *pszStr = new TCHAR[str.length()+1];
    MultiByteToWideChar(CP_ACP,0,str.c_str(),-1,pszStr,str.length()+1);
    szStr = pszStr;
    delete []pszStr;
    return szStr;
}

string CXMLFile::getContent(string tab, string item)
{
    xmlNodePtr node = findItemInTab(tab,item);
    return getNodeContent(node);
}

string CXMLFile::getNodeContent(xmlNodePtr node)
{
    if(!node)
        return NULL_STR;

    char* str = (char*)xmlNodeGetContent(node);
    if(str)
        return str;
    else
        return NULL_STR;
}

CString CXMLFile::getContentEx(string tab,string item)
{
    string str = getContent(tab, item);
    CString szStr;
    TCHAR  *pszStr = new TCHAR[str.length()+1];
    MultiByteToWideChar(CP_ACP,0,str.c_str(),-1,pszStr,str.length()+1);
    szStr = pszStr;
    delete []pszStr;
    return szStr;
}

bool CXMLFile::saveToFile(string filename, bool blankpad)
{
    int format = blankpad?1:0;

    if(filename.empty())
        filename = dbFileName;

    if(filename.empty())
        return false;

    if(xmlSaveFormatFileEnc(filename.c_str(), pxmlDoc, "UTF-8", format) < 0)
        return false;

    dbFileName = filename;
    return true;
}

int CXMLFile::getItemsCount(xmlNodePtr fatherNode)
{
    xmlNodePtr node;
    int count = 0;

    if(!fatherNode)
        return -1;

    for (node=fatherNode->children; node; node=node->next)
    {
        if (node->type != XML_ELEMENT_NODE)
            continue;
        count++;
    }
    return count;
}

int CXMLFile::getTabsCount()
{
    return getItemsCount(root_node);
}

int CXMLFile::getItemsCountInTab(string tab)
{
    xmlNodePtr tabnode;
    if(!(tabnode=findTab(tab)))
        return -1;

    return getItemsCount(tabnode);
}

xmlNodePtr CXMLFile::findNode(xmlNodePtr fatherNode, int index)
{
    xmlNodePtr node;
    int i = -1;

    if(!fatherNode)
        return NULL;

    for (node=fatherNode->children; node; node=node->next)
    {
        if (node->type != XML_ELEMENT_NODE)
            continue;
        i++;
        if(i==index)
            break;
    }

    if(node && i==index)
        return node;
    else
        return NULL;

}

string CXMLFile::getItemInTab(string tab, int index)
{
    xmlNodePtr tabnode;
    if(!(tabnode=findTab(tab)))
        return NULL_STR;

    xmlNodePtr node = findNode(tabnode,index);
    return getNodeName(node);
}

string CXMLFile::getTab(int index)
{
    xmlNodePtr node = findNode(root_node,index);
    return getNodeName(node);
}

string CXMLFile::getNodeName(xmlNodePtr node)
{
    if(!node)
        return NULL_STR;

    char* str = (char*)node->name;
    if(str)
        return str;
    else
        return NULL_STR;
}

bool CXMLFile::openXMLDoc(CString filename)
{
#ifdef _UNICODE
    char *chr=new char[filename.GetLength()+1];
    WideCharToMultiByte(CP_ACP,0,filename.GetBuffer(),-1,chr,filename.GetLength()+1,NULL,NULL);
    string str=chr;
    delete[] chr;
#else
    string str=filename.GetBuffer();
#endif
    return openXMLDoc(str);
}

bool CXMLFile::newXMLDoc(CString filename)
{
#ifdef _UNICODE
    char *chr=new char[filename.GetLength()+1];
    WideCharToMultiByte(CP_ACP,0,filename.GetBuffer(),-1,chr,filename.GetLength()+1,NULL,NULL);
    string str=chr;
    delete[] chr;
#else
    string str=filename.GetBuffer();
#endif
    return newXMLDoc(str);
}

bool CXMLFile::saveToFile(CString filename, bool blankpad)
{
#ifdef _UNICODE
    char *chr=new char[filename.GetLength()+1];
    WideCharToMultiByte(CP_ACP,0,filename.GetBuffer(),-1,chr,filename.GetLength()+1,NULL,NULL);
    string str=chr;
    delete[] chr;
#else
    string str=filename.GetBuffer();
#endif
    return saveToFile(str,blankpad);
}
