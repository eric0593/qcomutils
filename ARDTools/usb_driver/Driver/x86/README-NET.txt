Qualcomm USB Network Driver Package 1.0.7.6 for Windows XP/Vista/Win 7

This readme covers important information regarding the following drivers for
Windows XP/Vista/Win 7:
  -Qualcomm USB Network Driver (qcusbnet.sys) 1.0.7.6 (NDIS5.1 miniport driver)
  -Qualcomm USB Serial Driver (qcusbser.sys) 2.0.9.3

Table of Contents

1. Installation Notes
2. Major Features
3. What's New in This Release
4. Known issues
5. Build Information

----------------------------------------------------------------------

1. INSTALLATION NOTES

Please refer to Qualcomm document 80-VB717-1 Rev B for detailed instructions on
driver installation.

----------------------------------------------------------------------

2. MAJOR FEATURES

1) The driver is designed to be reentrant, which allows multiple device
   connections on Windows XP/Vista.
 
2) The driver is designed to be running safe with Intel Hyper-Threading processor
   and multi-processor systems with Windows XP.

3) Support Plug&Play, and power management (system standby and hibernation). USB
   selective suspension will be supported in the future.

4) Support multiple control clients through QUALCOMM MSM Interface (QMI).

----------------------------------------------------------------------

3. What's New in This Release

1) Fixed the BSOD issue triggered by run-away execution of the work item
   under arbitary system context. (1.0.3.0)

2) Fixed the BSOD issue triggered by a race condition during IRP completion.
   (1.0.3.0)

3) Provided a way to work around a BSOD issue on Windows XP when system came
   out of standby with NIC's power management disabled. (1.0.3.0)

4) Fixed a race condition under which outstanding I/O might not be cleaned
   up when the network adapter was disabled. (1.0.3.1)

5) Added IP-only mode support. (1.0.3.2)

6) Added additional remove lock to avoid stray-away QMI operations. (1.0.3.2)

7) Added a worker thread to handle QMI operations to avoid deadlock and increase
   efficiency. (1.0.3.2)

8) Added statistics summary to debug output to aid debugging. (1.0.3.2)

9) Changes have been made so that the driver does not request QMI statistics
   from the device, the driver maintains its own statistics instead. (1.0.3.2)

10) Enhanced error handling for device removal. (1.0.3.2)

11) Added QCMP_TEST_MODE to support development/testing. The feature would be
    enabled for engineering builds only. (1.0.3.2)

12) Added support for PID 900A. (1.0.3.2)

13) Changed QMI max receive size from 1K to 4K. (1.0.3.3) 

14) Changes were made so that remote wakeup is cleared for all non-D2 power states.
   to avoid system sleep failure when the hub controller is armed for remote wakeup.
   (1.0.3.4)

15) Changes were made to avoid double completion of a system power IRP when a series
   of non-consistent system power IRPs (e.g query-s3 followed by set-s0 and followed
   by set-s3) are dispatched to the driver. (1.0.3.4)

16) Added IOCTL support for retrieving device ESN/IMEI/MEID. (1.0.3.5)

17) Added support for PID 900B. (1.0.3.6)

18) Reset pipe status when threads resume. (1.0.3.6)

19) Fixed a race condition when multiple QMI clients terminate. (1.0.3.7)

20) Added support for PID 900C. (1.0.3.7)

21) Changes were made to support larger number of read buffers. (1.0.3.7)

22) Changes were made to synchronize selective suspension and
    remote wakeup. (1.0.3.7)

23) Fixed the problem that a read buffer could be mistakenly recycled. (1.0.3.7)

24) Version 1.0.3.8/1.0.3.9 contains changes in serial/modem driver only.

25) Corrected mask value for TLV 0x10 in QMI message WDS_GET_RUNTIME_SETTINGS_REQ
    so that IP address can be obtained in IP mode. (1.0.4.0)

26) Added support for PID 900D. (1.0.4.0)

27) Made QMI_CTL service version available to QMI clients. (1.0.4.1)

28) Driver internal states at USB layer are changed earlier once the surprise-removal
    IRP is received by the dispatch routine. This prevents the USB dispatch routine
    from falling into an infinite loop. (1.0.4.1)

29) Fixed the bug that could double free the memory block for USB config descriptor.
    (1.0.4.1)

30) Updated with new method to synchronize selective suspend and remote wakeup.
    (1.0.4.2)

31) Added IPV6 support for QOS. (1.0.4.3)

32) Always try to enter D0 when idle notification IRP is completed. (1.0.4.3)

33) Added new features for internal testing only. (1.0.4.4)

34) Made changes so that QoS-related IOs are cancelled on receiving surprise-removal
    event to avoid device removal hang. (1.0.4.4)

35) Added support to disable QoS from registry. (1.0.4.4)

36) Corrected channel-rate TLV of WDS_GET_CURRENT_CHANNEL_RATE_RESP. (1.0.4.4)

37) Refined support for QoS over IPV6. (1.0.4.4)

38) Made changes so that the USB layer is cleaned up before freeing miniport buffers
    to avoid possible memory access fault. (1.0.4.5)

39) Added support for PID 900F, 9021 and 9022. (1.0.4.6)

40) Added support for QoS precedence filtering. (1.0.4.6)

41) Made changes so that QoS settings are cleared when data call is disconnected.(1.0.4.6)

42) Removed images for IA64 processor architecture. (1.0.4.7)

43) Feature cleanup. (1.0.4.7)

44) Added registry support (QCDevDisableIPMode(DWORD) under software key) to disable
    IP mode. (1.0.4.8)

45) Added support for PID 9010 and 9011. (1.0.4.8)

46) Added dual-IP support. (1.0.4.8)

47) Modified the driver to provide any QMI service version information to applications via
    pre-defined IOCTL. (1.0.4.8)

48) Fixed the bug which could cause memory access fault after a QoS filter was modified.
    (1.0.4.9)

49) Modified driver to enable QoS default flow when data connection is made. (1.0.4.9)

50) Added QoS general port filtering for TCP/UDP (1.0.5.0)

51) Added QoS port filtering for IPV6 (1.0.5.0)

52) Added QoS filtering for fragmented IP Packets. (1.0.5.0)

53) Enhanced USB error detection when the device stack's power state is not initialized
    by OS, which avoids potential driver hang. (1.0.5.0)

54) Added uplink TLP support which can be enabled/disabled from registry. (1.0.5.0)

55) Made changes so that IPV6 traffic is filtered in an IPV4 connection when working
    with targets not supporting dual IP. (1.0.5.0)

56) Added support for PID 9025 & 9026 (1.0.5.0)

57) Added support for PID 9028, 9029, 902C, 902D, 902E, 902F, and 9030. (1.0.5.1)

58) Added support for USB compound device to handle multiple physical device instances.
    (1.0.5.1)

59) Added QMI support for uplink TLP. (1.0.5.2)

60) Added support for PID 9031 and 9032. (1.0.5.2)

61) Added internal QMI client for IPV6 to handle dual IP. (net 1.0.5.3)

62) Added code to handle IP netxt-protocol code 0xFD for QoS TCP/UDP port filtering.
    (net 1.0.5.3)

63) Added support for PID 9033. (net 1.0.5.4, ser 2.0.7.9)

64) Eliminated the timeout in driver initialization when device does not provide
    MSISDN. (net 1.0.5.5)

65) Added new synchronization method that data format can be set from the device to
    the host. (net 1.0.5.5)

66) Fixed the problem that driver would hang due to possible stray-away TX packets
    in TLP mode. (net 1.0.5.5)

67) Fixed the bug which could result in a same read IRP to be enqueued twice to the
    completion queue, causing system crash. (ser 2.0.8.0)

68) Fixed the bug that caused the IP fragment Flags to be incorrectly checked.
    (net 1.0.5.6)

69) Fixed the bug that caused the IPV6 transport protocol to be incorrectly checked.
   (net 1.0.5.6)

70) Added support for PID 9034. (net 1.0.5.6, ser 2.0.8.1)

71) Added new TX buffering scheme to enhance host-to-device throughput when QoS is
    disabled. (net 1.0.5.6)

72) Increased QMI initialization timeout/retry period to 120s. (net 1.0.5.7)

73) Added support for PID 9035 and 9036. (net 1.0.5.7)

74) Provided a solution to a BSOD issue caused by the race condition among IRP completion,
    cancellation and timeout manipulation. (ser 2.0.8.2)

75) Made changes to correctly arm the reconfig timer in dual-IP mode. (net 1.0.5.8)

76) Made changes to drop oversized pkts. (net 1.0.5.8)

77) Made changes to correctly detect if read cancel routine is re-set during normal
    IRP completion process to avoid driver hang. (ser 2.0.8.3)

78) Featurized the changes to the read operation in serial/modem driver (changes in
    version 2.0.8.2/2.0.8.3 (release 1057/1058)) in order to valid the changes fully
    before making them to the production build. (ser 2.0.8.4)

79) Made changes so that the modem/serial driver does not try to process an IRP if the
    internal data store is empty, which avoids losing track of the IRP. (ser 2.0.8.4)

80) Added support for PID 9037 and 9038 (net 1.0.5.9, ser 2.0.8.4).

81) Corrected build script for making free builds. (ser 2.0.8.4)

82) Added new TLV in set-data-format-rsp for QoS setting. (net 1.0.5.9)

83) Fixed queue access fault during miniport removal when miniport is partially
    initialized. (net 1.0.6.0)

84) Made QMI initialization reentrant. (net 1.0.6.1)

85) Made changes so that a NDIS TX packet is not denied when driver runs out of TX 
    buffers. This helps to avoid premature termination of apps such as iperf which has
    very low error tolerance. (net 1.0.6.1)

86) Implemented queue-based RX to handle out-of-order IRP cancellation from the lower
    layer driver. (net 1.0.6.1)

87) Made changes to deny system-control (WMI) IRP when device is in removal state to
    avoid dead lock. (ser 2.0.8.5)

88) Added support for PID 901B, 903A, 903B, 903C, 903D, and 903E.
    (net 1.0.6.1, ser 2.0.8.5)

89) Refined device names for PID 9031, 9032, 9035, 9036, 9037, and 9038. (ser 2.0.8.5)

90) Reduced TX throttle for QoS. (net 1.0.6.2)

91) Removed get-runtime-settings request for IPV6 connection to avoid confusion.
    (net 1.0.6.2)

92) Added support for PID 903F and 9040. (ser 2.0.8.6)

93) Added QCDevDisableQoS in registry during installation so that QoS is disabled by
    default. (net 1.0.6.3)

94) Fixed a problem which could cause IPV6-only connection to fail. (net 1.0.6.4)

95) Added support to accommodate UE's apps-centric design. (net 1.0.6.5)

96) Increased priority for downlink processing. (net 1.0.6.5)

97) Provided solution to possible IPV4 connection loss in iRAT testing. (net 1.0.6.5)

98) Added support for PID 9041 and 9042. (ser 2.0.8.7)

99) Fixed the issue of possible enumeration failure with delayed QMI initialization.
    (net 1.0.6.6)

100) Implemented a solution to dual-IP call failure when the two calls are made more
     than 12 seconds apart on Windows Vista and later versions. (net 1.0.6.6)

101) Added QoS filtering support for ICMPV6. (net 1.0.6.6)

102) Added cleanup for miniport TxPendingQueue to avoid driver hang when device is being
     debugged and disconnected later. (net 1.0.6.6)

103) Added feature to enable/disable TLP from Windows registry by using qdcfg.exe to 
     create and set registry entry QCMPEnableTLP (value 0/1). (net 1.0.6.6)

104) Updated driver configuration tool qdcfg.exe to version 3.5. (net 1.0.6.6)

105) Added QMI CTL_SYNC to support QMI reset. (net 1.0.6.7)

106) Fixed the bug that an ICMP packet may not be categorized properly with a QoS filter
     containing the port information of TCP/UDP type (0xFD). (net 1.0.6.7)

107) Made TLP initialization reentrant to accommodate multiple initialization attempts.
     (net 1.0.6.7)

108) Added more validation on the USB notifications to detect corrupted data. (net 1.0.6.7)

109) Added support for PID 9043, 9044 and 9045. (ser 2.0.8.8)

110) Added the NCM Data aggregation support for both DL and UL. (net 1.0.6.8)

111) Added the WDS Admin client support for selecting different data formats. (net 1.0.6.8)

112) Corrected compilation errors when NCM data aggregation flags are not defined. (net 1.0.6.9)

113) Added support for PID 9046 and 9047. (net 1.0.7.0, ser 2.0.8.9)

114) Incrased USB retries to accommodate up to 30 seconds of continuous USB-level error
     conditions to improve recovery probability. (ser 2.0.8.9)

115) Added support for static MAC address assignment for a network adapter. (net 1.0.7.0)

116) Added support for QMI fuzzing test. (net 1.0.7.0)

117) Made changes to clean up OID pending queue to avoid driver hang during device
     debugging. (net 1.0.7.0)

118) Refined handling of QMI_CTL_SYNC logic. (net 1.0.7.0)

119) Updated configuration tool qdcfg.exe to version 3.6. (net 1.0.7.0)

120) Added support for PID 9048. (ser 2.0.9.0, net 1.0.7.1)

121) Added DL control feature for internal debugging. (net 1.0.7.1)

122) Increased number of I/O buffers to accommodate the burty I/O characteristics of certain
     targets. (net 1.0.7.1)

123) Refined QMI initialization to block external clients until QMI is fully initialized.
     (net 1.0.7.1)

124) Fixed the bug which could cause queue corruption during device removal.(ser 2.0.9.1)

125) Added support for PID 904B and 904C.(ser 2.0.9.2, net 1.0.7.4)

126) Corrected defination for PID F005.(ser 2.0.9.2)

127) Added registry settings so that USB serial number is ignored for specific PIDs.(ser 2.0.9.2)

128) comment out options to read multi read/write registry values.(ser 2.0.9.2)

129) Added "QCIgnoreErrors" registry item and also added a 
     check to ignore STATUS_INVALID_PARAMETER for QMI ctl write.(net 1.0.7.4)

130) comment out options to read multi read/write registry values.(net 1.0.7.4)

131) Made changes to dynamically determine number of TX buffers based on 
     data aggregation state (on/off)(net 1.0.7.4)

132) Return either DeviceDesc or FriendlyName for OID_GEN_VENDOR_DESCRIPTION.(net 1.0.7.4)

133) Added enabling TLP DL settings with both WDS_ADMIN and CTL QMI.(net 1.0.7.5)

134) Added SelectiveSuspendIdleTime configuration in Milliseconds/Seconds.(net 1.0.7.5)

135) Added SelectiveSuspendIdleTime configuration in Milliseconds.(ser 2.0.9.3)

136) Reset client ID before opening WDS client.(net 1.0.7.5) 

138) Stop L2 thread creation and complete IRP_MJ_CREATE/WRITE when device 
     is in REMOVED0 state.(serial 2.0.9.3) 

139) Added support for configuring MTU Size. (net 1.0.7.5) 

140) Made changes to handle out-of-order removal event. (ser 2.0.9.3)

141) Added transmit timer for Transmit handler of aggregate packets. (net 1.0.7.6)
----------------------------------------------------------------------

4. KNOWN ISSUES:

   It's been observed that the Qualcomm USB driver could trigger bugs in older
   versions of McAfee mini-firewall driver mvstd5x.sys. McAfee has suggested
   that users obtain McAfee VirusScan patch 14 or later patches for bug fixes.

   Compilation flag QCUSB_TARGET_XP should NOT be defined with this version due to
   unsolved issues with XP-specific function calls.

   McAfee Intermediate Filter driver may cause IPV6 connection failure. If such
   a failure happens, a user may either need to remove the McAfee driver from the
   device stack or contact McAfee for solution.

   Driver verifier (verfiler.exe) may cause throughput degradation.

----------------------------------------------------------------------

5. BUILD INFORMATION:

   The following compilation flags are defined for customers to build the driver
   to debug various issues related to host and device, they are not recommended
   for other purposes.
      ENABLE_LOGGING
      QCUSB_ENABLE_LOG_REC
      QCUSB_DBGPRINT
      QCUSB_DBGPRINT2
      QCUSB_TARGET_XP

   IMPORTANT!!! To achieve best security and performance, all of the above features
   should be turned off for commercial distributions. The free build binaries are
   recommended for commercial distributions.

----------------------------------------------------------------------
Copyright (c) 2012 QUALCOMM Incorporated
All rights reserved.
