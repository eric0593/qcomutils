#pragma once
#include "afx.h"

#define HEX_DATA_SIZE_MAX   0xFF
#define HEX_SEG_SIZE_MAX    0x10000 // 64K
#define HEX_RECORD_HEADER   ':'
#define HEX_RECORD_MIN_LEN  11
#define HEX_RECORD_MAX_LEN  (HEX_RECORD_MIN_LEN+HEX_DATA_SIZE_MAX)

enum
{
    HEX_TYPE_DATA           = 0x00, // Data Record 
    HEX_TYPE_EOF            = 0x01, // End of File Record 
    HEX_TYPE_EXT_SADDR      = 0x02, // Extended Segment Address Record
    HEX_TYPE_START_SADDR    = 0x03, // Start Segment Address Record 
    HEX_TYPE_EXT_LADDR      = 0x04, // Extended Linear Address Record 
    HEX_TYPE_START_LADDR    = 0x05, // Start Linear Address Record 
    HEX_TYPE_MAX
};

typedef struct{
    BYTE bLen;
    WORD wAddr;
    BYTE bType;
    union{
        BYTE    bData[HEX_DATA_SIZE_MAX];
        WORD    wExtSAddr;
        DWORD   wStartSAddr;
        WORD    wExtLAddr;
        DWORD   wStartLAddr;
    }d;
}HexRecord;

class CHexFile : public CStdioFile
{
public:
    CHexFile(void);
	CHexFile(LPCTSTR lpszFileName);
    ~CHexFile(void);
    
    BOOL        Open(LPCTSTR lpszFileName);
    BOOL        EnumSegmentInit(void);
    BOOL        EnumNextSegment(void);
    BOOL        IsValid(void)                               {return m_bValid;};
    void        GetSegmentAddr(DWORD *pAddr, BYTE *pType)   {if(pAddr) *pAddr = m_dwExtAddr; if(pType) *pType = m_dwExtType;};
    BYTE       *GetSegmentBuf(void)                         {return m_pBuf;};
    DWORD       GetSegmentSize(void)                        {return m_dwSegmentSize;};
    DWORD       GetSegmentStartAddr(void)                   {return m_dwSegmentStartAddr>=HEX_SEG_SIZE_MAX?0:m_dwSegmentStartAddr;};
    DWORD       GetStartAddr(void)                          {return m_dwStartAddr;};
    
private:
    void        InitParam(void);
    BOOL        DecodeRecord(HexRecord *pRecord, TCHAR *pData, WORD wLen);
    BYTE        CharsToHex(TCHAR H, TCHAR L);
    
private:
    BYTE        m_pBuf[HEX_SEG_SIZE_MAX];
    DWORD       m_dwStartAddr;
    WORD        m_dwExtAddr;
    BYTE        m_dwExtType;
    DWORD       m_dwSegmentSize;        // Decode之后的长度
    DWORD       m_dwSegmentStartAddr;   // Segment
    HexRecord   m_HexExtSegInfo;
    BOOL        m_bSegmentEnumInit;
    BOOL        m_bValid;
};
