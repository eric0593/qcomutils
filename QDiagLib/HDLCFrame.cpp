/*=================================================================================================

  FILE : HDLCFRAME.C

  SERVICES: Declaration of an MFC wrapper class for Async-HDLC frame's encapsulation and getting
  origin data from a Async-HDLC frame

  GENERAL DESCRIPTION:
  
  PUBLIC CLASSES AND STATIC FUNCTIONS:
  CHDLCFrame;
    
  INITIALIZATION AND SEQUENCING REQUIREMENTS:
  
====================================================================================================*/


/*====================================================================================================

                                       ==MODIFY HISTOY FOR FILE==

  when         who     what, where, why
  ----------   ---     ----------------------------------------------------------
  12/23/2003   tyz      create
  
=====================================================================================================*/
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/*===========================================================================

                  INCLUDE FILES FOR MODULE
  
===========================================================================*/
#include "stdafx.h"
#include "HDLCFrame.h"
#include "crc.c"

/*===========================================================================

                  DEFINITIONS AND CONSTANTS
  
===========================================================================*/



/*===========================================================================

                     LOCAL/STATIC DATA

===========================================================================*/


/*==========================================================================

                     MEMBER FUNCTION DEFINATION
            
==========================================================================*/
CHDLCFrame::CHDLCFrame(char escChar, char flagChar, char escCompl)
{
   m_EscChar = escChar;
   m_FlagChar = flagChar;
   m_EscComplement = escCompl;
}

CHDLCFrame::~CHDLCFrame()
{
   
}

int CHDLCFrame::CRCEncode(BYTE* pData, WORD length)
{
   WORD crc = crc_16_l_calc (pData, length * 8);
   m_bOriginData[length] = crc & 0x00ff;
   m_bOriginData[length + 1] = (crc>>8) & 0x00ff;
   length += 2;
#if 0
   {
        int pos,count;
        crc = CRC_16_L_STEP_SEED;
        pos = 0;
        while (pos < length)
        {
          count = length - pos;
          if (count > 128)
            count = 128;
          crc = crc_16_l_step (crc, m_bOriginData + pos, count); /*lint !e734 */
          pos += count;
        }

        if (crc != CRC_16_L_OK)
        {
            TRACE("CRC Error\n");
        }
   }
#endif
   return length;
}

BOOL CHDLCFrame::CRCCheck(BYTE* pData, WORD length)
{
    WORD crc = crc_16_l_calc (pData, length * 8);
    if (crc != CRC_16_L_OK)
    {
        return FALSE;
    }
    return TRUE;
}

DWORD CHDLCFrame::Encode(char* pOriginData, WORD length, BOOL isHeadEnc, BOOL bHDLCEncode)
{
   DWORD wFrDataIndex = 0, wOrDataIndex = 0;
   
   m_bHeadEnc = isHeadEnc;
   memcpy(m_bOriginData,pOriginData,length);
   length = CRCEncode((BYTE*) pOriginData, length);     //计算CRC码

   if(isHeadEnc == TRUE && bHDLCEncode == TRUE)
   {
      m_bHDLCFrame[wFrDataIndex++] = m_FlagChar;
   }

   if(bHDLCEncode == TRUE)
   {
       while(wOrDataIndex < length)
       {//帧封装
          if(m_bOriginData[wOrDataIndex] == m_EscChar||
             m_bOriginData[wOrDataIndex] == m_FlagChar)
          {
             m_bHDLCFrame[wFrDataIndex++] = m_EscChar;
             m_bHDLCFrame[wFrDataIndex++] = m_bOriginData[wOrDataIndex++]^m_EscComplement;
          }
          else
          {
             m_bHDLCFrame[wFrDataIndex++] = m_bOriginData[wOrDataIndex++];
          }
       }
   }
   else
   {
       while(wOrDataIndex < length)
       {
          m_bHDLCFrame[wFrDataIndex++] = m_bOriginData[wOrDataIndex++];
       }
   }
   
   if(bHDLCEncode == TRUE)
   {
      m_bHDLCFrame[wFrDataIndex++] = m_FlagChar;
   }

   m_dwHDLCFrameLen = wFrDataIndex;
   m_dwOriginDatalen = length;

   return m_dwHDLCFrameLen;
}

BOOL CHDLCFrame::Decode(char* pHDLCFrame, WORD length)
{
   WORD wFrDataIndex = 0, wOrDataIndex = 0;
   
   if(length < 4)
   {
      return FALSE;
   }

   m_bHeadEnc = FALSE;
   while(wFrDataIndex < length)
   {//帧还原
      if(*(pHDLCFrame + wFrDataIndex) == m_FlagChar)
      {  //遇到标志，退出循环。
         m_bHDLCFrame[wFrDataIndex] = *(pHDLCFrame + wFrDataIndex);
         if(wFrDataIndex == 0)
         {//起始标志
             m_bHeadEnc = TRUE;
         }
         else
         {//结束标志
            break;
         }
      }
      else if(*(pHDLCFrame + wFrDataIndex) == m_EscChar)
      { 
         m_bHDLCFrame[wFrDataIndex] = *(pHDLCFrame + wFrDataIndex);
         wFrDataIndex++;
         m_bHDLCFrame[wFrDataIndex] = *(pHDLCFrame + wFrDataIndex);
         m_bOriginData[wOrDataIndex++] = (*(pHDLCFrame + wFrDataIndex)) ^ m_EscComplement;
      }
      else
      {
         m_bHDLCFrame[wFrDataIndex] = *(pHDLCFrame + wFrDataIndex);
         m_bOriginData[wOrDataIndex++] = *(pHDLCFrame + wFrDataIndex);
      }
      wFrDataIndex++;
   }
   
   if(!CRCCheck(GetOriginData(), wOrDataIndex))
   {
      // CRC校验错误不清除长度
      return FALSE;
   }

   m_dwHDLCFrameLen = length;
   //清除CRC数据。
   m_bOriginData[wOrDataIndex - 2] = 0;   
   m_bOriginData[wOrDataIndex - 1] = 0;
   m_dwOriginDatalen = wOrDataIndex - 2;       //减去CRC码２byte
   return TRUE;
}