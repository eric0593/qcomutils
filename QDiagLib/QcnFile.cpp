#include "StdAfx.h"
#include "QcnFile.h"
#include "Log.h"

CQcnFile::CQcnFile(void)
{
    InitParam();
}

CQcnFile::~CQcnFile(void)
{
    Close();
}

void CQcnFile::InitParam(void)
{
    m_bValid                = FALSE;
    m_nCount                = 0;
    m_bReadOnly             = FALSE;
    m_FileVersion.wMajor    = 2;
    m_FileVersion.wMinor    = 0;
    m_FileVersion.wRevision = 0;
    m_pRootStorage          = NULL;
    m_pModelStorage         = NULL;
    m_pDefaultStorage       = NULL;
    m_pNVItemsStorage       = NULL;
    m_pNVItemsStream        = NULL;
}

BOOL CQcnFile::Open(LPCTSTR pDocName, BOOL bCreate)
{
    HRESULT hr;
    IEnumSTATSTG *spEnum;
    STATSTG stat;
    DWORD dwMode;

    if(pDocName==NULL || _tcslen(pDocName) == 0)
    {
        return FALSE;
    }
    
    if(m_bValid)
    {
        Close();
    }

    m_bValid = FALSE;
    m_bReadOnly = !bCreate;
    
    if(m_bReadOnly)
    {
        dwMode = STGM_DIRECT | STGM_READ | STGM_SHARE_EXCLUSIVE;
    }
    else
    {
        dwMode = STGM_DIRECT | STGM_READWRITE | STGM_SHARE_EXCLUSIVE;
    }

    hr = StgOpenStorageEx(pDocName, dwMode, STGFMT_STORAGE, 0, NULL, 0, IID_IStorage, (void **)&m_pRootStorage); 
    if (!SUCCEEDED(hr))
    {
        if(!m_bReadOnly)
        {
            hr = StgCreateStorageEx(pDocName, dwMode, STGFMT_STORAGE, 0, NULL, 0, IID_IStorage, (void **)&m_pRootStorage); 
            if (!SUCCEEDED(hr))
            {
                return m_bValid;
            }
        }
        else
        {
            return m_bValid;
        }
    }
    
    hr = m_pRootStorage->EnumElements(0, NULL, 0, &spEnum); 
    if(SUCCEEDED (hr)) 
    {
        hr = -1;;
        while (spEnum->Next(1, &stat, NULL) == S_OK) 
        { 
            if(stat.type == STGTY_STORAGE)
            {
                hr = m_pRootStorage->OpenStorage(stat.pwcsName, NULL, dwMode, NULL, 0, &m_pModelStorage); 
                CoTaskMemFree(stat.pwcsName); 
                break;
            }
            CoTaskMemFree(stat.pwcsName); 
        }
        spEnum->Release();
    }
    
    if (!SUCCEEDED(hr))
    {
        if(!m_bReadOnly)
        {
            hr = m_pRootStorage->CreateStorage(QCN_STORAGE_MODEL_DEFAULT, dwMode, 0, 0, &m_pModelStorage);
            if (!SUCCEEDED(hr))
            {
                return m_bValid;
            }
        }
        else
        {
            return m_bValid;
        }
    }
    
    hr = m_pModelStorage->OpenStorage(QCN_STORAGE_DEFAULT, NULL, dwMode, NULL, 0, &m_pDefaultStorage); 
    if (!SUCCEEDED(hr))
    {
        if(!m_bReadOnly)
        {
            hr = m_pModelStorage->CreateStorage(QCN_STORAGE_DEFAULT, dwMode, 0, 0, &m_pDefaultStorage);
            if (!SUCCEEDED(hr))
            {
                return m_bValid;
            }
        }
        else
        {
            return m_bValid;
        }
    }

    hr = m_pDefaultStorage->OpenStorage(QCN_STORAGE_NV_NUMBERED_ITEMS, NULL, dwMode, NULL, 0, &m_pNVItemsStorage); 
    if (!SUCCEEDED(hr))
    {
        if(!m_bReadOnly)
        {
            hr = m_pDefaultStorage->CreateStorage(QCN_STORAGE_NV_NUMBERED_ITEMS, dwMode, 0, 0, &m_pNVItemsStorage);
            if (!SUCCEEDED(hr))
            {
                return m_bValid;
            }
        }
        else
        {
            return m_bValid;
        }
    }

    hr = m_pNVItemsStorage->OpenStream(QCN_STREAM_NV_ITEM_ARRAY, NULL, dwMode, 0, &m_pNVItemsStream);
    if (!SUCCEEDED(hr))
    {
        if(!m_bReadOnly)
        {
            hr = m_pNVItemsStorage->CreateStream(QCN_STREAM_NV_ITEM_ARRAY, dwMode, 0, 0, &m_pNVItemsStream);
            if (!SUCCEEDED(hr))
            {
                return m_bValid;
            }
        }
        else
        {
            return m_bValid;
        }
    }
    
    LARGE_INTEGER li;
    li.QuadPart = 0;
    if(!m_bReadOnly)
    {
        ULARGE_INTEGER uli;
        uli.QuadPart = 0;
        m_pNVItemsStream->SetSize(uli);
        m_pNVItemsStream->Seek(li,STREAM_SEEK_SET,NULL);
        m_nCount = 0;
    }
    else
    {
        m_pNVItemsStream->Stat(&stat,STATFLAG_NONAME);
        m_nCount = stat.cbSize.LowPart/sizeof(NV_ITEM);
    }
    
    IStream *pStream;
    hr = m_pRootStorage->OpenStream(QCN_STREAM_FILE_VERSION, NULL, dwMode, 0, &pStream);
    if (!SUCCEEDED(hr))
    {
        if(!m_bReadOnly)
        {
            hr = m_pRootStorage->CreateStream(QCN_STREAM_FILE_VERSION, dwMode, 0, 0, &pStream);
            if (!SUCCEEDED(hr))
            {
                return m_bValid;
            }
            
            pStream->Seek(li,STREAM_SEEK_SET,NULL);
            hr = pStream->Write(&m_FileVersion, sizeof(m_FileVersion),NULL);
            if (!SUCCEEDED(hr))
            {
                return m_bValid;
            }
            pStream->Commit(STGC_CONSOLIDATE);
        }
        else
        {
            return m_bValid;
        }
    }
    pStream->Release();
    pStream = NULL;
    m_bValid = TRUE;
    return m_bValid;
}

void CQcnFile::Close(void)
{
    if(m_pNVItemsStream)
    {
        m_pNVItemsStream->Release();
    }
    if(m_pNVItemsStorage)
    {
        m_pNVItemsStorage->Release();
    }
    if(m_pDefaultStorage)
    {
        m_pDefaultStorage->Release();
    }
    if(m_pModelStorage)
    {
        m_pModelStorage->Release();
    }
    if(m_pRootStorage)
    {
        m_pRootStorage->Release();
    }
    InitParam();
}

BOOL CQcnFile::GetNVItem(int nIndex, nv_items_enum_type *pItem, void *pBuf, UINT nSize, UINT *pContext)
{
    if(!m_bValid || nIndex >= (int)m_nCount)
    {
        return FALSE;
    }

    m_CritiSect.Lock();
    NV_ITEM item;
    LARGE_INTEGER li;
    li.QuadPart = 0;
    
    memset(&item, 0, sizeof(NV_ITEM));
    li.LowPart = nIndex*sizeof(NV_ITEM);
    m_pNVItemsStream->Seek(li,STREAM_SEEK_SET,NULL);
    m_pNVItemsStream->Read(&item,sizeof(NV_ITEM),NULL);
    if(item.wSize != sizeof(NV_ITEM))
    {
        m_CritiSect.Unlock();
        return FALSE;
    }
    if(pItem)
    {
        *pItem = (nv_items_enum_type)item.wItem;
    }
    if(pBuf)
    {
        memcpy(pBuf,item.bData,MIN(nSize,DIAG_NV_ITEM_SIZE));
    }
    if(pContext)
    {
        *pContext = (UINT)item.wContext;
    }
    m_CritiSect.Unlock();
    return TRUE;
}

BOOL CQcnFile::SetNVItem(nv_items_enum_type nItem, void *pBuf, UINT nSize, UINT pContext)
{
    if(!m_bValid || m_bReadOnly || !pBuf)
    {
        return FALSE;
    }
    
    NV_ITEM item;
    memset(&item, 0, sizeof(NV_ITEM));
    item.wSize      = sizeof(NV_ITEM);
    item.wCount     = 1;
    item.wItem      = (WORD)nItem;
    item.wContext   = (WORD)pContext;
    memcpy(item.bData, pBuf, MIN(nSize,DIAG_NV_ITEM_SIZE));
    m_pNVItemsStream->Write(&item, sizeof(NV_ITEM), NULL);
    m_pNVItemsStream->Commit(STGC_CONSOLIDATE);
    m_nCount++;
    return TRUE;
}

#ifdef _DEBUG
void CQcnFile::OpenDocument(LPCTSTR pDocName, BOOL binDump) 
{ 
    IStorage * spRoot; 
 
    const HRESULT hr = StgOpenStorageEx(pDocName, STGM_DIRECT | STGM_READ | STGM_SHARE_EXCLUSIVE, STGFMT_STORAGE, 0, NULL, 0, IID_IStorage, (void **)&spRoot); 
 
    if (SUCCEEDED(hr)) 
    { 
        IterateStorage(spRoot, 0, binDump);
        spRoot->Release();
        spRoot = NULL; 
    } 
    else 
    { 
        LOG_OUTPUT(_T("Failed to open <%S>, hr = 0x%08X\n"), pDocName, hr); 
    } 
}

void CQcnFile::IterateStorage(IStorage *spStorage, int indentCount, BOOL binDump) 
{ 
    STATSTG stat; 
    CString indent;
    for(int i=0;i<indentCount;i++)
    {
        indent.AppendChar('-');
    }
 
    const HRESULT hr = spStorage->Stat(&stat, STATFLAG_DEFAULT); 
 
    if (SUCCEEDED (hr)) 
    { 
        LOG_OUTPUT(_T ("%Sstg <%ls>\n"), indent, stat.pwcsName); 
        CoTaskMemFree(stat.pwcsName); 
        EnumBranch(spStorage, indentCount, binDump); 
    } 
    else
    {
        LOG_OUTPUT(_T("%SFailed to Stat storage, hr = 0x%08X\n"), indent, hr); 
    } 
}

void CQcnFile::EnumBranch(IStorage *spStorage, int indentCount, BOOL binDump) 
{ 
    STATSTG stat; 
    IEnumSTATSTG *spEnum; 
    CString indent;
    for(int i=0;i<indentCount;i++)
    {
        indent.AppendChar('-');
    }

    const HRESULT hr = spStorage->EnumElements(0, NULL, 0, &spEnum); 
   
    if (SUCCEEDED(hr)) 
    { 
        while (spEnum->Next(1, &stat, NULL) == S_OK) 
        { 
            ExamineBranch(spStorage, indentCount, stat, binDump); 
            CoTaskMemFree(stat.pwcsName); 
        } 
        spEnum->Release();
    } 
    else 
    { 
        LOG_OUTPUT(_T("%SFailed to EnumElements, hr = 0x%08X\n"), indent, hr); 
    } 
}

void CQcnFile::ExamineBranch(IStorage *spStorage, int indentCount, STATSTG& stat, BOOL binDump) 
{ 
    CString indent;
    for(int i=0;i<indentCount;i++)
    {
        indent.AppendChar('-');
    }

    switch(stat.type){ 
    case STGTY_STORAGE: 
        ExamineStorage(spStorage, indentCount, stat, binDump); 
        break ; 
 
    case STGTY_STREAM: 
        LOG_OUTPUT(_T("%S str <%ls> %I64d\n"), indent, stat.pwcsName, stat.cbSize); 
        if (binDump) 
        { 
            DumpStreamContents(spStorage, indentCount, stat.pwcsName); 
        } 
        break ; 
    } 
} 

void CQcnFile::ExamineStorage(IStorage *spStorage, int indentCount, STATSTG& stat, BOOL binDump) 
{ 
    IStorage *spRoot; 
    CString indent;
    for(int i=0;i<indentCount;i++)
    {
        indent.AppendChar('-');
    }
   
    const HRESULT hr = spStorage->OpenStorage(stat.pwcsName, NULL, STGM_DIRECT | STGM_READ | STGM_SHARE_EXCLUSIVE, NULL, 0, &spRoot); 
   
    if (SUCCEEDED(hr)) 
    { 
        IterateStorage(spRoot, indentCount + 1, binDump); 
        spRoot->Release();
    } 
    else 
    { 
        printf ("%SFailed to OpenStorage <%ls>, hr = 0x%08X\n", indent, stat.pwcsName, hr); 
    } 
    spRoot = NULL; 
}

void CQcnFile::DumpStreamContents(IStorage *spStorage, int indentCount, LPWSTR pStreamName) 
{ 
    IStream* spData; 
    CString indent;
    for(int i=0;i<indentCount;i++)
    {
        indent.AppendChar('-');
    }
 
    const HRESULT hr = spStorage->OpenStream(pStreamName, NULL, STGM_DIRECT | STGM_READ | STGM_SHARE_EXCLUSIVE, 0, &spData); 
 
    if (SUCCEEDED(hr)) 
    { 
        BYTE streamData[16]; 
        ULONG streamRead = 0; 
 
        while(spData->Read(streamData, sizeof(streamData), &streamRead) == S_OK) 
        {
            LOG_OUTPUT(_T("%S    "), indent); 
            for (ULONG i = 0 ; i < streamRead ; ++i) 
            {
                TRACE (" %02X", streamData[i]); 
            } 
            LOG_OUTPUT(_T("\n")); 
            if (streamRead < sizeof(streamData)) break; 
        }
        spData->Release();
        spData = NULL; 
    } 
    else 
    { 
        LOG_OUTPUT(_T("%SFailed to OpenStream <%ls>, hr = 0x%08X\n"), indent, pStreamName, hr); 
    } 
} 
#endif
