#pragma once
#include "afx.h"
#include <afxmt.h>
#include "Diagnostic.h"

//QCNFILE ROOT(Storage)
//- "File_Version"(Stream)
//- MODEL NUMBER(Storage)
//--    "default"(Storage)
//---       "NV_NUMBERED_ITEMS"(Storage)
//----          "NV_ITEM_ARRAY"(Stream)
//----          "Mobile_Property_Info"(Stream)
//---       "EFS_Backup"(Storage)
//----          "EFS_Dir"(Storage)
//----          "EFS_Data"(Storage)
//

#define QCN_STORAGE_MODEL_DEFAULT           _T("00000000")
#define QCN_STORAGE_DEFAULT                 _T("default")
#define QCN_STORAGE_NV_NUMBERED_ITEMS       _T("NV_NUMBERED_ITEMS")
#define QCN_STREAM_NV_ITEM_ARRAY            _T("NV_ITEM_ARRAY")
#define QCN_STREAM_MOBILE_PROPERTY_INFO     _T("Mobile_Property_Info")
#define QCN_STREAM_FILE_VERSION             _T("File_Version")

#pragma pack(1)
typedef struct{
    WORD    wSize;
    WORD    wCount;
    WORD    wItem;
    WORD    wContext;
    BYTE    bData[DIAG_NV_ITEM_SIZE];
}NV_ITEM;

typedef struct{
    WORD    wMajor;
    WORD    wMinor;
    WORD    wRevision;
}FILE_VERSION;
#pragma pack()

class CQcnFile
{
public:
    CQcnFile(void);
    ~CQcnFile(void);
    
    BOOL            Open(LPCTSTR pDocName, BOOL bCreate=FALSE);
    void            Close(void);
    BOOL            GetNVItem(int nIndex, nv_items_enum_type *pItem, void *pBuf, UINT nSize, UINT *pContext);
    BOOL            SetNVItem(nv_items_enum_type nItem, void *pBuf, UINT nSize, UINT pContext=0);
    DWORD           GetCount(void)      {return m_nCount;};
    BOOL            IsValid(void)       {return m_bValid;};

#ifdef _DEBUG
    void            OpenDocument(LPCTSTR pDocName, BOOL binDump) ; 
    void            IterateStorage(IStorage *spStorage, int indentCount, BOOL binDump); 
    void            EnumBranch(IStorage *spStorage, int indentCount, BOOL binDump); 
    void            ExamineBranch(IStorage *spStorage, int indentCount, STATSTG& stat, BOOL binDump); 
    void            ExamineStorage(IStorage *spStorage, int indentCount, STATSTG& stat, BOOL binDump); 
    void            DumpStreamContents(IStorage *spStorage, int indentCount, LPWSTR pStreamName);
#endif
private:
    void            InitParam(void);
    
private:
    BOOL            m_bValid;
    BOOL            m_bReadOnly;
    DWORD           m_nCount;
    FILE_VERSION    m_FileVersion;
    IStorage       *m_pRootStorage;
    IStorage       *m_pModelStorage;
    IStorage       *m_pDefaultStorage;
    IStorage       *m_pNVItemsStorage;
    IStream        *m_pNVItemsStream;
    CCriticalSection    m_CritiSect;
};
