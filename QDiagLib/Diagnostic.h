/*=================================================================================================

FILE : Diagnostic.H

SERVICES: Declaration of an MFC wrapper class for Diagnostic on DM

GENERAL DESCRIPTION:

PUBLIC CLASSES AND STATIC FUNCTIONS:
CDiagnostic;

INITIALIZATION AND SEQUENCING REQUIREMENTS:

====================================================================================================*/


/*====================================================================================================

==MODIFY HISTOY FOR FILE==

when         who     what, where, why
----------   ---     ----------------------------------------------------------
04/28/2004   tyz      (tyz040528)Add DLOAD mode process
12/28/2003   tyz      create

=====================================================================================================*/
#if !defined(_DIAGNOSTIC_H__)
#define _DIAGNOSTIC_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include <afxmt.h>
#include "SerialComm.h"
#include "HDLCFrame.h"
#include "StreamFrames.h"
#include "DLoadPkt.h"
#include "diagcmd.h"
#include "diagpkt.h"
#include "fs_diag_i.h"
#include "fs_errno.h"
#include "toolsdiag.h"
#include "nv.h"
#include "nvdiag.h"
#include "cmdiag.h"
#include "diagdiag.h"

/*===========================================================================

DEFINITIONS AND CONSTANTS

===========================================================================*/
#define REQUEST_SEND_FAILED 0xFFFFFFFF
#define EFS2_DATAPACKET_LEN 512

/* -------------------------------------------------------------------------
 * Bulid Packet MACRO
 *-------------------------------------------------------------------------*/
#define SYSTEM_PACKET_BUILT(packet, cmd_code)   ((diagpkt_hdr_type*)(&packet))->command_code      = (cmd_code)
                                                
#define FSSUBSYS_PACKET_BUILT(packet, cmd_code) ((diagpkt_subsys_hdr_type*)(&packet))->command_code      = DIAG_SUBSYS_CMD_F; \
                                                ((diagpkt_subsys_hdr_type*)(&packet))->subsys_id         = DIAG_SUBSYS_FS; \
                                                ((diagpkt_subsys_hdr_type*)(&packet))->subsys_cmd_code   = (cmd_code)

#define NVSUBSYS_PACKET_BUILT(packet, cmd_code) ((diagpkt_subsys_hdr_type*)(&packet))->command_code      = DIAG_SUBSYS_CMD_F; \
                                                ((diagpkt_subsys_hdr_type*)(&packet))->subsys_id         = DIAG_SUBSYS_NV; \
                                                ((diagpkt_subsys_hdr_type*)(&packet))->subsys_cmd_code   = (cmd_code)
/*===========================================================================

GLOBAL DATA

===========================================================================*/
#define DIAG_INI_FILE               _T(".\\QCOMConfig.ini")
#define DIAG_INI_APP_NAME           _T("DIAG_PORT")
#define DIAG_INI_COM_CURR_KEY       _T("COMPORT")
#define DIAG_INI_COM_CURR_DEF_STR   _T("1")
#define DIAG_INI_COM_CURR_DEF_INT   1
#define DIAG_INI_SPC_KEY            _T("SPCSTR")
#define DIAG_INI_SPC_DEF_STR        _T("000000")

// Response
#define DIAG_DLOAD_RSP              0x0D

// 根据发送DIAG_STATUS_F命令返回第一个字节不同的值来判断状态
enum{
    DIAG_PHONEMODE_NOPORT,          // No COM Port Open
    DIAG_PHONEMODE_NONE,            // Open COM Port but No phone Connected
    DIAG_PHONEMODE_CDMA,            // DIAG_STATUS_F CDMA状态 0x0C
    DIAG_PHONEMODE_WCDMA,           // DIAG_BAD_CMD_F 0x13
    DIAG_PHONEMODE_DLOAD,           // DIAG_DLOAD_RSP 0x0D
    DIAG_PHONEMODE_EDLOAD,          // DEFAULT_FLAG_CHAR 0x7E DIAG_DLOAD_RSP 0x0D
    DIAG_PHONEMODE_ARMPRG,          // DEFAULT_FLAG_CHAR 0x7E
    DIAG_PHONEMODE_FASTBOOT,
    DIAG_PHONEMODE_MAX
};

enum{
    CMD_MODE_DIAG,
    CMD_MODE_DLOAD,
    CMD_MODE_ARMPRG,
    CMD_MODE_STREAM,
    CMD_MODE_MAX
};

#define DIAG_RETRY_TIMES            3
#define DIAG_WAIT_TIME              2000
#define DIAG_CRITISECT_LOCK()       m_CritiSect.Lock()
#define DIAG_CRITISECT_UNLOCK()     m_CritiSect.Unlock()
/*===========================================================================

LOCAL/STATIC DATA

===========================================================================*/

/*===========================================================================

CLASS DEFINITIONS

===========================================================================*/

class CDiagnostic  //: virtual public CObject
{
protected:
    //data member
    CSerialComm         m_COMPort;
    CHDLCFrame          m_reqFrame, m_rspFrame;
    CCriticalSection    m_CritiSect;
    BOOL                m_bDiagStarted;
    BOOL                m_bRXSuccess;
    DWORD               m_CommandMode;
    DWORD               m_dwStreamMode;
    DWORD               m_dwStreamCommand;
    CStreamFrames       m_StreamFrames;
    HANDLE              m_hStreamEvent;
    HANDLE              m_hMonitorEvent;
    BOOL                m_bMonitor;
    CWinThread         *m_pThread; 
    BOOL                m_bFastBoot;

    // Diag Mode
    int32               m_nDiagErr;

    // EFS2 Mode
    int32               m_nEFS2Err;
    int32               m_nEFS2MaxPath;
    int32               m_nEFS2MaxName;

    // DLoad Mode
    int32               m_nDLoadErr;

    // ARMPRG Mode
    int32               m_nAPErr;

protected:
    //Implementation
    BOOL                SendReqPacket(void* pReqData, int Length, BOOL bHeadEnc = FALSE, int nWaitMS=DIAG_WAIT_TIME, int nRetry=DIAG_RETRY_TIMES);
    BOOL                SendDiagPacket(void* pReqData, int Length, int nWaitMS=DIAG_WAIT_TIME, int nRetry=DIAG_RETRY_TIMES);
    BOOL                SendDloadPacket(void* pReqData, int Length, int nWaitMS=DIAG_WAIT_TIME, int nRetry=DIAG_RETRY_TIMES);
    BOOL                SendAPPacket(void* pReqData, int Length, int nWaitMS=DIAG_WAIT_TIME, int nRetry=DIAG_RETRY_TIMES);
    BOOL                SendStreamPacket(void* pReqData, int Length);
    
    void                ReceiveFrame(WORD wFrameSize);
    void                MonitorFunc(void);
    void                ResetMonitorThread(void)    { m_pThread = NULL;}
    //Static Implementation
    static void         RspPacketCB(LPVOID pParam, DWORD dwFrameSize);
    static UINT         MonitorThread(LPVOID pParam);

public:
    // Construction and Destruction
    CDiagnostic();
    virtual  ~CDiagnostic();

    //Implementation
    BOOL                StartDiag(int nPortID, BOOL bMonitor=FALSE);
    BOOL                StopDiag(void);
    BOOL                IsDiagStarted(void)     {return m_bDiagStarted;};
    int                 ConnectPhone(void);
    int                 GetPortID(void)         {return m_COMPort.GetPortID();}
    int32               GetLastDiagErr(void)    {return m_nDiagErr;}
    BOOL                SetStreamWindowSize(int nSize);
    BOOL                IsStreamComplete(void)  {return ((m_CommandMode!=CMD_MODE_STREAM) || m_StreamFrames.IsStreamComplete());};
    BOOL                SendTimeoutStreamFrames(void);
    BOOL                SwitchStreamMode(DWORD dwStreamMode, DWORD dwStreamCommand);
    void                SetFastBootMode(BOOL bFastBoot) {m_bFastBoot = bFastBoot;};
    
    // Common Operation
    BOOL                SendModeChangeCmd(BYTE mode);
    BOOL                SendSwitchToDLoadReq(void);
    BOOL                EnterSPCMode(void);
    BOOL                SendSpcReq(diagpkt_sec_code_type *pSPC);  

    // NV Opration
    BOOL                SendWriteNVItem(nv_items_enum_type nItem, void *pBuf, UINT nSize);
    BOOL                SendReadNVItem(nv_items_enum_type nItem, void *pBuf, UINT nSize);
    BOOL                SendWriteNVItemExt(nv_items_enum_type nItem, void *pBuf, UINT nSize, UINT context);
    BOOL                SendReadNVItemExt(nv_items_enum_type nItem, void *pBuf, UINT nSize, UINT context);

    //EFS2 Opration
    // Parameter negotiation packet
    BOOL                SendEFS2HelloReq(void);
    // Send information about EFS2 params
    BOOL                SendEFS2QueryReq(void);
    // Open a file
    BOOL                SendEFS2OpenReq(char *pName, int32 oFlag, int32 *pFd);
    // Close a file
    BOOL                SendEFS2CloseReq(int32 nFd);
    // Read a file
    BOOL                SendEFS2ReadReq(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset, int32 *pReaded);
    // Write a file
    BOOL                SendEFS2WriteReq(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset, int32 *pWritten);
    // Remove a symbolic link or file
    BOOL                SendEFS2UnlinkReq(char *pName);
    // Create a directory
    BOOL                SendEFS2MkDirReq(char *pName);
    // Remove a directory
    BOOL                SendEFS2RmDirReq(char *pName);
    // Open a directory for reading
    BOOL                SendEFS2OpenDirReq(char *pName, uint32 *pdwDirp);
    // Read a directory
    BOOL                SendEFS2ReadDirReq(uint32 dwDirp, int32 nSeq, char *pName, uint32 dwLen, BOOL *pbFile, int32 *pSize);
    // Close an open directory
    BOOL                SendEFS2CloseDirReq(uint32 dwDirp);
    // Rename a file or directory
    BOOL                SendEFS2RenameReq(char *pOldName, char *pNewName);
    // Obtain information about a named file
    BOOL                SendEFS2StatReq(char *pName, int32 *pSize);
    // Obtain file system information
    BOOL                SendEFS2StatFsReq(char *pName, efs2_diag_statfs_rsp_type *pStatFs);
    // Get flash device info
    BOOL                SendEFS2DevInfoReq(efs2_diag_dev_info_rsp_type *pDevInfo);
    // Truncate a file by the name
    BOOL                SendEFS2TruncateReq(char *pName, int32 nTruncLen);
    // Obtains extensive file system info
    BOOL                SendEFS2StatVFSReq(char *pName, efs2_diag_statvfs_v2_rsp_type *pStatVFS);
    // Format a Connected device
    BOOL                SendEFS2HotplugFormatReq(char *pName);
    // get the hotplug device info
    BOOL                SendEFS2HotplugDevInfoReq(char *pName, efs2_diag_hotplug_device_info_rsp_type *pHotplugInfo);
    int32               GetLastEFS2Err(void)    {return m_nEFS2Err;}
    
    BOOL                EFS2Connect(void);
    int32               EFS2OpenFile(CString &szName);
    int32               EFS2CreateFile(CString &szName);
    int32               EFS2ReadFile(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset);
    int32               EFS2WriteFile(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset);
    BOOL                EFS2CloseFile(int32 nFd);
    BOOL                EFS2RemoveFile(CString &szName);
    BOOL                EFS2RenameFile(CString &szOldName, CString &szNewName);
    BOOL                EFS2MkDir(CString &szName);
    BOOL                EFS2RmDir(CString &szName);
    BOOL                EFS2TruncateFile(CString &szName, int32 nTruncLen);
    int32               EFS2GetFileSize(CString &szName);
    BOOL                EFS2IsFileExist(CString &szName);
    

    //Sub mode请求函数
    //DLoad Mode
    BOOL                SendDLoadWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen);
    BOOL                SendDLoadEreaseReq(DWORD dwAddr, DWORD dwLen);
    BOOL                SendDLoadGoReq(DWORD dwAddr);
    BOOL                SendDLoadNOPReq(void);
    BOOL                SendDLoadParamReq(diag_dload_param_rsp_type *pRspParam);
    BOOL                SendDLoadVerReq(diag_dload_ver_rsp_type *pRspVer);
    BOOL                SendDLoadResetReq(void);
    BOOL                SendDLoadUnlockReq(const BYTE *pCode, WORD wLen);
    BOOL                SendDLoadPoweroffReq(void);
    BOOL                SendDLoadWrite32Req(DWORD dwAddr, const BYTE *pData, WORD wLen);
    BOOL                SendDLoadResetModeReq(DWORD dwMode);
    BOOL                ProcessDLoadRsp(void);
    int32               GetLastDLoadErr(void)   {return m_nDLoadErr;}

    // ARMPRG Mode
    BOOL                SendAPHelloReq(diag_ap_hello_rsp_type *pHelloRsp, BYTE req_features = SUPPORTED_FEATURES);
    BOOL                SendAPReadReq(diag_ap_read_rsp_type *pReadRsp, DWORD dwAddr, WORD wLen);
    BOOL                SendAPWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen);
    BOOL                SendAPStreamWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen);
    BOOL                SendAPSyncReq(const BYTE *pData, WORD wLen);
    BOOL                SendAPResetReq(void);
    BOOL                SendAPPoweroffReq(void);
    BOOL                SendAPOpenReq(open_mode_type mode);
    BOOL                SendAPCloseReq(void);
    BOOL                SendAPSecModeReq(BYTE mode);
    BOOL                SendAPPartiTblReq(BYTE bOverride, const BYTE *pTbl, WORD wLen);
    BOOL                SendAPUnframedStreamWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen);
    BOOL                SendAPOpenMultiReq(open_multi_mode_type mode, const BYTE *pData=NULL, WORD wLen=0);
    BOOL                ProcessAPRsp(void);
    int32               GetLastAPErr(void)      {return m_nAPErr;}
};
#endif // !defined(_DIAGNOSTIC_H__)